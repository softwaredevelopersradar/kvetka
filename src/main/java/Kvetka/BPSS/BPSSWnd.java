package Kvetka.BPSS;

import BPSSControl.BPSSControl;
import Kvetka.Interfaces.IWndEvents;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class BPSSWnd  extends AnchorPane implements Initializable {

    public static Stage WindowBPSS;
    static Scene FrontEndBPSS;

    @FXML public BPSSControl ucBPSS;

    private List<IWndEvents> listenersBPSS = new ArrayList<IWndEvents>();
    public void addListenerBPSS(IWndEvents toAdd) {
        listenersBPSS.add(toAdd);
    }

    public BPSSWnd() {

        InitBPSSWnd();

        WindowBPSS.setTitle("Подавление навигации");
        WindowBPSS.getIcons().add(new Image(getClass().getResourceAsStream("Ico/ShowBPSS.png")));
    }

    private void InitBPSSWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BPSSWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndBPSS = new Scene(fxmlLoader.load(), 250, 355);
            WindowBPSS = new Stage();
            WindowBPSS.initModality(Modality.WINDOW_MODAL);
            WindowBPSS.setResizable(false);
            WindowBPSS.setScene(FrontEndBPSS);
            WindowBPSS.show();

            WindowBPSS.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing
                    for (IWndEvents BPSSEvent : listenersBPSS)
                        BPSSEvent.OnCloseBPSS();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
