package Kvetka.MapTasks;

import EnteringMyStations.EnteringMyStationsControl;
import Kvetka.Interfaces.IMapTasksWndEvents;
import KvetkaFolder.EnteringComplexControl;
import KvetkaFolder.IEnemyStationsValueSubmitted;
import MapModels.EnemyStationModel;
import MapModels.MyStationsModel;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ComplexCompositionEnemyWnd extends AnchorPane implements IEnemyStationsValueSubmitted, Initializable {

    @FXML public EnteringComplexControl ucComplexCompositionEnemy;

    public static Stage WindowComplexCompositionEnemy;
    static Scene FrontEndComplexCompositionEnemy;

    public List<IMapTasksWndEvents> listenersComplexCompositionEnemy = new ArrayList<IMapTasksWndEvents>();
    public void addListenerComplexCompositionEnemy(IMapTasksWndEvents toAdd) {
        listenersComplexCompositionEnemy.add(toAdd);
    }

    public ComplexCompositionEnemyWnd() {

        InitComplexCompositionEnemyWnd();

        WindowComplexCompositionEnemy.setTitle("Состав комплекса ПРОТИВНИК");
        WindowComplexCompositionEnemy.getIcons().add(new Image(getClass().getResourceAsStream("Ico/ComplexCompositionEnemy.png")));
    }

    private void InitComplexCompositionEnemyWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ComplexCompositionEnemyWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndComplexCompositionEnemy = new Scene(fxmlLoader.load(), 240, 350);
            WindowComplexCompositionEnemy = new Stage();
            WindowComplexCompositionEnemy.initModality(Modality.WINDOW_MODAL);
            WindowComplexCompositionEnemy.setResizable(false);
            WindowComplexCompositionEnemy.setScene(FrontEndComplexCompositionEnemy);
            WindowComplexCompositionEnemy.show();

            WindowComplexCompositionEnemy.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing

                    for (IMapTasksWndEvents ComplexCompositionEnemyEvent : listenersComplexCompositionEnemy)
                        ComplexCompositionEnemyEvent.OnCloseComplexCompositionEnemyWnd();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ucComplexCompositionEnemy.AddListener(this);
    }

    @Override
    public void onEnemyStationSubmitted(EnemyStationModel enemyStationModel) {

        for (IMapTasksWndEvents complexCompositionEnemyTaskEvent : listenersComplexCompositionEnemy) {
            try {
                complexCompositionEnemyTaskEvent.OnSendComplexCompositionEnemy(enemyStationModel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void UpdateCoordinates(EnemyStationModel enemyStationModel){
        ucComplexCompositionEnemy.UpdateCoordinates(enemyStationModel);
    }
}
