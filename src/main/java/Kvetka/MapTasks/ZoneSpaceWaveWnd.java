package Kvetka.MapTasks;

import EnergyFolder.EnergyAvailabilityCalculationControl;
import EnergyFolder.interfaces.IEnergyAvailabilitySubmitted;
import Kvetka.Interfaces.IMapTasksWndEvents;
import MapModels.EDPRVModel;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ZoneSpaceWaveWnd extends AnchorPane implements IEnergyAvailabilitySubmitted,  Initializable {

    @FXML public EnergyAvailabilityCalculationControl ucZoneSpaceWave;

    public static Stage WindowZoneSpaceWave;
    static Scene FrontEndZoneSpaceWave;

    public List<IMapTasksWndEvents> listenersZoneSpaceWave = new ArrayList<IMapTasksWndEvents>();
    public void addListenerZoneSpaceWave(IMapTasksWndEvents toAdd) {
        listenersZoneSpaceWave.add(toAdd);
    }

    public ZoneSpaceWaveWnd() {

        InitZoneSpaceWaveWnd();

        WindowZoneSpaceWave.setTitle("Расчет зоны ЭД пространственной волной");
        WindowZoneSpaceWave.getIcons().add(new Image(getClass().getResourceAsStream("Ico/SpaceWave.png")));
    }

    private void InitZoneSpaceWaveWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ZoneSpaceWaveWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndZoneSpaceWave = new Scene(fxmlLoader.load(), 364, 299);
            WindowZoneSpaceWave = new Stage();
            WindowZoneSpaceWave.initModality(Modality.WINDOW_MODAL);
            WindowZoneSpaceWave.setResizable(false);
            WindowZoneSpaceWave.setScene(FrontEndZoneSpaceWave);
            WindowZoneSpaceWave.show();

            WindowZoneSpaceWave.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing

                    for (IMapTasksWndEvents zoneSpaceWaveEvent : listenersZoneSpaceWave)
                        zoneSpaceWaveEvent.OnCloseZoneSpaceWaveWnd();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ucZoneSpaceWave.AddListener(this);
    }

    @Override
    public void onSubmittedEDPRVModel(EDPRVModel edprvModel) {

        for (IMapTasksWndEvents zoneSpaceWaveTaskEvent : listenersZoneSpaceWave)
            zoneSpaceWaveTaskEvent.OnSendZoneSpaceWave(edprvModel);
    }

    public void UpdateRezults(EDPRVModel edprvModel){
        ucZoneSpaceWave.UpdateCalculationParameters(edprvModel);
    }
}
