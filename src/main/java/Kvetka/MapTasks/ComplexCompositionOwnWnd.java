package Kvetka.MapTasks;

import EnteringMyStations.EnteringMyStationsControl;
import EnteringMyStations.interfaces.IMyStationsSubmitted;
import Kvetka.Interfaces.IMapTasksWndEvents;
import MapModels.MyStationsModel;
import RouteDrawing.RouteDrawingControl;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ComplexCompositionOwnWnd extends AnchorPane implements IMyStationsSubmitted, Initializable {

    @FXML public EnteringMyStationsControl ucComplexCompositionOwn;

    public static Stage WindowComplexCompositionOwn;
    static Scene FrontEndComplexCompositionOwn;

    public List<IMapTasksWndEvents> listenersComplexCompositionOwn = new ArrayList<IMapTasksWndEvents>();
    public void addListenerComplexCompositionOwn(IMapTasksWndEvents toAdd) {
        listenersComplexCompositionOwn.add(toAdd);
    }

    public ComplexCompositionOwnWnd() {

        InitComplexCompositionOwnWnd();

        WindowComplexCompositionOwn.setTitle("Состав комплекса СВОЙ");
        WindowComplexCompositionOwn.getIcons().add(new Image(getClass().getResourceAsStream("Ico/ComplexCompositionOwn.png")));
    }

    private void InitComplexCompositionOwnWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ComplexCompositionOwnWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndComplexCompositionOwn = new Scene(fxmlLoader.load(), 190, 350);
            WindowComplexCompositionOwn = new Stage();
            WindowComplexCompositionOwn.initModality(Modality.WINDOW_MODAL);
            WindowComplexCompositionOwn.setResizable(false);
            WindowComplexCompositionOwn.setScene(FrontEndComplexCompositionOwn);
            WindowComplexCompositionOwn.show();

            WindowComplexCompositionOwn.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing

                    for (IMapTasksWndEvents ComplexCompositionOwnEvent : listenersComplexCompositionOwn)
                        ComplexCompositionOwnEvent.OnCloseComplexCompositionOwnWnd();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ucComplexCompositionOwn.AddListener(this);
    }

    @Override
    public void onMyStationSubmitted(MyStationsModel myStationsModel) {

        for (IMapTasksWndEvents complexCompositionOwnTaskEvent : listenersComplexCompositionOwn) {
            try {
                complexCompositionOwnTaskEvent.OnSendComplexCompositionOwn(myStationsModel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void UpdateCoordinates(MyStationsModel myStationsModel){

        ucComplexCompositionOwn.UpdateCoordinates(myStationsModel);
    }
}
