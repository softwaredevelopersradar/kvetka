package Kvetka.MapTasks;

import Kvetka.Interfaces.IMapTasksWndEvents;
import MapModels.AzimuthModel;
import RouteDrawing.ICoordinatesSubmitted;
import RouteDrawing.RouteDrawingControl;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AzimuthWnd extends AnchorPane implements ICoordinatesSubmitted, Initializable {

    @FXML public RouteDrawingControl ucRouteDrawing;

    public static Stage WindowAzimuth;
    static Scene FrontEndAzimuth;

    public List<IMapTasksWndEvents> listenersAzimuth = new ArrayList<IMapTasksWndEvents>();
    public void addListenerAzimuth(IMapTasksWndEvents toAdd) {
        listenersAzimuth.add(toAdd);
    }

    public AzimuthWnd() {

        InitAzimuthWnd();

        WindowAzimuth.setTitle("Азимут");
        WindowAzimuth.getIcons().add(new Image(getClass().getResourceAsStream("Ico/Azimuth.png")));
    }

    private void InitAzimuthWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AzimuthWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndAzimuth = new Scene(fxmlLoader.load(), 213, 261);
            WindowAzimuth = new Stage();
            WindowAzimuth.initModality(Modality.WINDOW_MODAL);
            WindowAzimuth.setResizable(false);
            WindowAzimuth.setScene(FrontEndAzimuth);
            WindowAzimuth.show();

            WindowAzimuth.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    for (IMapTasksWndEvents azimuthEvent : listenersAzimuth)
                        azimuthEvent.OnCloseAzimuthWnd();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void UpdateCoordinatesASP(AzimuthModel azimuthModel) {

       ucRouteDrawing.UpdateCoordinatesASP(azimuthModel);
    }

    public void UpdateCoordinatesTarget(AzimuthModel azimuthModel) {

        ucRouteDrawing.UpdateCoordinatesTarget(azimuthModel);
    }

    public void UpdateAzimuth(AzimuthModel azimuthModel){
        ucRouteDrawing.UpdateAzimuth(azimuthModel);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ucRouteDrawing.AddListener(this);
    }

    @Override
    public void onSubmitted(AzimuthModel azimuthModel) {

        for (IMapTasksWndEvents azimuthTaskEvent : listenersAzimuth)
            azimuthTaskEvent.OnSendAzimuth(azimuthModel);
    }
}
