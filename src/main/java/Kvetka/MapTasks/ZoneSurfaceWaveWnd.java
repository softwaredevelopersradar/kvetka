package Kvetka.MapTasks;

import EnergyFolder.EnergySkyWaveCalculationControl;
import EnergyFolder.interfaces.IEnergySkyWaveSubmitted;
import Kvetka.Interfaces.IMapTasksWndEvents;
import MapModels.EDPOVModel;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ZoneSurfaceWaveWnd extends AnchorPane implements IEnergySkyWaveSubmitted, Initializable {

    @FXML public EnergySkyWaveCalculationControl ucZoneSurfaceWave;

    public static Stage WindowZoneSurfaceWave;
    static Scene FrontEndZoneSurfaceWave;

    public List<IMapTasksWndEvents> listenersZoneSurfaceWave = new ArrayList<IMapTasksWndEvents>();
    public void addListenerZoneSurfaceWave(IMapTasksWndEvents toAdd) {
        listenersZoneSurfaceWave.add(toAdd);
    }

    public ZoneSurfaceWaveWnd() {

        InitZoneSurfaceWaveWnd();

        WindowZoneSurfaceWave.setTitle("Расчет зоны ЭД поверхностной волной");
        WindowZoneSurfaceWave.getIcons().add(new Image(getClass().getResourceAsStream("Ico/SurfaceWave.png")));
    }

    private void InitZoneSurfaceWaveWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ZoneSurfaceWaveWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndZoneSurfaceWave = new Scene(fxmlLoader.load(), 367, 260);
            WindowZoneSurfaceWave = new Stage();
            WindowZoneSurfaceWave.initModality(Modality.WINDOW_MODAL);
            WindowZoneSurfaceWave.setResizable(false);
            WindowZoneSurfaceWave.setScene(FrontEndZoneSurfaceWave);
            WindowZoneSurfaceWave.show();

            WindowZoneSurfaceWave.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing

                    for (IMapTasksWndEvents zoneSurfaceWaveEvent : listenersZoneSurfaceWave)
                        zoneSurfaceWaveEvent.OnCloseZoneSurfaceWaveWnd();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        ucZoneSurfaceWave.AddListener(this);
    }


    @Override
    public void onSubmittedEDPOVModel(EDPOVModel edpovModel) {

        for (IMapTasksWndEvents zoneSurfaceWaveTaskEvent : listenersZoneSurfaceWave)
            zoneSurfaceWaveTaskEvent.OnSendZoneSurfaceWave(edpovModel);
    }
}
