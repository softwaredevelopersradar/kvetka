package Kvetka.TablesOperations;

import KvetkaModels.SectorsModel;
import KvetkaModels.SpecFreqsModel;
import KvetkaModels.SuppressFWSModel;
import ValuesCorrectLib.Suppress.CorrectParamsSuppressFWS;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SuppressFWSOperations {

    /**
     * Проверка возможности добавления / изменения в таблицу SuppressFWS
     * @param listSuppressFWS Текущий список ИРИ ФРЧ РП
     * @param listRangesSuppress Текущий список диапазонов РП
     * @param listFreqsForbidden Текущий список запрещенных частот
     * @param SuppressFWS запись, которую хотим добавить / изменить
     * @param CountIri количество ИРИ (из GlobalProperties)
     * @param IsChange true - измененить запись,
     *                 false - добавить запись
     * @return сообщение об ошибке (пустая строка - успешно)
     */
    public static String IsUpdateTableSuppressFWS(List<SuppressFWSModel> listSuppressFWS, List<SpecFreqsModel> listRangesSuppress, List<SpecFreqsModel> listFreqsForbidden, List<SectorsModel> listSectors, SuppressFWSModel SuppressFWS, Byte CountIri, Boolean IsChange)
    {
        List<SuppressFWSModel> tempListSuppressFWS = new ArrayList<>();
        if (IsChange) {

            tempListSuppressFWS.addAll(listSuppressFWS.stream()
                    .filter(x -> x.getId() != SuppressFWS.getId())
                    .collect(Collectors.toList()));
        } else {

            tempListSuppressFWS = listSuppressFWS;
        }

        String sMessage = CorrectParamsSuppressFWS.IsCheckRangesSuppress(SuppressFWS.getFrequency(), listRangesSuppress);
        if (sMessage != "")
            return sMessage;

        sMessage = CorrectParamsSuppressFWS.IsCheckForbiddenFreq(SuppressFWS.getFrequency(), listFreqsForbidden);
        if (sMessage != "")
            return sMessage;

        sMessage = CorrectParamsSuppressFWS.CountIRILetter(SuppressFWS.getLetter(), CountIri, tempListSuppressFWS);
        if (sMessage != "")
            return sMessage;

        sMessage = CorrectParamsSuppressFWS.IsCheckSectors(SuppressFWS.getBearing(), listSectors);
        if (sMessage != "")
            return sMessage;

        return sMessage;
    }
}
