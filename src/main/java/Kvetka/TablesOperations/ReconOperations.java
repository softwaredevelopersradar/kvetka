package Kvetka.TablesOperations;
import KvetkaModels.AnalogReconFWSModel;
import KvetkaModels.DigitalReconFWSModel;
import KvetkaModels.ReconFWSModel;
import KvetkaModels.SectorsModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReconOperations {

    public static List<ReconFWSModel> SelectedReconFWS(List<SectorsModel> lSectors, List<ReconFWSModel> lReconFWS)
    {
        List<ReconFWSModel> tempListReconFWS = new ArrayList<>();

        for (int i = 0; i < lSectors.size(); i++) {

            int finalI = i;
            tempListReconFWS.addAll(lReconFWS.stream()
                    .filter(x -> x.getBearing1() >= lSectors.get(finalI).getAngleMin() &&
                                 x.getBearing1() <= lSectors.get(finalI).getAngleMax() ||
                                 x.getBearing1() == -10)
                    .collect(Collectors.toList()));
        }

        return tempListReconFWS;
    }

    public static List<DigitalReconFWSModel> SelectedDigitalReconFWS(List<SectorsModel> lSectors, List<DigitalReconFWSModel> lDigitalReconFWS)
    {
        List<DigitalReconFWSModel> tempListDigitalReconFWS = new ArrayList<>();

        for (int i = 0; i < lSectors.size(); i++) {

            int finalI = i;
            tempListDigitalReconFWS.addAll(lDigitalReconFWS.stream()
                    .filter(x -> x.getBearingOwn() >= lSectors.get(finalI).getAngleMin() &&
                                 x.getBearingOwn() <= lSectors.get(finalI).getAngleMax() ||
                                 x.getBearingOwn() == -10)
                    .collect(Collectors.toList()));
        }

        return tempListDigitalReconFWS;
    }

    public static List<AnalogReconFWSModel> SelectedAnalogReconFWS(List<SectorsModel> lSectors, List<AnalogReconFWSModel> lAnalogReconFWS)
    {
        List<AnalogReconFWSModel> tempListAnalogReconFWS = new ArrayList<>();

        for (int i = 0; i < lSectors.size(); i++) {

            int finalI = i;
            tempListAnalogReconFWS.addAll(lAnalogReconFWS.stream()
                    .filter(x -> x.getBearing1() >= lSectors.get(finalI).getAngleMin() &&
                                 x.getBearing1() <= lSectors.get(finalI).getAngleMax() ||
                                 x.getBearing1() == -10)
                    .collect(Collectors.toList()));
        }

        return tempListAnalogReconFWS;
    }
}
