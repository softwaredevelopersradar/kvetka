package Kvetka.TablesOperations;

import KvetkaModels.SpecFreqsModel;
import ValuesCorrectLib.CorrectParams;
import ValuesCorrectLib.SpecFreqs.CorrectParamsSpecFreqs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpecFreqsOperations {

    /**
     * Проверка на возможность добавления диапазона
     * @param lTemp Текущий список диапазонов
     * @param recAdd Диапазон для добавления в таблицу
     * @param IsChange true - измененить запись,
     *                 false - добавить запись
     * @return true - диапазон можно добавить
     */
    public static Boolean IsAddSpecFreq(List<SpecFreqsModel> lTemp, SpecFreqsModel recAdd, Boolean IsChange)
    {
        List<SpecFreqsModel> tempListSpecFreq = new ArrayList<>();
        if (IsChange) {

            tempListSpecFreq.addAll(lTemp.stream()
                    .filter(x -> x.getId() != recAdd.getId())
                    .collect(Collectors.toList()));
        } else {

            tempListSpecFreq = lTemp;
        }

        CorrectParamsSpecFreqs.IsCorrectMinMax(recAdd);
        if (CorrectParams.IsCorrectFreqMinMax(recAdd.getFreqMin(), recAdd.getFreqMax())) {

            List<SpecFreqsModel> tempSpecFreq = new ArrayList<>(tempListSpecFreq);
            List<SpecFreqsModel> addSpecFreq = new ArrayList<>();
            addSpecFreq.add(recAdd);
            if (CorrectParamsSpecFreqs.IsAddRecordsToList(tempSpecFreq, addSpecFreq)) {

                return true;
            }
        }
        return false;
    }


}
