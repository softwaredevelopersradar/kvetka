package Kvetka.TablesOperations;

import KvetkaModels.SpecFreqsModel;
import KvetkaModels.SuppressFHSSModel;
import ValuesCorrectLib.Properties.PropertiesStations;
import ValuesCorrectLib.Suppress.CorrectParamsSuppressFHSS;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SuppressFHSSOperations {

    /**
     * Проверка возможности добавления / изменения в таблицу SuppressFHSS
     * @param rangesSuppress Текущий список диапазонов РП
     * @param freqsForbidden Текущий список запрещенных частот
     * @param listSuppressFHSS Текущий список ИРИ ППРЧ РП
     * @param SuppressFHSS запись, которую хотим добавить / изменить
     * @return сообщение об ошибке (пустая строка - успешно)
     */
    public static String IsUpdateTableSuppressFHSS(List<SpecFreqsModel> rangesSuppress, List<SpecFreqsModel> freqsForbidden, List<SuppressFHSSModel> listSuppressFHSS, SuppressFHSSModel SuppressFHSS)
    {
        String sMessage = CorrectParamsSuppressFHSS.IsCheckRangeSuppressFHSS(SuppressFHSS.getFreqMin(), SuppressFHSS.getFreqMax());
        if (sMessage != "")
            return sMessage;

        sMessage = CorrectParamsSuppressFHSS.IsCheckRangesSuppress(SuppressFHSS.getFreqMin(), SuppressFHSS.getFreqMax(), rangesSuppress);
        if (sMessage != "")
            return sMessage;

        sMessage = CorrectParamsSuppressFHSS.IsCheckForbiddenRange(SuppressFHSS.getFreqMin(), SuppressFHSS.getFreqMax(),
                freqsForbidden.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));

        return sMessage;
    }
}
