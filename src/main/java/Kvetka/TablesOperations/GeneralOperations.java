package Kvetka.TablesOperations;

import javafx.scene.control.Alert;

public class GeneralOperations {

    public static void ShowMessage(String sMessage) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Сообщение!");
        alert.setHeaderText((String)null);
        alert.setContentText(sMessage);
        alert.show();
    }
}
