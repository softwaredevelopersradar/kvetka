package Kvetka.DFAccuracy;

import DFAccuracyControl.*;
import Kvetka.Interfaces.IWndEvents;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class DFAccuracyWnd extends AnchorPane implements Initializable {

    public static Stage WindowDFAccuracy;
    static Scene FrontEndDFAccuracy;

    @FXML public DFAccuracyControl ucDFAccuracy;


    private List<IWndEvents> listenersDFAccuracy = new ArrayList<IWndEvents>();
    public void addListenerDFAccuracy(IWndEvents toAdd) {
        listenersDFAccuracy.add(toAdd);
    }

    public DFAccuracyWnd() {

        InitDFAccuracyWnd();

        WindowDFAccuracy.setTitle("Точность пеленгования");
     //   WindowDFAccuracy.getIcons().add(new Image(getClass().getResourceAsStream("Ico/DFAccuracy.png")));
    }

    private void InitDFAccuracyWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("DFAccuracyWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndDFAccuracy = new Scene(fxmlLoader.load(), 802, 425);
            WindowDFAccuracy = new Stage();
            WindowDFAccuracy.initModality(Modality.WINDOW_MODAL);
            WindowDFAccuracy.setResizable(false);
            WindowDFAccuracy.setScene(FrontEndDFAccuracy);
            WindowDFAccuracy.show();

            WindowDFAccuracy.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing
                    for (IWndEvents DFAccuracyEvent : listenersDFAccuracy)
                        DFAccuracyEvent.OnCloseDFAccuracy();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
