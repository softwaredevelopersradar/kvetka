package Kvetka;

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


import java.io.IOException;
import java.lang.reflect.Array;

public class Main extends Application {

    static Stage Window;
    static Scene FrontEnd;

    @Override
    public void start(Stage primaryStage) throws Exception{

        ArcGISRuntimeEnvironment.setInstallDirectory("100.11.2");

        Window = primaryStage;
        Initialize();

        Window.setTitle("Кветка");
        Window.getIcons().add(new Image(getClass().getResourceAsStream("Resources/IcoKvetka.png")));
        Window.setResizable(true);
        Window.setScene(FrontEnd);
        Window.setMaximized(true);
        Window.show();

        Window.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
    }

    private void Initialize() throws IOException {

        FrontEnd = new Scene(FXMLLoader.load(getClass().getResource("MainWnd.fxml")));
    }


    public static void main(String[] args) {
        launch(args);
    }


}