package Kvetka;

import AmplifiersControl.*;
import AmplifiersControl.StateButton;
import AnalogReconFWSControl.*;
import BPSSControl.*;
import ClientDB.ClientDB;
import ClientDB.eventargs.ConnectionEventArgs;
import ClientDB.eventargs.TableEventArgs;
import ClientDB.exceptions.ExceptionClient;
import ClientDB.interfaces.IClientEventsListener;
import ClientDB.interfaces.IDependentAsp;
import ConnectionControl.ConnectionControl;
import ConnectionControl.LedStates;
import ConnectionControl.IConnectionEvent;
import DFAccuracyControl.DFTableModel;
import DFAccuracyControl.IDFAccuracyEvent;
import DigitalReconFWSControl.DigitalReconFWSControl;
import GrpcClientKvetka.ClientKvetka;
import GrpcClientKvetka.ICallbackBora;
import GrpcClientKvetka.ICallbackBpss;
import GrpcClientKvetka.ICallbackClientKvetka;
import Kvetka.BPSS.BPSSWnd;
import Kvetka.DFAccuracy.DFAccuracyWnd;
import Kvetka.DirectionFinder.DirectionFinderWnd;
import Kvetka.Interfaces.IMapRouteEvents;
import Kvetka.Interfaces.IWndEvents;
import Kvetka.Map.MapOperations;
import Kvetka.Map.MapWnd;
import Kvetka.Map.operations.signalSources.BearingParameters;
import Kvetka.Map.operations.situation.SituationProperties;
import Kvetka.PlayerWAV.WAVPlayerWnd;
import Kvetka.Properties.PropertiesOperations;
import Kvetka.TablesOperations.*;
import KvetkaModels.*;
import KvetkaModels.Led;
import KvetkaSpectrumLib.Exceptions.NotFoundHeadException;
import KvetkaSpectrumLib.Interfaces.*;
import KvetkaSpectrumLib.Pane_Spectr;
import PackageBora.Bora;
import PackageBpss.Bpss;
import PackageJSG.JSG;
import PanelBModesControl.ITBModeEvent;
import PanelBModesControl.PanelBModesControl;
import PanelBModesControl.TButtonMode;
import PanelBModesControl.TButtonModeEvent;
import PropertiesControl.IPropertiesEvent;
import PropertiesControl.LocalSettings;
import PropertiesControl.PropertiesControl;
import PropertiesControl.*;
import ReconFHSSControl.ReconFHSSControl;
import ReconFWSControl.ReconFWSControl;
import ReconFWSDistribControl.ReconFWSDistribControl;
import SectorsControl.SectorsControl;
import SpecFreqsControl.SpecFreqsControl;
import StationsControl.StationsControl;
import SuppressFHSSControl.SuppressFHSSControl;
import SuppressFWSControl.SuppressFWSControl;
import TransmissionPackageKvetka.Kvetka;
import ValuesCorrectLib.Events.ITableEvents;
import ValuesCorrectLib.Events.TableEvents;
import ValuesCorrectLib.Properties.Settings;
import ValuesCorrectLib.Properties.PropertiesStations;
import com.google.protobuf.Timestamp;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleButton;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class MainWnd implements ICallbackClientKvetka, ICallbackBora, ICallBackCommonChartEvents, ICallBackSignalEvents,
        IPropertiesEvent, ITBModeEvent, ITableEvents, ISoundEvents, IConnectionEvent, IClientEventsListener, ICallBackAthenuators,
        ICallBackFilters, ICallBackSound, IWndEvents, IMapRouteEvents, IAmplifiersEvents, IBPSSEvents, ICallbackBpss, IDFAccuracyEvent,
        Initializable {

    //region Controls, Containers
    @FXML
    public ToggleButton tbProperty;

    @FXML
    public ToggleButton tbRIwithBearing;
    @FXML
    public ToggleButton tbSearchFHSS;
    @FXML
    public ToggleButton tbSuppressFWS;
    @FXML
    public ToggleButton tbSuppressFHSS;

    @FXML
    public ToggleButton tbShowMap;
    @FXML
    public ToggleButton tbShowDirectionFinder;
    @FXML
    public ToggleButton tbShowBPSS;

    @FXML
    public ToggleButton tbShowPlayer;

    @FXML
    public ToggleButton tbShowPanelPanorama;
    @FXML
    public ToggleButton tbShowPanelTablesRES;
    @FXML
    public ToggleButton tbShowPanelTablesASPSpecFreqs;
    @FXML
    public Button bShowPanelsDefault;

    @FXML
    public ToggleButton tbDFAccuracy;
    @FXML
    public Button bTEST;

    @FXML
    public SplitPane splitProperty;
    @FXML
    public SplitPane splitCenterOfBorderPane;
    @FXML
    public SplitPane splitPaneTables;

    @FXML
    public Label lTimeWorking;
    // endregion

    // region Variables Database
    private static String user = "Kvetka";
    //    private static String serverIp = "localhost";
//    private static int serverPort = 30051;
    public ClientDB clientDB;
    // endregion

    // region Variables ClientKvetka
//    private static String clientKvetkaIp = "127.0.0.1";
//    private static int clientKvetkaPort = 5900;
    private static String userClientKvetka;

    public static String getUserClientKvetka() {return userClientKvetka;}

    public static void setUserClientKvetka(String userClientKvetka) {MainWnd.userClientKvetka = userClientKvetka;}


    private Thread narrowSpectrumTask =  null;
    private Thread spectrumThread;
    public ClientKvetka clientKvetka;
//    private boolean isRunning = false;
    // endregion

    // region Sound
    private final SoundBora soundBora = new SoundBora();
    // endregion

    // region UserControls
    @FXML
    public Pane_Spectr ucPaneSpectr;
    @FXML
    public PanelBModesControl ucPanelBModes;
    @FXML
    public PropertiesControl ucProperties;
    @FXML
    public SpecFreqsControl ucFreqsForbidden;
    @FXML
    public SpecFreqsControl ucFreqsKnown;
    @FXML
    public SpecFreqsControl ucFreqsImportant;
    @FXML
    public SpecFreqsControl ucRangesRI;
    @FXML
    public SpecFreqsControl ucRangesRS;
    @FXML
    public StationsControl ucStations;
    @FXML
    public SectorsControl ucSectors;
    @FXML
    public SuppressFWSControl ucSuppressFWS;
    @FXML
    public SuppressFHSSControl ucSuppressFHSS;
    @FXML
    public ReconFWSControl ucReconFWS;
    @FXML
    public DigitalReconFWSControl ucDigitalReconFWS;
    @FXML
    public AnalogReconFWSControl ucAnalogReconFWS;
    @FXML
    public ReconFWSDistribControl ucReconFWSDistrib;
    @FXML
    public ReconFHSSControl ucReconFHSS;
    @FXML
    public ConnectionControl ucServerDBConnection;
    @FXML
    public ConnectionControl ucServerDAPConnection;
    @FXML
    public ConnectionControl ucBoraState;
    @FXML
    public ConnectionControl ucVereskState;
    @FXML
    public ConnectionControl ucPelNarrowBandState;
    @FXML
    public ConnectionControl ucPelWideBandState;
    @FXML
    public AmplifiersControl ucAmplifiers;
//    @FXML public BPSSControl ucBPSS;
    // endregion

    // region Lists
    List<GlobalPropertiesModel> listGlobalProperties = new ArrayList<>();
    List<SpecFreqsModel> listFreqsForbidden = new ArrayList<>();
    List<SpecFreqsModel> listFreqsKnown = new ArrayList<>();
    List<SpecFreqsModel> listFreqsImportant = new ArrayList<>();
    List<SpecFreqsModel> listRangesRI = new ArrayList<>();
    List<SpecFreqsModel> listRangesRS = new ArrayList<>();
    List<StationsModel> listStations = new ArrayList<>();
    List<SectorsModel> listSectors = new ArrayList<>();
    List<SuppressFWSModel> listSuppressFWS = new ArrayList<>();
    List<SuppressFHSSModel> listSuppressFHSS = new ArrayList<>();
    List<TempSuppressFWSModel> listTempSuppressFWS = new ArrayList<>();
    List<TempSuppressFHSSModel> listTempSuppressFHSS = new ArrayList<>();
    List<SuppressFHSSExcludedModel> listFHSSExcluded = new ArrayList<>();
    List<ReconFWSModel> listReconFWS = new ArrayList<>();
    List<DigitalReconFWSModel> listDigitalReconFWS = new ArrayList<>();
    List<AnalogReconFWSModel> listAnalogReconFWS = new ArrayList<>();
    List<ReconFWSDistribModel> listReconFWSDistrib = new ArrayList<>();
    List<FHSSModel> listReconFHSS = new ArrayList<>();
    List<RouteModel> listRoutes = new ArrayList<>();
    AmplifiersModel amplifiersModel = new AmplifiersModel();
    ButtonsModel buttonModel = new ButtonsModel();
    BPSSModel bpssModelL1 = new BPSSModel();
    BPSSModel bpssModelL2 = new BPSSModel();
    List<DFTableModel> listDFTable = new ArrayList<>();

    // endregion

    MapWnd mapWnd;
    DirectionFinderWnd directionFinderWnd;
    BPSSWnd BPSSWnd;
    DFAccuracyWnd DFAccuracyWnd;
    WAVPlayerWnd WAVPlayerWnd;

    Timer timerBPSS;
    TimerTask timerTaskBPSS;

    public MainWnd() {

    }

    public void RIwithBearing_Click(ActionEvent actionEvent) {

        try {

            if (tbRIwithBearing.isSelected()) {

                tbRIwithBearing.setSelected(false);

                if (ucPanelBModes.tbPreparation.isSelected() || ucPanelBModes.tbSuppress.isSelected()) {

                    clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.RadioIntelligenceWithBearing).build());
                }
                if (ucPanelBModes.tbRecon.isSelected()) {

                    clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.RadioIntelligenceWithBearing).build());
                }
            } else {

                clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder()
                        .setMode(Kvetka.DspServerMode.RadioIntelligence).build());
            }

        } catch (Exception ex) {

        }
    }

    public void SearchFHSS_Click(ActionEvent actionEvent) {

        try {

            if (tbSearchFHSS.isSelected()) {

                tbSearchFHSS.setSelected(false);

                if (ucPanelBModes.tbPreparation.isSelected() || ucPanelBModes.tbSuppress.isSelected()) {

                    clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder().setMode(Kvetka.DspServerMode.RadioIntelligenceFFHS).build());
                }
                if (ucPanelBModes.tbRecon.isSelected()) {

                    clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder().setMode(Kvetka.DspServerMode.RadioIntelligenceFFHS).build());
                }
            } else {

                if (tbRIwithBearing.isSelected()) {

                    clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder().setMode(Kvetka.DspServerMode.RadioIntelligenceWithBearing).build());
                } else {

                    clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder().setMode(Kvetka.DspServerMode.RadioIntelligence).build());
                }

            }

        } catch (Exception ex) {

        }
    }

    public void SuppressFWS_Click(ActionEvent actionEvent) {

        try {

            if (tbSuppressFWS.isSelected()) {

                tbSuppressFWS.setSelected(false);

                if (ucPanelBModes.tbPreparation.isSelected() || ucPanelBModes.tbRecon.isSelected()) {

                    clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.RadioJammingFrs).build());
                }
                if (ucPanelBModes.tbSuppress.isSelected()) {

                    clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.RadioJammingFrs).build());
                }
            } else {

                clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder()
                        .setMode(Kvetka.DspServerMode.RadioJammingFrs).build());
            }

        } catch (Exception ex) {

        }
    }

    public void SuppressFHSS_Click(ActionEvent actionEvent) {

        try {

            if (tbSuppressFHSS.isSelected()) {

                tbSuppressFHSS.setSelected(false);

                if (ucPanelBModes.tbPreparation.isSelected() || ucPanelBModes.tbRecon.isSelected()) {

                    clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.RadioJammingFhss).build());
                }
                if (ucPanelBModes.tbSuppress.isSelected()) {

                    clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.RadioJammingFhss).build());
                }
            } else {

                clientKvetka.setPlannedModeAsync(Kvetka.SetModeRequest.newBuilder()
                        .setMode(Kvetka.DspServerMode.RadioJammingFrs).build());
            }

        } catch (Exception ex) {

        }
    }

    public void ShowMap_Click(ActionEvent actionEvent) throws IOException {

        if (tbShowMap.isSelected()) {

            if (mapWnd == null) {

                mapWnd = new MapWnd();
                mapWnd.addListenerMap(this);
                if(mapWnd.isLocalUpdate) {
                    ucProperties.setLocalSettings(PropertiesOperations.getLocalProperties());
                    mapWnd.isLocalUpdate = false;
                }
                mapWnd.ucRoutes.addListener(this);
                mapWnd.ucRoutes.UpdateRoutes(listRoutes);
                mapWnd.AddListenerRoute(this);
                mapWnd.UpdateGraphicsFromDB(listRoutes);
                mapWnd.SetReconFWSDistribOnMap(listReconFWSDistrib);
                mapWnd.setSupressFWSSignalSourcesOnMap(listSuppressFWS);
                mapWnd.SetReconFHSS(listReconFHSS);
                mapWnd.setSituationProperties(SituationProperties.YamlLoad());
                //  mapWnd.ucAirplanes.addListener(this);
            } else {

                mapWnd.WindowMap.show();
            }
        } else {

            mapWnd.WindowMap.hide();//.close();
        }
    }

    public void ShowDirectionFinder_Click(ActionEvent actionEvent) throws IOException {

        if (tbShowDirectionFinder.isSelected()) {

            if (directionFinderWnd == null) {

                directionFinderWnd = new DirectionFinderWnd();
                directionFinderWnd.addListenerDirectionFinder(this);
                if(directionFinderWnd.isLocalUpdate) {
                    ucProperties.setLocalSettings(PropertiesOperations.getLocalProperties());
                    directionFinderWnd.isLocalUpdate = false;
                }
                directionFinderWnd.ucReconFWS.addListener(this);
                directionFinderWnd.ucReconFWS.setListFreqsForbidden(listFreqsForbidden);
                directionFinderWnd.ucReconFWS.setListFreqsKnown(listFreqsKnown);
                directionFinderWnd.ucReconFWS.setListFreqsImportant(listFreqsImportant);

                directionFinderWnd.ucPaneSpectrPelengs.setAmountDisplayedOldPelengs(25); // Установка количества накоплений старых пеленгов
//                directionFinderWnd.ucPaneSpectrPelengs.setAmountDisplayedOldPelengs(30); // Установка количества накоплений старых пеленгов
            } else {

                directionFinderWnd.WindowDirectionFinder.show();
            }
        } else {

            directionFinderWnd.WindowDirectionFinder.hide();//.close();
        }
    }

    public void ShowBPSS_Click(ActionEvent actionEvent) throws InterruptedException {

        if (tbShowBPSS.isSelected()) {

            BPSSWnd = new BPSSWnd();
            BPSSWnd.addListenerBPSS(this);
            BPSSWnd.ucBPSS.addListenerProperty(this);

            if (clientKvetka != null) {

                clientKvetka.getBpssAntennaStateAsync();
                Thread.sleep(300);
//                clientKvetka.getBpssLitterStateAsync();

                timerBPSS = new Timer();
                timerTaskBPSS = new TimerTask()
                {
                    public void run()
                    {
                        clientKvetka.getBpssLitterStateAsync();
                    }
                };
                timerBPSS.schedule(timerTaskBPSS, 10000);
            }
        } else {

            timerTaskBPSS.cancel();
            timerBPSS.cancel();
            BPSSWnd.WindowBPSS.close();

            if (clientKvetka != null) {

                clientKvetka.setBpssRadiationAsync(Bpss.SetRadiationRequest.newBuilder().setState(false).build());
                Thread.sleep(300);
                clientKvetka.setBpssAntennaAsync(Bpss.AntennaStateMessage.newBuilder().setAntennaState(Bpss.AntennaState.LOG).build());
            }
        }
    }

    public void ShowPlayer_Click(ActionEvent actionEvent) throws InterruptedException {

        if (tbShowPlayer.isSelected()) {

            WAVPlayerWnd = new WAVPlayerWnd();
            WAVPlayerWnd.addListenerPlayer(this);
        } else {

            WAVPlayerWnd.WindowPlayer.close();
        }
    }

    public void Property_Click(ActionEvent actionEvent) {

        if (tbProperty.isSelected())
            splitProperty.setDividerPosition(0, 0.2);
        else
            splitProperty.setDividerPosition(0, 0);
    }

    public void DFAccuracy_Click(ActionEvent actionEvent) {

        if (tbDFAccuracy.isSelected()) {

            DFAccuracyWnd = new DFAccuracyWnd();
            DFAccuracyWnd.addListenerDFAccuracy(this);
            DFAccuracyWnd.ucDFAccuracy.addListener(this);
        } else {

            DFAccuracyWnd.WindowDFAccuracy.close();
        }
    }

    byte[] data;
    int deley = 100;
    int j = 0;
    int k = 0;

    public void Test_Click(ActionEvent actionEvent) throws ExceptionClient {

        ////////////////////////////////////////////////////////////////////////////////////
        Platform.runLater(() -> {

            if (DFAccuracyWnd != null) {

                listDFTable = DFAccuracyWnd.ucDFAccuracy.AddRecToList();

                for (int i = 0; i < listDFTable.size(); i++) {

                    DFAccuracyWnd.ucDFAccuracy.setModelDFTable(listDFTable.get(i));
                }
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////
        Platform.runLater(() -> {

            listReconFHSS = ucReconFHSS.AddRecToList();
            for (int i = 0; i < listReconFHSS.size(); i++) {

                try {
                    clientDB.tables.get(NameTable.TableResFHSS).add(listReconFHSS.get(i));
                } catch (ExceptionClient e) {
                    e.printStackTrace();
                }
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////

        Platform.runLater(() -> {

            listReconFWS = ucReconFWS.AddRecToList();
            for (int i = 0; i < listReconFWS.size(); i++) {

                try {
                    clientDB.tables.get(NameTable.TableResFF).add(listReconFWS.get(i));
                } catch (ExceptionClient e) {
                    e.printStackTrace();
                }
            }
        });

//        if(directionFinderWnd != null) {
//
//            directionFinderWnd.ucReconFWS.UpdateReconFWS(listReconFWS);
//        }
        ////////////////////////////////////////////////////////////////////////////////////
        Platform.runLater(() -> {

            listDigitalReconFWS = ucDigitalReconFWS.AddRecToList();
            for (int i = 0; i < listDigitalReconFWS.size(); i++) {

                DigitalReconFWSModel model = listDigitalReconFWS.get(i);
//                model.setReconFWS(new ReconFWSModel());
                model.setReconFWS(listReconFWS.get(0));
                try {
                    clientDB.tables.get(NameTable.TableDigitalResFF).add(model);
                } catch (ExceptionClient e) {
                    e.printStackTrace();
                }
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////

        Platform.runLater(() -> {

            listAnalogReconFWS = ucAnalogReconFWS.AddRecToList();
            for (int i = 0; i < listAnalogReconFWS.size(); i++) {

                AnalogReconFWSModel model = listAnalogReconFWS.get(i);
                model.setReconFWS(listReconFWS.get(0));
                try {
                    clientDB.tables.get(NameTable.TableAnalogResFF).add(model);
                } catch (ExceptionClient e) {
                    e.printStackTrace();
                }
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////

        amplifiersModel = new AmplifiersModel();
        amplifiersModel.setTurnOn(true);
        amplifiersModel.setCrash(true);
        amplifiersModel.setReady(false);
        amplifiersModel.setCoeff(123);
        amplifiersModel.setOutputRegulator(50);
        amplifiersModel.setTemperature(345);
        amplifiersModel.setPower(45.454);

        ucAmplifiers.UpdateStateButton(true);
        ucAmplifiers.UpdateStateAmplifiers(amplifiersModel);

//        lTimeWorking.setText("2500");
        StartTimeWorking();
        ////////////////////////////////////////////////////////////////////////////////////////

        listReconFWSDistrib = ucReconFWSDistrib.AddRecToList();
        for (int i = 0; i < listReconFWSDistrib.size(); i++) {

            ReconFWSDistribModel model = listReconFWSDistrib.get(i);
            clientDB.tables.get(NameTable.TableResFFDistribution).add(model);
        }
        ////////////////////////////////////////////////////////////////////////////////////////

        if (mapWnd != null) {

            listRoutes = mapWnd.ucRoutes.AddRecToList();
            for (int i = 0; i < listRoutes.size(); i++) {

                clientDB.tables.get(NameTable.TableRoute).add(new TableEvents(listRoutes.get(i)).getRecord());
            }
        }

        ////////////////////////////////////////////////////////////
        Platform.runLater(() -> {

            bpssModelL1.setPower((byte) 1);
            bpssModelL1.setTemperature((short) 3);
            bpssModelL1.setRadiation((byte) 0);
            bpssModelL1.setCurrent((byte) 5);
            bpssModelL1.setError((byte) 6);
            bpssModelL1.setSnt((byte) 7);
            BPSSWnd.ucBPSS.UpdateParamsL1(bpssModelL1);

            bpssModelL2.setPower((byte) 0);
            bpssModelL2.setTemperature((short) 6);
            bpssModelL2.setRadiation((byte) 1);
            bpssModelL2.setCurrent((byte) 4);
            bpssModelL2.setError((byte) 3);
            bpssModelL2.setSnt((byte) 2);
            BPSSWnd.ucBPSS.UpdateParamsL2(bpssModelL2);

            buttonModel.setGPSL1(true);
            buttonModel.setGPSL2(false);
            buttonModel.setGLONASSL1(true);
            buttonModel.setGLONASSL2(true);
            buttonModel.setGNSSL1(true);
            buttonModel.setGNSSL2(false);

            BPSSWnd.ucBPSS.UpdateButtonsBPSS(buttonModel);
        });
        //////////////////////////////////////


//        Runnable task = new Runnable() {
//            //final TimerTask task = new TimerTask() {
//            final int size = 3000;
//            @Override
//            public void run() {
//                while (true){
//
//                    if(k == 100)
//                        deley = 32;
//                    else k++;
//                    try {
//                        Thread.sleep(deley);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    long m = System.currentTimeMillis();
//                    Platform.runLater(() -> {
//                        data = new byte[size];
//                        for(int i = 0; i < data.length; i++)
//                        {
//                            data[i] = (byte) ThreadLocalRandom.current().nextInt(90, 120);
//                        }
//
//                        double v1 = ThreadLocalRandom.current().nextInt(20,90);
//
//                        for(int i = 0; i < 90; i++)
//                        {
//                            double v = Math.sin(Math.toRadians(i * 2)) * v1;
//                            data[data.length/2 + i] = (byte) ThreadLocalRandom.current().nextInt(80 - (int) v,100 - (int) v);
//                        }
//
//
//
//                        ucPaneSpectr.UpdateSpectr(data, 1500 + 500 * j, 2000 + 500 * j);
//                        if(j >= 56)
//                        {
//                            j=0;
//
//                        }
//                        else j++;
//                        data = null;
//                    });
//                }
//            }
//        };
//
//        Thread thread = new Thread(task);
//        thread.start();

        //////////////////////////////////////
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        // region Markup
        splitProperty.getDividers().get(0).positionProperty().addListener((obs, oldPos, newPos) -> {

            tbProperty.setSelected(newPos.doubleValue() > 0.01);
        });
        splitProperty.getDividers().get(0).positionProperty().set(0.0);

        splitCenterOfBorderPane.getDividers().get(0).positionProperty().addListener((obs, oldPos, newPos) -> {

            setDividersPositionCenter(new double[]{oldPos.doubleValue(), newPos.doubleValue()});

            tbShowPanelPanorama.setSelected(newPos.doubleValue() > 0.01);
        });
        splitCenterOfBorderPane.getDividers().get(0).positionProperty().set(0.0);

        splitPaneTables.getDividers().get(0).positionProperty().addListener((obs, oldPos, newPos) -> {

//            if(newPos.doubleValue() == 1.0 || newPos.doubleValue() == 0.0) oldPos = 0.0;

            setDividersPositionTables(new double[]{oldPos.doubleValue(), newPos.doubleValue()});

            splitPaneTables.getDividers().get(0).positionProperty().set(newPos.doubleValue());
        });
        splitPaneTables.getDividers().get(0).positionProperty().set(0.0);

        ShowPanelsDefault_Click(new ActionEvent());
        // endregion

        // region Add Listeners
        ucProperties.addListener(this);
        try {

            PropertiesOperations.setLocalProperties(PropertiesOperations.YamlLoad());
            ucProperties.setLocalSettings(PropertiesOperations.getLocalProperties());

            ucProperties.checkTypes(PropertiesOperations.getLocalProperties().getEd().getType(),
                                    PropertiesOperations.getLocalProperties().getPc().getType(),
                                    PropertiesOperations.getLocalProperties().getSound().getType());

            Settings.setCoordView((PropertiesOperations.getLocalProperties().getGeneral().getCoordinateView().getCoordinateType()));

            bTEST.setVisible(PropertiesOperations.getLocalProperties().getGeneral().getAccess() == AccessEnum.ADMIN ? true : false);
           // tbDFAccuracy.setVisible(PropertiesOperations.getLocalProperties().getGeneral().getAccess() == AccessEnum.ADMIN ? true : false);
        } catch (IOException e) { e.printStackTrace(); }

        ucFreqsForbidden.addListener(this);
        ucFreqsKnown.addListener(this);
        ucFreqsImportant.addListener(this);
        ucRangesRI.addListener(this);
        ucRangesRS.addListener(this);
        ucStations.addListener(this);
        ucSectors.addListener(this);
        ucSuppressFWS.addListener(this);
        ucSuppressFHSS.addListener(this);
        ucServerDBConnection.addListener(this);
        ucServerDAPConnection.addListener(this);
        ucBoraState.addListener(this);
        ucVereskState.addListener(this);
        ucPelNarrowBandState.addListener(this);
        ucPelWideBandState.addListener(this);
        ucReconFHSS.addListener(this);
        ucReconFWS.addListener(this);
        ucDigitalReconFWS.addListener(this);
        ucAnalogReconFWS.addListener(this);
        ucAnalogReconFWS.addListenerSound(this);
        ucReconFWSDistrib.addListener(this);
        ucPanelBModes.addListener(this);
        ucPaneSpectr.registerCallBackCommonChartEvents(this);
        ucPaneSpectr.registerCallBackSignalEvents(this);
        ucPaneSpectr.registerCallBackAthenuators(this);
        ucPaneSpectr.registerCallBackFilters(this);
        ucPaneSpectr.registerCallBackSound(this);
        ucAmplifiers.addListener(this);



        // endregion

    }

    // region Markup Wnd
    public void ShowPanelPanorama_Click(ActionEvent actionEvent) {

        ClickTBMarkupTables();
    }

    public void ShowPanelTablesRES_Click(ActionEvent actionEvent) {

        ClickTBMarkupTables();
    }

    private void ClickTBMarkupTables() {

        if (tbShowPanelPanorama.isSelected() && tbShowPanelTablesRES.isSelected() && tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 0.6);
            splitCenterOfBorderPane.setDividerPosition(0, 0.5);
        }
        if (tbShowPanelPanorama.isSelected() && tbShowPanelTablesRES.isSelected() && !tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 1.0);
            splitCenterOfBorderPane.setDividerPosition(0, 0.5);
        }
        if (tbShowPanelPanorama.isSelected() && !tbShowPanelTablesRES.isSelected() && tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 0.0);
            splitCenterOfBorderPane.setDividerPosition(0, 0.5);
        }
        if (tbShowPanelPanorama.isSelected() && !tbShowPanelTablesRES.isSelected() && !tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 1.0);
            splitCenterOfBorderPane.setDividerPosition(0, 1.0);
        }
        if (!tbShowPanelPanorama.isSelected() && tbShowPanelTablesRES.isSelected() && tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 0.6);
            splitCenterOfBorderPane.setDividerPosition(0, 0.0);
        }
        if (!tbShowPanelPanorama.isSelected() && tbShowPanelTablesRES.isSelected() && !tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 1.0);
            splitCenterOfBorderPane.setDividerPosition(0, 0.0);
        }
        if (!tbShowPanelPanorama.isSelected() && !tbShowPanelTablesRES.isSelected() && tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 0.0);
            splitCenterOfBorderPane.setDividerPosition(0, 0.0);
        }
        if (!tbShowPanelPanorama.isSelected() && !tbShowPanelTablesRES.isSelected() && !tbShowPanelTablesASPSpecFreqs.isSelected()) {

            splitPaneTables.setDividerPosition(0, 0.6);
            splitCenterOfBorderPane.setDividerPosition(0, 0.5);
        }
    }

    public void ShowPanelTablesASPSpecFreqs_Click(ActionEvent actionEvent) {

        ClickTBMarkupTables();
    }

    public void ShowPanelsDefault_Click(ActionEvent actionEvent) {

        splitCenterOfBorderPane.setDividerPosition(0, 0.5);
        splitPaneTables.setDividerPosition(0, 0.6);

        tbShowPanelPanorama.setSelected(true);
        tbShowPanelTablesRES.setSelected(true);
        tbShowPanelTablesASPSpecFreqs.setSelected(true);
    }

    private double[] DividersPositionCenter = new double[2];

    public double[] getDividersPositionCenter() {return DividersPositionCenter;}

    public void setDividersPositionCenter(double[] dividersPositionCenter) {

        DividersPositionCenter = dividersPositionCenter;
        UpdateMarkupCenter();
    }

    private void UpdateMarkupCenter() {

        double[] positionCenter = getDividersPositionCenter();

        if (positionCenter[1] > 0.97 && positionCenter[1] <= 1.0) {

            tbShowPanelTablesRES.setSelected(false);
            tbShowPanelTablesASPSpecFreqs.setSelected(false);
        } else {
            tbShowPanelTablesRES.setSelected(true);
            tbShowPanelTablesASPSpecFreqs.setSelected(true);
        }
    }

    private double[] DividersPositionTables = new double[2];

    public double[] getDividersPositionTables() {return DividersPositionTables;}

    public void setDividersPositionTables(double[] dividersPositionTables) {

        DividersPositionTables = dividersPositionTables;
        UpdateMarkupTables();
    }

    private void UpdateMarkupTables() {

        double[] positionTables = getDividersPositionTables();

        if (positionTables[1] > 0.9 && positionTables[1] <= 1.0) {

            tbShowPanelTablesASPSpecFreqs.setSelected(false);
            tbShowPanelTablesRES.setSelected(true);
        }

        if (positionTables[1] >= 0.0 && positionTables[1] < 0.02) {

            tbShowPanelTablesASPSpecFreqs.setSelected(true);
            tbShowPanelTablesRES.setSelected(false);
        }

        if (positionTables[1] == 0.6) {

            tbShowPanelTablesASPSpecFreqs.setSelected(true);
            tbShowPanelTablesRES.setSelected(true);
        }
    }
    // endregion

    // region Добавить, изменить, удалить, очистить в БД
    @Override
    public void OnAddRecord(TableEvents tableEvents) {

        try {

            switch (tableEvents.getNameTable()) {

                case TableFreqRangesElint -> {

                    if (SpecFreqsOperations.IsAddSpecFreq(listRangesRI, (SpecFreqsModel) tableEvents.getRecord(), false))
                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                }

                case TableFreqRangesJamming -> {

                    if (SpecFreqsOperations.IsAddSpecFreq(listRangesRS, (SpecFreqsModel) tableEvents.getRecord(), false))
                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                }

                case TableFreqKnown -> {

                    if (SpecFreqsOperations.IsAddSpecFreq(listFreqsKnown, (SpecFreqsModel) tableEvents.getRecord(), false))
                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                }

                case TableFreqImportant -> {

                    if (SpecFreqsOperations.IsAddSpecFreq(listFreqsImportant, (SpecFreqsModel) tableEvents.getRecord(), false))
                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                }

                case TableFreqForbidden -> {

                    if (SpecFreqsOperations.IsAddSpecFreq(listFreqsForbidden, (SpecFreqsModel) tableEvents.getRecord(), false))
                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                }

                case TableResFFJam -> {

                    String sMessage = SuppressFWSOperations.IsUpdateTableSuppressFWS(listSuppressFWS, listRangesRS, listFreqsForbidden, listSectors, (SuppressFWSModel) tableEvents.getRecord(), (byte) listGlobalProperties.get(0).getNumberOfResInLetter(), false);
                    if (sMessage == "") {

                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                    } else {

                        GeneralOperations.ShowMessage(sMessage);
                    }
                }

                case TableResFHSSJam -> {

                    if (listSuppressFHSS.size() >= 1) { return; }

                    String sMessage = SuppressFHSSOperations.IsUpdateTableSuppressFHSS(listRangesRS, listFreqsForbidden, listSuppressFHSS, (SuppressFHSSModel) tableEvents.getRecord());
                    if (sMessage == "") {

                        clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
                    } else {

                        GeneralOperations.ShowMessage(sMessage);
                    }
                }

//                case TableResFFDistribution -> {
//
//                    ((ReconFWSDistribModel)tableEvents.getRecord()).setMode(ModeSound.Manual);
//                    ((ReconFWSDistribModel)tableEvents.getRecord()).setMgcCoefficient(PropertiesOperations.getLocalProperties().getSound().getMGCFactor());
//                    clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
//                }

                default -> clientDB.tables.get(tableEvents.getNameTable()).add(tableEvents.getRecord());
            }
        } catch (ExceptionClient exceptionClient) {

            exceptionClient.printStackTrace();
        }
    }

    @Override
    public void OnChangeRecord(TableEvents tableEvents) {

        switch (tableEvents.getNameTable()) {

            case TableFreqRangesElint -> {

                if (SpecFreqsOperations.IsAddSpecFreq(listRangesRI, (SpecFreqsModel) tableEvents.getRecord(), true))
                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
            }

            case TableFreqRangesJamming -> {

                if (SpecFreqsOperations.IsAddSpecFreq(listRangesRS, (SpecFreqsModel) tableEvents.getRecord(), true))
                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
            }

            case TableFreqKnown -> {

                if (SpecFreqsOperations.IsAddSpecFreq(listFreqsKnown, (SpecFreqsModel) tableEvents.getRecord(), true))
                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
            }

            case TableFreqImportant -> {

                if (SpecFreqsOperations.IsAddSpecFreq(listFreqsImportant, (SpecFreqsModel) tableEvents.getRecord(), true))
                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
            }

            case TableFreqForbidden -> {

                if (SpecFreqsOperations.IsAddSpecFreq(listFreqsForbidden, (SpecFreqsModel) tableEvents.getRecord(), true))
                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
            }

            case TableResFFJam -> {

                String sMessage = SuppressFWSOperations.IsUpdateTableSuppressFWS(listSuppressFWS, listRangesRS, listFreqsForbidden, listSectors, (SuppressFWSModel) tableEvents.getRecord(), (byte) listGlobalProperties.get(0).getNumberOfResInLetter(), true);
                if (sMessage == "") {

                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
                } else {

                    GeneralOperations.ShowMessage(sMessage);
                }
            }

            case TableResFHSSJam -> {

                String sMessage = SuppressFHSSOperations.IsUpdateTableSuppressFHSS(listRangesRS, listFreqsForbidden, listSuppressFHSS, (SuppressFHSSModel) tableEvents.getRecord());
                if (sMessage == "") {

                    clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
                } else {

                    GeneralOperations.ShowMessage(sMessage);
                }
            }
            default -> clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
        }
    }

    @Override
    public void OnDeleteRecord(TableEvents tableEvents) {

        switch (tableEvents.getNameTable()) {

            case TableResFF -> {

                if (((ReconFWSModel) tableEvents.getRecord()).getAnalogReconFwsId() != null) {

                    var modelA = listAnalogReconFWS.stream()
                            .filter(x -> (x.getReconFWS() != null)
                                    && (x.getReconFWS().getAnalogReconFwsId() != null)
                                    && (x.getReconFWS().getAnalogReconFwsId().intValue() == ((ReconFWSModel) tableEvents.getRecord()).getAnalogReconFwsId().intValue()))
                            .findFirst()
                            .orElse(null);

                    if(modelA != null){

                        Platform.runLater(() -> {

                            clientDB.tables.get(NameTable.TableAnalogResFF).delete(modelA);
                        });
                    }
                }

                if (((ReconFWSModel) tableEvents.getRecord()).getDigitalReconFwsId() != null) {

                    var modelD = listDigitalReconFWS.stream()
                            .filter(x -> x.getReconFWS() != null
                                    && (x.getReconFWS().getDigitalReconFwsId() != null)
                                    && (x.getReconFWS().getDigitalReconFwsId().intValue() == ((ReconFWSModel) tableEvents.getRecord()).getDigitalReconFwsId().intValue()))
                            .findFirst()
                            .orElse(null);

                    if(modelD != null){

                        Platform.runLater(() -> {

                            clientDB.tables.get(NameTable.TableDigitalResFF).delete(modelD);
                        });
                    }
                }
            }

            default -> {

                clientDB.tables.get(tableEvents.getNameTable()).delete(tableEvents.getRecord());
            }
        }
    }

    @Override
    public void OnDeleteRange(List<DigitalReconFWSModel> list, NameTable nameTable) {

        clientDB.tables.get(nameTable).removeRange(list);
    }

    @Override
    public void OnClearRecords(NameTable nameTable) {

        switch (nameTable) {

            case TableResFF -> {



                    List<AnalogReconFWSModel> listA =  new ArrayList<AnalogReconFWSModel>();
                for (ReconFWSModel item : listReconFWS) {

                    if (item.getAnalogReconFwsId() != null) {

                       var modelA = listAnalogReconFWS.stream()
                               .filter(x -> (x.getReconFWS() != null)
                                       && (x.getReconFWS().getAnalogReconFwsId() != null)
                                       && (x.getReconFWS().getAnalogReconFwsId().intValue() == item.getAnalogReconFwsId().intValue()))
                               .findFirst()
                               .orElse(null);

                       if(modelA != null){

                            listA.add(modelA);
                        }
                    }
                }

                if (listA.size() > 0) {

                    Platform.runLater(() -> {

                        clientDB.tables.get(NameTable.TableAnalogResFF).removeRange(listA);
                    });
                }


                List<DigitalReconFWSModel> listD =  new ArrayList<DigitalReconFWSModel>();
                for (ReconFWSModel item : listReconFWS) {

                    if (item.getDigitalReconFwsId() != null) {

                        var modelD = listDigitalReconFWS.stream()
                                .filter(x -> x.getReconFWS() != null
                                        && (x.getReconFWS().getDigitalReconFwsId() != null)
                                        && (x.getReconFWS().getDigitalReconFwsId().intValue() == item.getDigitalReconFwsId().intValue()))
                                .findFirst()
                                .orElse(null);

                        if(modelD != null){

                            listD.add(modelD);
                        }
                    }
                }

                if (listD.size() > 0) {

                    Platform.runLater(() -> {

                        clientDB.tables.get(NameTable.TableDigitalResFF).removeRange(listD);
                    });
                }
            }

            default -> {

                clientDB.tables.get(nameTable).clear();
            }
        }

    }

    @Override
    public void OnSelectedStation(int id) {

        if (id > 0) {

            LoadTablesByFilter(id);
        }
    }

    /**
     * Исполнительное пеленгование
     * @param tableEvents
     */
    @Override
    public void OnGetExecBear(TableEvents tableEvents) {

        clientDB.tables.get(tableEvents.getNameTable()).change(tableEvents.getRecord());
    }

    @Override
    public void OnDoubleClickRow(TableEvents tableEvents) {

        switch (tableEvents.getNameTable()) {

            case TableResFFDistribution -> {

                //todo: Draw bearing to map
                ReconFWSDistribModel model = (ReconFWSDistribModel) tableEvents.getRecord();
                BearingParameters params = new BearingParameters() { };
                params.setBearing(model.getBearing());
                params.setBearingLength(Integer.valueOf(ucProperties.getDistanceBearingLine().getText()));
                params.setLatitude(model.getCoordinates().latitude);
                params.setLongitude(model.getCoordinates().longitude);

                if (mapWnd != null) { mapWnd.DrawingBearing(params); }
            }

            case TableResFF -> {

                if (directionFinderWnd != null) { directionFinderWnd.ucChartPolar.UpdateArrow(((ReconFWSModel) tableEvents.getRecord()).getBearing1()); }

                ReconFWSModel model = (ReconFWSModel) tableEvents.getRecord();
                BearingParameters params = new BearingParameters() { };
                params.setBearing(model.getBearing1());
                params.setBearingLength(Integer.valueOf(ucProperties.getDistanceBearingLine().getText()));
                params.setLatitude(model.getCoordinates().latitude);
                params.setLongitude(model.getCoordinates().longitude);

                if (mapWnd != null) { mapWnd.DrawingBearing(params); }
            }

            case TableAnalogResFF -> {

                if (directionFinderWnd != null) { directionFinderWnd.ucChartPolar.UpdateArrow(((AnalogReconFWSModel) tableEvents.getRecord()).getBearing1()); }

                AnalogReconFWSModel model = (AnalogReconFWSModel) tableEvents.getRecord();
                BearingParameters params = new BearingParameters() { };
                params.setBearing(model.getBearing1());
                params.setBearingLength(Integer.valueOf(ucProperties.getDistanceBearingLine().getText()));
                params.setLatitude(model.getCoordinates().latitude);
                params.setLongitude(model.getCoordinates().longitude);

                if (mapWnd != null) { mapWnd.DrawingBearing(params); }
            }

            case TableDigitalResFF -> {

                if (directionFinderWnd != null) { directionFinderWnd.ucChartPolar.UpdateArrow(((DigitalReconFWSModel) tableEvents.getRecord()).getBearingOwn()); }

                DigitalReconFWSModel model = (DigitalReconFWSModel) tableEvents.getRecord();
                BearingParameters params = new BearingParameters() { };
                params.setBearing(model.getBearingOwn());
                params.setBearingLength(Integer.valueOf(ucProperties.getDistanceBearingLine().getText()));
                params.setLatitude(model.getCoordinates().latitude);
                params.setLongitude(model.getCoordinates().longitude);

                if (mapWnd != null) { mapWnd.DrawingBearing(params); }
            }
        }
    }

    /**
     * Звуковая обработка (Отправить из таблиц ФРЧ в ucPaneSpectr)
     * @param frequency
     */
    @Override
    public void OnSoundProcessing(double frequency) {

        Platform.runLater(() -> {

            ucPaneSpectr.SetFrequencyOnSoundControl(frequency);
            ucPaneSpectr.getSmallChart().setFrequencyInSoundControl(frequency);
        });

    }
    // endregion

    // region ClientDB Tables
    void InitClientDB() {

        try {

            clientDB.clientEvents.addListener(this);

            clientDB.tables.get(NameTable.TableJammer).onUpdatedTable.addListener((Consumer<TableEventArgs<StationsModel>>) ((tableStations) -> UpdateStations(tableStations)));

            clientDB.tables.get(NameTable.TableFreqForbidden).onUpdatedTable.addListener((Consumer<TableEventArgs<FreqsForbiddenModel>>) ((tableSpecFreqs) -> UpdateSpecFreqs(tableSpecFreqs)));
            clientDB.tables.get(NameTable.TableFreqImportant).onUpdatedTable.addListener((Consumer<TableEventArgs<FreqsImportantModel>>) ((tableSpecFreqs) -> UpdateSpecFreqs(tableSpecFreqs)));
            clientDB.tables.get(NameTable.TableFreqKnown).onUpdatedTable.addListener((Consumer<TableEventArgs<FreqsKnownModel>>) ((tableSpecFreqs) -> UpdateSpecFreqs(tableSpecFreqs)));
            clientDB.tables.get(NameTable.TableFreqRangesElint).onUpdatedTable.addListener((Consumer<TableEventArgs<RangesRIModel>>) ((tableSpecFreqs) -> UpdateSpecFreqs(tableSpecFreqs)));
            clientDB.tables.get(NameTable.TableFreqRangesJamming).onUpdatedTable.addListener((Consumer<TableEventArgs<RangesRSModel>>) ((tableSpecFreqs) -> UpdateSpecFreqs(tableSpecFreqs)));

            clientDB.tables.get(NameTable.TableSectorsElint).onUpdatedTable.addListener((Consumer<TableEventArgs<SectorsModel>>) ((tableSectors) -> UpdateSectors(tableSectors)));

            clientDB.tables.get(NameTable.TableResFFJam).onUpdatedTable.addListener((Consumer<TableEventArgs<SuppressFWSModel>>) ((tableSuppressFWS) -> UpdateSuppressFWS(tableSuppressFWS)));
            clientDB.tables.get(NameTable.TempSuppressFF).onUpdatedTable.addListener((Consumer<TableEventArgs<TempSuppressFWSModel>>) ((tableSuppressFWS) -> UpdateSuppressFWS(tableSuppressFWS)));

            clientDB.tables.get(NameTable.TableResFHSSJam).onUpdatedTable.addListener((Consumer<TableEventArgs<SuppressFHSSModel>>) ((tableSuppressFHSS) -> UpdateSuppressFHSS(tableSuppressFHSS)));
            clientDB.tables.get(NameTable.TempSuppressFHSS).onUpdatedTable.addListener((Consumer<TableEventArgs<TempSuppressFHSSModel>>) ((tableSuppressFHSS) -> UpdateSuppressFHSS(tableSuppressFHSS)));
            clientDB.tables.get(NameTable.TableFHSSExcludedJam).onUpdatedTable.addListener((Consumer<TableEventArgs<SuppressFHSSExcludedModel>>) ((tableFHSSExcluded) -> UpdateFHSSExcluded(tableFHSSExcluded)));

            clientDB.tables.get(NameTable.TableResFHSS).onUpdatedTable.addListener((Consumer<TableEventArgs<FHSSModel>>) ((tableReconFHSS) -> UpdateReconFHSS(tableReconFHSS)));

            clientDB.tables.get(NameTable.TableResFF).onUpdatedTable.addListener((Consumer<TableEventArgs<ReconFWSModel>>) ((tableReconFWS) -> UpdateReconFWS(tableReconFWS)));
            clientDB.tables.get(NameTable.TableDigitalResFF).onUpdatedTable.addListener((Consumer<TableEventArgs<DigitalReconFWSModel>>) ((tableDigitalReconFWS) -> UpdateDigitalReconFWS(tableDigitalReconFWS)));
            clientDB.tables.get(NameTable.TableAnalogResFF).onUpdatedTable.addListener((Consumer<TableEventArgs<AnalogReconFWSModel>>) ((tableAnalogReconFWS) -> UpdateAnalogReconFWS(tableAnalogReconFWS)));

            clientDB.tables.get(NameTable.TableResFFDistribution).onUpdatedTable.addListener((Consumer<TableEventArgs<ReconFWSDistribModel>>) ((tableReconFWSDistrib) -> UpdateReconFWSDistrib(tableReconFWSDistrib)));

            clientDB.tables.get(NameTable.TableGlobalProperties).onUpdatedTable.addListener((Consumer<TableEventArgs<GlobalPropertiesModel>>) ((tableGlobalProperties) -> UpdateGlobalProperties(tableGlobalProperties)));

            clientDB.tables.get(NameTable.TableRoute).onUpdatedTable.addListener((Consumer<TableEventArgs<RouteModel>>) ((tableRoutes) -> UpdateRoutes(tableRoutes)));
        } catch (Exception ex) {

        }
    }

    // region Properties
    private void UpdateGlobalProperties(TableEventArgs<GlobalPropertiesModel> tableGlobalProperties) {

        Platform.runLater(() -> {

            listGlobalProperties = tableGlobalProperties.getTable();
            ucProperties.setGlobalSettings(listGlobalProperties.get(0));
        });
    }

    @Override
    public void OnLocalProperties(LocalSettings localSettings) {

        try {

            PropertiesOperations.YamlSave(localSettings);

//            bTEST.setVisible(PropertiesOperations.getLocalProperties().getGeneral().getAccess() == AccessEnum.ADMIN ? true : false);
            tbDFAccuracy.setVisible(PropertiesOperations.getLocalProperties().getGeneral().getAccess() == AccessEnum.ADMIN ? true : false);

            Settings.setCoordView((localSettings.getGeneral().getCoordinateView().getCoordinateType()));
            ucStations.UpdateStations(listStations);
            ucReconFWSDistrib.setListReconFWSDistrib(listReconFWSDistrib);
            ucSuppressFWS.UpdateSuppressFWS(listSuppressFWS);
            if(mapWnd != null) {
                mapWnd.ucAirplanes.UpdateAirplanes(mapWnd.listAirplanes);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void OnGlobalProperties(GlobalPropertiesModel globalPropertiesModel) {

        try {

            clientDB.tables.get(NameTable.TableGlobalProperties).add(globalPropertiesModel);
        } catch (ExceptionClient exceptionClient) {

            exceptionClient.printStackTrace();
        }
    }

    // endregion

    // region ИРИ ФРЧ
    /**
     * ИРИ ФРЧ Пеленгатор
     *
     * @param tableReconFWS
     */
    private void UpdateReconFWS(TableEventArgs<ReconFWSModel> tableReconFWS) {

        Platform.runLater(() -> {

            listReconFWS = ReconOperations.SelectedReconFWS(listSectors, tableReconFWS.getTable());
            ucReconFWS.UpdateReconFWS(listReconFWS);

            if (directionFinderWnd != null) {

                directionFinderWnd.ucReconFWS.UpdateReconFWS(listReconFWS);
            }

//            listReconFWS = tableReconFWS.getTable();
//            ucReconFWS.UpdateReconFWS(listReconFWS);
//
//            if (directionFinderWnd != null) {
//
//                directionFinderWnd.ucReconFWS.UpdateReconFWS(listReconFWS);
//            }
        });
    }

    /**
     * ИРИ ФРЧ Цифра
     *
     * @param tableDigitalReconFWS
     */
    private void UpdateDigitalReconFWS(TableEventArgs<DigitalReconFWSModel> tableDigitalReconFWS) {

        Platform.runLater(() -> {

            listDigitalReconFWS = ReconOperations.SelectedDigitalReconFWS(listSectors, tableDigitalReconFWS.getTable());
            ucDigitalReconFWS.UpdateDigitalReconFWS(listDigitalReconFWS);

//            listDigitalReconFWS = tableDigitalReconFWS.getTable();
//            ucDigitalReconFWS.UpdateDigitalReconFWS(listDigitalReconFWS);
        });
    }

    /**
     * ИРИ ФРЧ Аналог
     *
     * @param tableAnalogReconFWS
     */
    private void UpdateAnalogReconFWS(TableEventArgs<AnalogReconFWSModel> tableAnalogReconFWS) {

        Platform.runLater(() -> {

            listAnalogReconFWS = ReconOperations.SelectedAnalogReconFWS(listSectors, tableAnalogReconFWS.getTable());
            ucAnalogReconFWS.setListAnalogReconFWS(listAnalogReconFWS);

//            listAnalogReconFWS = tableAnalogReconFWS.getTable();
//            ucAnalogReconFWS.setListAnalogReconFWS(listAnalogReconFWS);
        });
    }

    @Override
    public void OnSoundListen(AnalogReconFWSModel analogReconFWSModel, boolean isListen) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.getSoundRequestAsync(Bora.SoundRequest.newBuilder()
                                .setId(analogReconFWSModel.getId())
                                .setCentralFrequencyKhz(analogReconFWSModel.getFrequency())
                                .setDemodulation(analogReconFWSModel.getTypeSignal())
                                .setBandwidthKhz(analogReconFWSModel.getBandAudio())
                                .setSampleRate(PropertiesOperations.getLocalProperties().getSound().getSound())
                                .setMode(PropertiesOperations.getLocalProperties().getSound().getType() == 1 ? Bora.GainControl.Auto : Bora.GainControl.Manual)
                                .setAgcLevel(PropertiesOperations.getLocalProperties().getSound().getAGLLevel())
                                .setMgcCoef(PropertiesOperations.getLocalProperties().getSound().getMGCFactor())
                                .setStartTime(Timestamp.getDefaultInstance())
                                .setDurationMs(1000)
                                .setIsListen(isListen)
                                .build());
            }
        });
    }

    @Override
    public void OnSoundWriteFile(AnalogReconFWSModel analogReconFWSModel, boolean isWriteFile) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.setTimeDomainAsync(Bora.SoundRequest.newBuilder()
                        .setId(analogReconFWSModel.getId())
                        .setCentralFrequencyKhz(analogReconFWSModel.getFrequency())
                        .setDemodulation(analogReconFWSModel.getTypeSignal())
                        .setBandwidthKhz(analogReconFWSModel.getBandAudio())
                        .setSampleRate(PropertiesOperations.getLocalProperties().getSound().getSound())
                        .setMode(PropertiesOperations.getLocalProperties().getSound().getType() == 1 ? Bora.GainControl.Auto : Bora.GainControl.Manual)
                        .setAgcLevel(PropertiesOperations.getLocalProperties().getSound().getAGLLevel())
                        .setMgcCoef(PropertiesOperations.getLocalProperties().getSound().getMGCFactor())
                        .setStartTime(Timestamp.getDefaultInstance())
                        .setDurationMs(1000)
                        .setWriteFile(isWriteFile)
                        .build());
            }
        });
    }
    // endregion

    // region ИРИ ФРЧ ЦР
    private void UpdateReconFWSDistrib(TableEventArgs<ReconFWSDistribModel> tableReconFWSDistrib) {

        Platform.runLater(() -> {

            listReconFWSDistrib = tableReconFWSDistrib.getTable();
            ucReconFWSDistrib.setListReconFWSDistrib(listReconFWSDistrib);

            if (mapWnd != null) {

                mapWnd.UpdateReconFWSDistribOnMap(listReconFWSDistrib);
            }
        });
    }
    // endregion

    // region ИРИ ППРЧ
    private void UpdateReconFHSS(TableEventArgs<FHSSModel> tableReconFHSS) {

        Platform.runLater(() -> {

            listReconFHSS = tableReconFHSS.getTable();
            ucReconFHSS.UpdateReconFHSS(listReconFHSS);

            if (mapWnd != null) {

                mapWnd.UpdateReconFHSS(listReconFHSS);
            }
        });
    }
    // endregion

    // region ИРИ ППРЧ РП
    private void UpdateSuppressFHSS(TableEventArgs<?> tableSuppressFHSS) {

        Platform.runLater(() -> {

            switch (tableSuppressFHSS.getType().getAnnotation(InfoTable.class).name()) {

                case TableResFHSSJam:

                    listSuppressFHSS = (List<SuppressFHSSModel>) tableSuppressFHSS.getTable();
                    ucSuppressFHSS.setListSuppressFHSS(listSuppressFHSS.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));
                    break;

                case TempSuppressFHSS:

                    listTempSuppressFHSS = (List<TempSuppressFHSSModel>) tableSuppressFHSS.getTable();
                    ucSuppressFHSS.UpdateTempSuppressFHSS(
                            listTempSuppressFHSS.stream()
                                    .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                                    .collect(Collectors.toList()),
                            listSuppressFHSS.stream()
                                    .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                                    .collect(Collectors.toList()));
                    break;
            }
        });
    }

    /**
     * Выколотые частоты
     *
     * @param tableFHSSExcluded
     */
    private void UpdateFHSSExcluded(TableEventArgs<SuppressFHSSExcludedModel> tableFHSSExcluded) {

        Platform.runLater(() -> {

            listFHSSExcluded = (List<SuppressFHSSExcludedModel>) tableFHSSExcluded.getTable();
            ucSuppressFHSS.UpdateFHSSExcluded(listFHSSExcluded);
        });
    }
    // endregion

    // region ИРИ ФРЧ РП
    private void UpdateSuppressFWS(TableEventArgs<?> tableSuppressFWS) {

        Platform.runLater(() -> {

            switch (tableSuppressFWS.getType().getAnnotation(InfoTable.class).name()) {

                case TableResFFJam:

                    listSuppressFWS = (List<SuppressFWSModel>) tableSuppressFWS.getTable();
                    ucSuppressFWS.UpdateSuppressFWS(listSuppressFWS.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));

                    if (mapWnd != null) {
                        mapWnd.updateSupressFWSSignalSourcesOnMap(listSuppressFWS);
                    }
                    break;

                case TempSuppressFF:

                    listTempSuppressFWS = (List<TempSuppressFWSModel>) tableSuppressFWS.getTable();
                    ucSuppressFWS.UpdateTempSuppressFWS(
                            listTempSuppressFWS.stream()
                                    .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                                    .collect(Collectors.toList()),
                            listSuppressFWS.stream()
                                    .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                                    .collect(Collectors.toList()));
                    break;
            }
        });
    }
    // endregion

    // region Сектора
    private void UpdateSectors(TableEventArgs<SectorsModel> tableSectors) {

        Platform.runLater(() -> {

            listSectors = tableSectors.getTable();
            ucSectors.UpdateSectors(listSectors.stream()
                    .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                    .collect(Collectors.toList()));
        });
    }
    // endregion

    // region ЗЧ, ИЧ, ВЧ, ДРР, ДРП
    private void UpdateSpecFreqs(TableEventArgs<?> tableSpecFreqs) {

        Platform.runLater(() -> {

            switch (tableSpecFreqs.getType().getAnnotation(InfoTable.class).name()) {

                case TableFreqForbidden:

                    listFreqsForbidden = (List<SpecFreqsModel>) tableSpecFreqs.getTable();
                    ucFreqsForbidden.UpdateSpecFreqs(listFreqsForbidden.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));

                    ucReconFWS.setListFreqsForbidden(listFreqsForbidden);
                    if (directionFinderWnd != null) {

                        directionFinderWnd.ucReconFWS.setListFreqsForbidden(listFreqsForbidden);
                    }

                    break;

                case TableFreqImportant:

                    listFreqsImportant = (List<SpecFreqsModel>) tableSpecFreqs.getTable();
                    ucFreqsImportant.UpdateSpecFreqs(listFreqsImportant.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));

                    ucReconFWS.setListFreqsImportant(listFreqsImportant);
                    if (directionFinderWnd != null) {

                        directionFinderWnd.ucReconFWS.setListFreqsImportant(listFreqsImportant);
                    }
                    ucAnalogReconFWS.setListFreqsImportant(listFreqsImportant);
                    ucDigitalReconFWS.setListFreqsImportant(listFreqsImportant);

                    break;

                case TableFreqKnown:

                    listFreqsKnown = (List<SpecFreqsModel>) tableSpecFreqs.getTable();
                    ucFreqsKnown.UpdateSpecFreqs(listFreqsKnown.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));

                    ucReconFWS.setListFreqsKnown(listFreqsKnown);
                    if (directionFinderWnd != null) {

                        directionFinderWnd.ucReconFWS.setListFreqsKnown(listFreqsKnown);
                    }

                    break;

                case TableFreqRangesElint:

                    listRangesRI = (List<SpecFreqsModel>) tableSpecFreqs.getTable();
                    ucRangesRI.UpdateSpecFreqs(listRangesRI.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));

                    break;

                case TableFreqRangesJamming:

                    listRangesRS = (List<SpecFreqsModel>) tableSpecFreqs.getTable();
                    ucRangesRS.UpdateSpecFreqs(listRangesRS.stream()
                            .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                            .collect(Collectors.toList()));
                    break;
            }
        });
    }
    // endregion

    // region Комплексы
    private void UpdateStations(TableEventArgs<StationsModel> tableStations) {

        Platform.runLater(() -> {

            listStations = tableStations.getTable();
            ucStations.UpdateStations(listStations);

            AddStationsToMap();
            AddStationsToMapDF();
        });
    }

    /**
     * Обновить Комплексы на карте
     */
    private void AddStationsToMap() {

        Platform.runLater(() -> {

            try {
                MapOperations.setListStationsModel(listStations);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mapWnd != null) {

                if (mapWnd.chbComplex.isSelected()) {

                    //mapWnd.RemoveGraphics();

                    //MapOperations.UpdateListMapStations(mapWnd.sceneViewMap);


                    mapWnd.setListStationsModel(listStations);

                    if (mapWnd.chbZoneRS.isSelected()) {
                        MapOperations.UpdateZoneRS(mapWnd.sceneViewMap);
                        // mapWnd.sceneViewMap.getGraphicsOverlays().add(DrawBearing.DrawBearingToTarget(mapWnd.sceneViewMap,  1000));
                    }
                }
            }
        });
    }

    private void AddStationsToMapDF() {

        Platform.runLater(() -> {

            MapOperations.setListStationsDFModel(listStations);

            if (directionFinderWnd != null) {

                if (directionFinderWnd.chbComplex.isSelected()) {

                    MapOperations.AddStationsDF(directionFinderWnd.sceneViewMapDF);
                }
            }
        });
    }
    // endregion

    // region Маршруты
    private void UpdateRoutes(TableEventArgs<RouteModel> tableRoutes) {

        Platform.runLater(() -> {

            listRoutes = tableRoutes.getTable();
            if (mapWnd != null) {

                mapWnd.ucRoutes.UpdateRoutes(listRoutes);
                mapWnd.RemoveGraphicsFromDB(listRoutes);
            }
        });
    }
    // endregion

    // region Update tables EXAMPLE
    private void TablesUpdate(TableEventArgs<?> test) {
//        switch (test.getType().getAnnotation(InfoTable.class).name()) {

//            case TableFreqForbidden:
//                listFreqsForbidden = (List<SpecFreqsModel>) test.getTable();
//                ucFreqsForbidden.UpdateSpecFreqs(listFreqsForbidden);
//                break;
//

//            case TableResFHSSJam:
//                listSuppressFHSS = (List<SuppressFHSSModel>) test.getTable();
//                ucSuppressFHSS.UpdateSuppressFHSS(listSuppressFHSS);
//                break;

//        }

//        System.out.println("get update for " + test.getType().getAnnotation(InfoTable.class).name());
    }
    // endregion

    @Override
    public void onConnect(ConnectionEventArgs connectionEventArgs) {

        ucServerDBConnection.ShowConnect();
        LoadTables();
    }

    @Override
    public void onDisconnect(ConnectionEventArgs connectionEventArgs) {

        ucServerDBConnection.ShowDisconnect();
        clientDB = null;
    }
    // endregion

    /**
     * Загрузить таблицы из базы данных, принадлежащие выбранной станции
     *
     * @param IdStation
     */
    private void LoadTablesByFilter(int IdStation) {

        listFreqsForbidden = ((IDependentAsp) clientDB.tables.get(NameTable.TableFreqForbidden)).loadByFilter(IdStation);
        ucFreqsForbidden.UpdateSpecFreqs(listFreqsForbidden);

        listFreqsImportant = ((IDependentAsp) clientDB.tables.get(NameTable.TableFreqImportant)).loadByFilter(IdStation);
        ucFreqsImportant.UpdateSpecFreqs(listFreqsImportant);

        listFreqsKnown = ((IDependentAsp) clientDB.tables.get(NameTable.TableFreqKnown)).loadByFilter(IdStation);
        ucFreqsKnown.UpdateSpecFreqs(listFreqsKnown);

        listRangesRI = ((IDependentAsp) clientDB.tables.get(NameTable.TableFreqRangesElint)).loadByFilter(IdStation);
        ucRangesRI.UpdateSpecFreqs(listRangesRI);

        listRangesRS = ((IDependentAsp) clientDB.tables.get(NameTable.TableFreqRangesJamming)).loadByFilter(IdStation);
        ucRangesRS.UpdateSpecFreqs(listRangesRS);

        listSectors = ((IDependentAsp) clientDB.tables.get(NameTable.TableSectorsElint)).loadByFilter(IdStation);
        ucSectors.UpdateSectors(listSectors);

        listSuppressFWS = ((IDependentAsp) clientDB.tables.get(NameTable.TableResFFJam)).loadByFilter(IdStation);
        ucSuppressFWS.UpdateSuppressFWS(listSuppressFWS);

        listSuppressFHSS = ((IDependentAsp) clientDB.tables.get(NameTable.TableResFHSSJam)).loadByFilter(IdStation);
        ucSuppressFHSS.setListSuppressFHSS(listSuppressFHSS);

    }

    /**
     * Загрузить таблицы из базы данных
     */
    private void LoadTables() {

        listGlobalProperties = clientDB.tables.get(NameTable.TableGlobalProperties).load();
        ucProperties.setGlobalSettings(listGlobalProperties.get(0));

        ObservableList<String> ls = FXCollections.observableArrayList(new String[]{});
        ucAnalogReconFWS.setModulationList(ls);
        ObservableList<Integer> li = FXCollections.observableArrayList(new Integer[]{});
        ucProperties.UpdateComboBox(li);
        ObservableList<Double> ld = FXCollections.observableArrayList(new Double[]{});
        ucAnalogReconFWS.setBandAudioList(ld);
        listAnalogReconFWS = clientDB.tables.get(NameTable.TableAnalogResFF).load();
        ucAnalogReconFWS.setListAnalogReconFWS(listAnalogReconFWS);

        listStations = clientDB.tables.get(NameTable.TableJammer).load();
        ucStations.UpdateStations(listStations);
        AddStationsToMap();
        AddStationsToMapDF();

        listFreqsForbidden = clientDB.tables.get(NameTable.TableFreqForbidden).load();
        ucFreqsForbidden.UpdateSpecFreqs(listFreqsForbidden.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));
        ucReconFWS.setListFreqsForbidden(listFreqsForbidden);

        listFreqsImportant = clientDB.tables.get(NameTable.TableFreqImportant).load();
        ucFreqsImportant.UpdateSpecFreqs(listFreqsImportant.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));
        ucReconFWS.setListFreqsImportant(listFreqsImportant);
        ucAnalogReconFWS.setListFreqsImportant(listFreqsImportant);
        ucDigitalReconFWS.setListFreqsImportant(listFreqsImportant);

        listFreqsKnown = clientDB.tables.get(NameTable.TableFreqKnown).load();
        ucFreqsKnown.UpdateSpecFreqs(listFreqsKnown.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));
        ucReconFWS.setListFreqsKnown(listFreqsKnown);

        listRangesRI = clientDB.tables.get(NameTable.TableFreqRangesElint).load();
        ucRangesRI.UpdateSpecFreqs(listRangesRI.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));

        listRangesRS = clientDB.tables.get(NameTable.TableFreqRangesJamming).load();
        ucRangesRS.UpdateSpecFreqs(listRangesRS.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));

        listSectors = clientDB.tables.get(NameTable.TableSectorsElint).load();
        ucSectors.UpdateSectors(listSectors.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));

        listReconFHSS = clientDB.tables.get(NameTable.TableResFHSS).load();
        ucReconFHSS.UpdateReconFHSS(listReconFHSS);

        listSuppressFWS = clientDB.tables.get(NameTable.TableResFFJam).load();
        ucSuppressFWS.UpdateSuppressFWS(listSuppressFWS.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));

        listSuppressFHSS = clientDB.tables.get(NameTable.TableResFHSSJam).load();
        ucSuppressFHSS.setListSuppressFHSS(listSuppressFHSS.stream()
                .filter(x -> x.getStationId() == PropertiesStations.getSelectedNumASP())
                .collect(Collectors.toList()));

        listFHSSExcluded = clientDB.tables.get(NameTable.TableFHSSExcludedJam).load();
        ucSuppressFHSS.UpdateFHSSExcluded(listFHSSExcluded);

        listReconFWSDistrib = clientDB.tables.get(NameTable.TableResFFDistribution).load();
        ucReconFWSDistrib.setListReconFWSDistrib(listReconFWSDistrib);

        listRoutes = clientDB.tables.get(NameTable.TableRoute).load();

        listDigitalReconFWS = ReconOperations.SelectedDigitalReconFWS(listSectors, clientDB.tables.get(NameTable.TableDigitalResFF).load());
        ucDigitalReconFWS.UpdateDigitalReconFWS(listDigitalReconFWS);

        listReconFWS = clientDB.tables.get(NameTable.TableResFF).load();
        ucReconFWS.UpdateReconFWS(listReconFWS);
    }

    /**
     * Connections
     *
     * @param nameServer
     */
    @Override
    public void OnButtonClick(String nameServer) {

        try {

            switch (nameServer) {

                case "ServerDAP":

                    if (clientKvetka != null) {

                        clientKvetka.disconnect();
                    } else {

                        setUserClientKvetka("АРМ " + String.valueOf(PropertiesOperations.getLocalProperties().getGeneral().getAWS()));
                        clientKvetka = new ClientKvetka(PropertiesOperations.getLocalProperties().getDf().getIPAddress(), PropertiesOperations.getLocalProperties().getDf().getPort(), getUserClientKvetka(),
                                this, this, this);
//                    clientKvetka = new ClientKvetka(PropertiesOperations.getLocalProperties().getDf().getIPAddress(), PropertiesOperations.getLocalProperties().getDf().getPort(), user, this);
                        clientKvetka.connect();
                    }
                    break;

                case "ServerDB":

                    if (clientDB != null) {

                        try {
                            clientDB.disconnect();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        clientDB = new ClientDB(user, PropertiesOperations.getLocalProperties().getDb().getIPAddress(), PropertiesOperations.getLocalProperties().getDb().getPort());
                        InitClientDB();
                        clientDB.connect();
                    }
                    break;
            }
        } catch (Exception ex) {

        }


    }

    // region Interfaces IWndMap
    @Override
    public void OnCloseMap() {

        tbShowMap.setSelected(false);
    }

    @Override
    public void OnCloseDirectionFinder() {

        tbShowDirectionFinder.setSelected(false);
    }

    @Override
    public void OnCloseBPSS() {

        tbShowBPSS.setSelected(false);
    }

    @Override
    public void OnCloseDFAccuracy() {

        tbDFAccuracy.setSelected(false);
    }

    @Override
    public void OnClosePlayer() {

        tbShowPlayer.setSelected(false);
    }

    @Override
    public void OnChangeLocalMapPath() {

        ucProperties.setLocalSettings(PropertiesOperations.getLocalProperties());
    }
    // endregion

    // region clientKvetka
    @Override
    public void connect() {

        ucServerDAPConnection.ShowConnect();
        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                .setMode(Kvetka.DspServerMode.Stop).build());

        clientKvetka.getBoraInfoAsync();
    }

    @Override
    public void disconnect() {

        ucServerDAPConnection.ShowDisconnect();
        clientKvetka = null;
        ucServerDAPConnection.ShowRead(LedStates.StopRead);
        ucServerDAPConnection.ShowWrite(LedStates.StopWrite);
    }

    @Override
    public void changeMode(Kvetka.DefaultResponse defaultResponse) {

        // Для использования в асинхронном режиме
        defaultResponse.getIsSucceeded();
    }

    @Override
    public void getMode(Kvetka.GetModeResponse getModeResponse) {


    }

    @Override
    public void getSpectrum(Kvetka.SpectrumResponse spectrumResponse) {

        Platform.runLater(() -> {

            var byteSpectrum = spectrumResponse.getAmplitude().toByteArray();
            ucPaneSpectr.UpdateSpectr(byteSpectrum, 1500, 30000);

//            if (tbShowDirectionFinder.isSelected()) {
//
//                double[] arrayDouble = new double[byteSpectrum.length];
//                for (int i = 0; i < byteSpectrum.length; i++) {
//                    arrayDouble[i] = ((double) byteSpectrum[i]);
//                }
//                directionFinderWnd.ucPaneSpectrPelengs.UpdateSpectr(arrayDouble, 1500, 30000);// ЧАП
//            }
        });
    }

    @Override
    public void getNarrowSpectrum(Kvetka.SpectrumResponse spectrumResponse) {

        Platform.runLater(() -> {

            ucPaneSpectr.UpdateSubSpectrum(spectrumResponse.getAmplitude().toByteArray(), narrowStart, narrowEnd);
        });
    }

    @Override
    public void getAmplitudeTimeDiagram(Kvetka.AmplitudeTimeDiagramResponse amplitudeTimeDiagramResponse) {

        if (amplitudeTimeDiagramResponse.getArrayOfAazimuthsList() != null) {

            double[] mas = new double[amplitudeTimeDiagramResponse.getArrayOfAazimuthsList().size()];
            for (int i = 0; i < mas.length; i++) {

                mas[i] = amplitudeTimeDiagramResponse.getArrayOfAazimuthsList().get(i);
            }

            if (directionFinderWnd != null) {

                Platform.runLater(() -> {

                    double[] arrayDouble = new double[amplitudeTimeDiagramResponse.getLevelsArrayList().size()];
                    for (int i = 0; i < amplitudeTimeDiagramResponse.getLevelsArrayList().size(); i++) {

                        arrayDouble[i] = ((double) amplitudeTimeDiagramResponse.getLevelsArrayList().get(i));
                    }

                    directionFinderWnd.ucPaneSpectrPelengs.UpdateSpectr(arrayDouble,
                            amplitudeTimeDiagramResponse.getStartFrequencyKhz(),
                            amplitudeTimeDiagramResponse.getEndFrequencyKhz());

                    directionFinderWnd.ucPaneSpectrPelengs.UpdatePelengs(mas,
                            amplitudeTimeDiagramResponse.getStepKHz(),
                            amplitudeTimeDiagramResponse.getStartFrequencyKhz());

                });
            }
        }
    }

    @Override
    public void setThresholdRI(Kvetka.DefaultResponse defaultResponse) {

    }

    @Override
    public void updateServerState(Kvetka.ServerState serverState) {

        // region Veresk
        switch (serverState.getVereskStatus()) {

            case NotConnected, DeviceError -> {
                ucVereskState.ShowLedRed();
                break;
            }
            case ConnectedRegister -> {
                ucVereskState.ShowLedGreen();
                break;
            }
        }
        // endregion

        // region Bora
        switch (serverState.getBoraStatus()) {

            case NotConnected, DeviceError -> {
                ucBoraState.ShowLedRed();
                break;
            }
            case ConnectedRegister -> {
                ucBoraState.ShowLedGreen();
                break;
            }
        }
        // endregion

        // region Пеленгатор УП
        switch (serverState.getPelengatorNarrowStatus()) {

            case NotConnected, DeviceError -> {
                ucPelNarrowBandState.ShowLedRed();
                break;
            }
            case ConnectedRegister -> {
                ucPelNarrowBandState.ShowLedGreen();
                break;
            }
        }
        // endregion

        // region Пеленгатор ШП
        switch (serverState.getPelengatorWideStatus()) {

            case NotConnected, DeviceError -> {
                ucPelWideBandState.ShowLedRed();
                break;
            }
            case ConnectedRegister -> {
                ucPelWideBandState.ShowLedGreen();
                break;
            }
        }
        // endregion

        // todo: здесь только загорать кнопки на боковой панели, только в Подготовке
        if (serverState.hasPlannedMode()) {

            switch (serverState.getPlannedMode()) {

                case Stop -> {

                    tbRIwithBearing.setSelected(false);
                    tbSearchFHSS.setSelected(false);
                    tbSuppressFWS.setSelected(false);
                    tbSuppressFHSS.setSelected(false);
                }

                case RadioIntelligence -> {

                    tbRIwithBearing.setSelected(false);
                    tbSearchFHSS.setSelected(false);
                }

                case RadioIntelligenceWithBearing -> {

                    tbRIwithBearing.setSelected(true);
                    tbSearchFHSS.setSelected(false);
                }

                case RadioIntelligenceFFHS -> {

                    tbRIwithBearing.setSelected(true);
                    tbSearchFHSS.setSelected(true);
                }

                case RadioJammingFrs -> {

                    tbSuppressFWS.setSelected(true);
                    tbSuppressFHSS.setSelected(false);
                }

                case RadioJammingFhss -> {

                    tbSuppressFWS.setSelected(false);
                    tbSuppressFHSS.setSelected(true);
                }

                case UNRECOGNIZED -> {

                    break;
                }
            }
            return;
        }

        DeleteIndicatorsSuppress();
        switch (serverState.getCurrentMode()) {

            case Stop -> {

                ucPanelBModes.CheckBMode(TButtonMode.Preparation);
                ucServerDAPConnection.ShowRead(LedStates.StopRead);
                ucServerDAPConnection.ShowWrite(LedStates.StopWrite);

                try {
                    spectrumThread.interrupt();

                }catch (Exception ex){

                }

                try{
                    narrowSpectrumTask.interrupt();
                }catch (Exception ex){

                }
                narrowSpectrumTask = null;
                StopTimeWorking();

                break;
            }

            case RadioIntelligence -> {

                ucPanelBModes.CheckBMode(TButtonMode.Recon);
                ucServerDAPConnection.ShowRead(LedStates.StartRead);
                ucServerDAPConnection.ShowWrite(LedStates.StartWrite);
                tbRIwithBearing.setSelected(false);
                tbSearchFHSS.setSelected(false);

                clientKvetka.SpectrumRequestInStream(Kvetka.SpectrumRequest.newBuilder()
                        .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(1500).setEndFrequencyKhz(30000)
                        .setStepHz(100).build());

                StopTimeWorking();

                break;
            }

            case RadioIntelligenceWithBearing -> {

                ucPanelBModes.CheckBMode(TButtonMode.Recon);
                ucServerDAPConnection.ShowRead(LedStates.StartRead);
                ucServerDAPConnection.ShowWrite(LedStates.StartWrite);
                tbRIwithBearing.setSelected(true);
                tbSearchFHSS.setSelected(false);

                clientKvetka.SpectrumRequestInStream(Kvetka.SpectrumRequest.newBuilder()
                        .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(1500).setEndFrequencyKhz(30000)
                        .setStepHz(100).build());

                clientKvetka.amplitudeTimeDiagramInStream(Kvetka.AmplitudeTimeDiagramRequest.newBuilder()
                        .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(1500).setEndFrequencyKhz(30000)
                        .setStepKHz(100).build());

                StopTimeWorking();

                break;
            }

            case RadioIntelligenceFFHS -> {

                ucPanelBModes.CheckBMode(TButtonMode.Recon);
                ucServerDAPConnection.ShowRead(LedStates.StartRead);
                ucServerDAPConnection.ShowWrite(LedStates.StartWrite);
                tbRIwithBearing.setSelected(true);
                tbSearchFHSS.setSelected(true);

                clientKvetka.SpectrumRequestInStream(Kvetka.SpectrumRequest.newBuilder()
                        .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(1500).setEndFrequencyKhz(30000)
                        .setStepHz(100).build());

                StopTimeWorking();

                break;
            }

            case RadioJammingFrs -> {

                ucPanelBModes.CheckBMode(TButtonMode.Suppress);
                ucServerDAPConnection.ShowRead(LedStates.StartRead);
                ucServerDAPConnection.ShowWrite(LedStates.StartWrite);
                tbSuppressFWS.setSelected(true);
                tbSuppressFHSS.setSelected(false);

                Runnable task = new Runnable() {

                    @Override
                    public void run() {

                        while (true) {

                            clientKvetka.getSpectrumAsync(Kvetka.SpectrumRequest.newBuilder()
                                    .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(1500).setEndFrequencyKhz(30000)
                                    .setStepHz(100).build());
                            try {
                                Thread.sleep(192);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };

                spectrumThread = new Thread(task);
                spectrumThread.start();

                StartTimeWorking();

                break;
            }

            case RadioJammingFhss -> {

                ucPanelBModes.CheckBMode(TButtonMode.Suppress);
                ucServerDAPConnection.ShowRead(LedStates.StartRead);
                ucServerDAPConnection.ShowWrite(LedStates.StartWrite);
                tbSuppressFWS.setSelected(false);
                tbSuppressFHSS.setSelected(true);

                StartTimeWorking();

                break;
            }
        }
    }

    /**
     * Удалить индикацию из таблиц, стрелки со спектра
     */
    private void DeleteIndicatorsSuppress() {

        Platform.runLater(() -> {

            try {
                Thread.sleep(10);
            } catch (Exception e) {

            }

            ucSuppressFWS.UpdateTempSuppressFWS(FXCollections.observableArrayList(), listSuppressFWS);
            ucSuppressFHSS.UpdateTempSuppressFHSS(FXCollections.observableArrayList(), listSuppressFHSS);

            ucPaneSpectr.getDrawingArea().clearArrows();
            ucPaneSpectr.getDrawingArea().removeRangeHead(0);

            if (directionFinderWnd != null) {

                directionFinderWnd.ucPaneSpectrPelengs.getDrawingArea().removeRangeHead(0);
                directionFinderWnd.ucPaneSpectrPelengs.getDrawingArea().clearArrows();
            }
        });
    }

    @Override
    public void updateStateFF(Kvetka.UpdateStateFFStream updateStateFFStream) {

        List<TempSuppressFWSModel> listTempSuppressFWS = FXCollections.observableArrayList();

        Platform.runLater(() -> {

            for(int i = 0; i < updateStateFFStream.getFFStatesList().size(); i++){

                TempSuppressFWSModel model = new TempSuppressFWSModel();
                model.setId(updateStateFFStream.getFFStatesList().get(i).getID());

                switch (updateStateFFStream.getFFStatesList().get(i).getControlStatus()){

                    case -1:
                        model.setControl(Led.Empty);
                        break;

                    case 0:
                        model.setControl(Led.Red);
                        break;

                    case 1:
                        model.setControl(Led.Green);
                        break;
                }

                switch (updateStateFFStream.getFFStatesList().get(i).getJammingStatus()){

                    case -1:
                        model.setSuppress(Led.Empty);
                        break;

                    case 0:
                        model.setSuppress(Led.Red);
                        break;

                    case 1:
                        model.setSuppress(Led.Green);
                        break;
                }

                switch (updateStateFFStream.getFFStatesList().get(i).getRadiationStatus()){

                    case -1:
                        model.setRadiation(Led.Empty);
                        break;

                    case 0:
                        model.setRadiation(Led.Red);
                        break;

                    case 1:
                        model.setRadiation(Led.Blue);
                        break;
                }
                listTempSuppressFWS.add(model);
            }
            ucSuppressFWS.UpdateTempSuppressFWS(listTempSuppressFWS, listSuppressFWS);

            double[] mas = new double[updateStateFFStream.getFFStatesList().size()];
            for (int i = 0; i < updateStateFFStream.getFFStatesList().size(); i++) {

                if(updateStateFFStream.getFFStatesList().get(i).getJammingStatus() == 1) {

                    mas[i] = updateStateFFStream.getFFStatesList().get(i).getFrequency();
                }
            }
            ucPaneSpectr.getDrawingArea().setArrows(mas);

            if (directionFinderWnd != null) {

                directionFinderWnd.ucPaneSpectrPelengs.getDrawingArea().setArrows(mas);
            }
        });
    }

    @Override
    public void updateStateFHSS(Kvetka.UpdateStateFHSSStream updateStateFHSSStream) {

        Platform.runLater(() -> {

            List<TempSuppressFHSSModel> listTempSuppressFHSS = FXCollections.observableArrayList(

                    new TempSuppressFHSSModel(updateStateFHSSStream.getFHSSStates(0).getID(), Led.Empty, Led.Green, Led.Empty, PropertiesStations.getSelectedNumASP())
            );
            ucSuppressFHSS.UpdateTempSuppressFHSS(listTempSuppressFHSS, listSuppressFHSS);

            ucPaneSpectr.getDrawingArea().addRangeHead(listSuppressFHSS.get(0).getFreqMin(), listSuppressFHSS.get(0).getFreqMax());
            double[] mas = new double[updateStateFHSSStream.getFHSSStates(0).getFrequenciesList().size()];
            for (int i = 0; i < updateStateFHSSStream.getFHSSStates(0).getFrequenciesList().size(); i++) {

                mas[i] = updateStateFHSSStream.getFHSSStates(0).getFrequenciesList().get(i);
            }

            try {
                ucPaneSpectr.getDrawingArea().getRangeHead(listSuppressFHSS.get(0).getFreqMin(), listSuppressFHSS.get(0).getFreqMax()).setSticks(mas);
            } catch (NotFoundHeadException e) {
                throw new RuntimeException(e);
            }

            if (directionFinderWnd != null) {

                try {
                    directionFinderWnd.ucPaneSpectrPelengs.getDrawingArea().getRangeHead(listSuppressFHSS.get(0).getFreqMin(), listSuppressFHSS.get(0).getFreqMax()).setSticks(mas);
                } catch (KvetkaSpectrWithPelengsLib.Exceptions.NotFoundHeadException e) {
                    throw new RuntimeException(e);
                }
//                directionFinderWnd.ucPaneSpectrPelengs.getDrawingArea().setSticks(mas, 0);
            }
        });
    }

    @Override
    public void getBearing(Kvetka.BearingResponse bearingResponse) {

        Platform.runLater(() -> {

            if (DFAccuracyWnd != null) {

                DFTableModel model = new DFTableModel();
                model.setIsCheck(true);
                model.setBearing((float) bearingResponse.getHDir());
                model.setQuality((int) bearingResponse.getQuality());
                model.setLevel((int) bearingResponse.getAmplitude());

                DFAccuracyWnd.ucDFAccuracy.setModelDFTable(model);
            }
        });
    }

    @Override
    public void getBearings(Kvetka.MultibleBearingResponse multibleBearingResponse) {

        Platform.runLater(() -> {

            if (DFAccuracyWnd != null) {

                for (int i = 0; i < multibleBearingResponse.getBearingResponsesCount(); i++)
                {
                    DFTableModel model = new DFTableModel();
                    model.setIsCheck(true);
                    model.setBearing((float) multibleBearingResponse.getBearingResponses(i).getHDir());
                    model.setQuality((int) multibleBearingResponse.getBearingResponses(i).getQuality());
                    model.setLevel((int) multibleBearingResponse.getBearingResponses(i).getAmplitude());

                    DFAccuracyWnd.ucDFAccuracy.setModelDFTable(model);
                }
            }
        });
    }

    @Override
    public void statusRequest(JSG.StateJsgResponseMessage stateJsgResponseMessage) {

    }

    @Override
    public void telesignalizationRequest(JSG.TelesignalResponseMessage telesignalResponseMessage) {

    }

    @Override
    public void setParamFFJam(Kvetka.DefaultResponse defaultResponse) {

    }

    @Override
    public void statusRequestAmp(JSG.StateAmResponseMessage stateAmResponseMessage) {

    }

    @Override
    public void setRadiation(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

    }

    @Override
    public void setPowerLevel(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

    }

    @Override
    public void errorsRequestAmp(JSG.FailureAmResponseMessage failureAmResponseMessage) {

    }

    @Override
    public void setParamFHSSAm(Kvetka.DefaultResponse defaultResponse) {

    }

    @Override
    public void setFilterAm(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

    }

    @Override
    public void setPowerSupply(JSG.DefaulAmResponseMessage defaulAmResponseMessage) {

        //   ucAmplifiers.UpdateStateButton(defaulAmResponseMessage.);
    }

    @Override
    public void connectToJSG(Kvetka.DefaultResponse defaultResponse) {

    }

    @Override
    public void setFHSSOff(Kvetka.DefaultResponse defaultResponse) {

    }

    @Override
    public void radiationOff(Kvetka.DefaultResponse defaultResponse) {

    }

    @Override
    public void setTimeDelayForBoraData(Kvetka.TimeDelayMessage timeDelayMessage) {

        // ответ установленного значения
    }

    @Override
    public void getTimeDelayForBoraData(Kvetka.TimeDelayMessage timeDelayMessage) {

    }
    // endregion

    // region Panel Modes
    @Override
    public void OnClickTBMode(TButtonModeEvent tButtonModeEvent) {

        if (clientKvetka != null) {

            switch (tButtonModeEvent.TButtonMode) {

                case Preparation:

                    clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                            .setMode(Kvetka.DspServerMode.Stop).build());
                    break;

                case Recon:

                    if (tbRIwithBearing.isSelected() && tbSearchFHSS.isSelected()) {

                        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                                .setMode(Kvetka.DspServerMode.RadioIntelligenceFFHS).build());
                    } else if (tbRIwithBearing.isSelected() && !tbSearchFHSS.isSelected()) {

                        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                                .setMode(Kvetka.DspServerMode.RadioIntelligenceWithBearing).build());
                    } else {

                        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                                .setMode(Kvetka.DspServerMode.RadioIntelligence).build());
                    }
                    break;

                case Suppress:

                    if (tbSuppressFWS.isSelected()) {

                        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                                .setMode(Kvetka.DspServerMode.RadioJammingFrs).build());
                    }

                    if (tbSuppressFHSS.isSelected()) {

                        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                                .setMode(Kvetka.DspServerMode.RadioJammingFhss).build());
                    }

                    if (!tbSuppressFWS.isSelected() && !tbSuppressFHSS.isSelected()) {

                        tbSuppressFWS.setSelected(true);
                        clientKvetka.setModeAsync(Kvetka.SetModeRequest.newBuilder()
                                .setMode(Kvetka.DspServerMode.RadioJammingFrs).build());
                    }
                    break;

                default:
                    break;
            }
        }
    }
    // endregion

    // region Spectr
    @Override
    public void signalAdded(double v, AnalogReconFWSModel analogReconFWSModel) {

    }

    @Override
    public void signalDeleted(double v, AnalogReconFWSModel analogReconFWSModel) {

    }

    @Override
    public void signalBecameUnavailable(double v, AnalogReconFWSModel analogReconFWSModel) {

    }

    @Override
    public void signalBecameAvailableAgain(double v, AnalogReconFWSModel analogReconFWSModel) {

    }

    private int narrowStart = 1500;
    private int narrowEnd = 1600;
    @Override
    public void recuestSubSpectrum(double v, double v1) {

        narrowStart = (int) Math.floor(v);
        narrowEnd = (int) Math.ceil(v1);

        if (narrowSpectrumTask == null) {

            Platform.runLater(() -> {

                if (narrowSpectrumTask == null) {

                    narrowSpectrumTask = new Thread() {

                        @Override
                        public void run() {
                            while (true) {

                                if (clientKvetka != null) {

                                    clientKvetka.getNarrowSpectrum(Kvetka.SpectrumRequest.newBuilder()
                                            .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(narrowStart).setEndFrequencyKhz(narrowEnd)
                                            .setStepHz(25).build());
                                }
                                try {
                                    Thread.sleep(128);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    };
                    narrowSpectrumTask.start();
                }
           });
        }
    }


    @Override
    public void thresholdChanged(double threshold) {

        int finalThreshold = (int) (threshold < 0 ? threshold * -1 : threshold);

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.setRIThresholdAsync(Kvetka.SetThresholdRequest.newBuilder()
                        .setThreshold(finalThreshold).build());
            }
        });
    }

    @Override
    public void onSubmittedRoute(RouteModel routeModel) {

        try {

            clientDB.tables.get(NameTable.TableRoute).add(new TableEvents(routeModel).getRecord());
        } catch (ExceptionClient e) {
            e.printStackTrace();
        }
    }
    // endregion

    // region Аттенюаторы
    @Override
    public void buttonAttenuatorSelected(int i) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.setAttAndAmpAsync(Kvetka.SetAttAndAmpRequest.newBuilder()
                        .setAttenuator(i).setAmplifier(true).build());
            }
        });
    }

    /**
     * Установить аттенюатор в ucPaneSpectr
     *
     * @param setAttAndAmpResponse
     */
    @Override
    public void setAttAndAmpl(Kvetka.SetAttAndAmpResponse setAttAndAmpResponse) {

        try {

            Platform.runLater(() -> {

                if (setAttAndAmpResponse.getAttenuator() >= 0) {

                    ucPaneSpectr.getAttenuatorsControl().setItemInSignalState(true);
                }

                // todo: при подключении к серверу обновить комбоБокс
                // ucPaneSpectr.getAttenuatorsControl().AddAttenuatorsList(new ArrayList<>());

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // endregion

    // region Усилители мощности
    @Override
    public void OnClickAmplifiers(StateButton stateButton) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.SetPowerSupplyAsync(JSG.PowerSupplyMessage.newBuilder()
                        .setPowerSupply(stateButton.getState()).build());
            }
        });
    }

    /**
     * Обновить состояние панели УМ в ucAmplifiers
     *
     * @param updateStateJSGStream
     */
    @Override
    public void updateStateJSG(Kvetka.UpdateStateJSGStream updateStateJSGStream) {

        Platform.runLater(() -> {

            try {
                if(updateStateJSGStream.hasJSGState()){

                }
                if(updateStateJSGStream.hasAmplifierInfo()){

                    amplifiersModel = new AmplifiersModel();
                    amplifiersModel.setCoeff(updateStateJSGStream.getAmplifierInfo().getVSWR());
                    // amplifiersModel.setCoeff(1.3);

                    amplifiersModel.setOutputRegulator((int) (((double)updateStateJSGStream.getAmplifierInfo().getPower() / 5000.0) * 100.0));
                    amplifiersModel.setTurnOn(updateStateJSGStream.getAmplifierInfo().getHasPower());
                    ucAmplifiers.UpdateStateButton(updateStateJSGStream.getAmplifierInfo().getHasPower());

                    amplifiersModel.setCrash(updateStateJSGStream.getAmplifierInfo().getHasFailure());
                    amplifiersModel.setReady(updateStateJSGStream.getAmplifierInfo().getIsReady());
                    amplifiersModel.setTemperature(updateStateJSGStream.getAmplifierInfo().getTemperature());
                    amplifiersModel.setPower(updateStateJSGStream.getAmplifierInfo().getPower() / 1000.0);
                    ucAmplifiers.UpdateStateAmplifiers(amplifiersModel);

                }
            } catch (Exception ex) {

            }
        });
    }

    private Thread TimeWorkingThread;
    /**
     * Обновить время работы УМ
     */
    private void StartTimeWorking() {

        StopTimeWorking();

        TimeWorkingThread = new Thread(() -> {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            long tStart = new Date().getTime();
            long tEnd = 0;

            while (true) {
                try {

                    Thread.sleep(1000); // 1 second
                    tEnd = new Date().getTime();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long milliseconds = tEnd - tStart;

                String[] sDateTime = new Date(milliseconds).toInstant().toString().split("T");
                String[] sTime = sDateTime[1].split(":");
                String hours = sTime[0];
                String minutes = sTime[1];
                String seconds = sTime[2].substring(0, 2);
                final String time = hours + ":" + minutes + ":" + seconds;

                Platform.runLater(() -> {
                    lTimeWorking.setText(time);
                });
            }
        });   TimeWorkingThread.start();
    }

    private void StopTimeWorking() {

        if (TimeWorkingThread != null) {

            TimeWorkingThread.stop();
            TimeWorkingThread = null;
            lTimeWorking.setText("00:00:00");
        }
    }
    // endregion

    // region БПСС

    /**
     * BPSSControl Events
     *
     * @param buttonsModel
     */
    @Override
    public void OnButtonClickBPSS(ButtonsModel buttonsModel) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.setBpssJammingAsync(Bpss.SetJammingRequest.newBuilder()
                        .setGnssL1(buttonsModel.isGNSSL1())
                        .setGnssL2(buttonsModel.isGNSSL2())
                        .setGpsL1(buttonsModel.isGPSL1())
                        .setGpsL2(buttonsModel.isGPSL2())
                        .setGlonassL1(buttonsModel.isGLONASSL1())
                        .setGlonassL2(buttonsModel.isGLONASSL2())
                        .setPower(0).build());
                if((!buttonsModel.isGPSL1() && !buttonsModel.isGLONASSL1() && !buttonsModel.isGNSSL1())) {
                    clientKvetka.setBpssRadiationAsync(Bpss.SetRadiationRequest.newBuilder().setState(true).build());
                }
            }
        });
    }

    @Override
    public void OnOffRadiationBPSS(ButtonsModel buttonsModel) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.setBpssRadiationAsync(Bpss.SetRadiationRequest.newBuilder()
                        .setState(false).build());
                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {}
                clientKvetka.getBpssLitterStateAsync();
            }
        });
    }

    @Override
    public void OnButtonClickAnt(AntModel antModel) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                clientKvetka.setBpssAntennaAsync(Bpss.AntennaStateMessage.newBuilder()
                        .setAntennaState(antModel.getAnt() == 1 ? Bpss.AntennaState.OMNI : Bpss.AntennaState.LOG).build()

                );
            }
        });
    }

    /**
     * client Events
     *
     * @param
     */
    @Override
    public void getBpssAntennaState(Bpss.AntennaStateMessage antennaStateMessage) {

        try {

            BPSSWnd.ucBPSS.UpdateStatusAntennas(antennaStateMessage
                    .getAntennaState() == Bpss.AntennaState.OMNI
                    ? new AntModel((byte) 1)
                    : new AntModel((byte) 0));

            Platform.runLater(() -> {

                if (clientKvetka != null) {

                    clientKvetka.getBpssLitterStateAsync();
                }
            });

        } catch (Exception ex) {

        }
    }

    @Override
    public void setBpssAntenna(Bpss.AntennaStateMessage antennaStateMessage) {

        try {

            BPSSWnd.ucBPSS.UpdateStatusAntennas(antennaStateMessage
                    .getAntennaState() == Bpss.AntennaState.OMNI
                    ? new AntModel((byte) 1)
                    : new AntModel((byte) 0));

        } catch (Exception ex) {

        }
    }

    @Override
    public void setBpssRadiation(Kvetka.DefaultResponse defaultResponse) {

    }


    @Override
    public void getBpssLiterState(Bpss.StatusResponse statusResponse) {
        try {
            bpssModelL1.setPower((byte) statusResponse.getLitterStatusList().get(0).getAmplifierState());
            bpssModelL1.setTemperature((short) statusResponse.getLitterStatusList().get(0).getTemperature());
            bpssModelL1.setRadiation((byte) statusResponse.getLitterStatusList().get(0).getRadiationStatus());
            bpssModelL1.setCurrent((byte) ( statusResponse.getLitterStatusList().get(0).getCurrent()/10));
            bpssModelL1.setError((byte) statusResponse.getLitterStatusList().get(0).getErrorCode());
            bpssModelL1.setSnt((byte) statusResponse.getLitterStatusList().get(0).getSynthesizer());
            Platform.runLater(() -> {
                        //cause of greatest firmware of bpss don't ask me.
                        BPSSWnd.ucBPSS.UpdateParamsL2(bpssModelL1);
                });

            bpssModelL2.setPower((byte) statusResponse.getLitterStatusList().get(1).getAmplifierState());
            bpssModelL2.setTemperature((short) statusResponse.getLitterStatusList().get(1).getTemperature());
            bpssModelL2.setRadiation((byte) statusResponse.getLitterStatusList().get(1).getRadiationStatus());
            bpssModelL2.setCurrent((byte) (statusResponse.getLitterStatusList().get(1).getCurrent()/10));
            bpssModelL2.setError((byte) statusResponse.getLitterStatusList().get(1).getErrorCode());
            bpssModelL2.setSnt((byte) statusResponse.getLitterStatusList().get(1).getSynthesizer());
            Platform.runLater(() -> {
                BPSSWnd.ucBPSS.UpdateParamsL1(bpssModelL2);
            });
        } catch (Exception ex) {

        }
    }

    @Override
    public void setBpssGnss(Bpss.SetJammingResponse setJammingResponse) {


    }

    @Override
    public void setBpssJamming(Bpss.SetJammingResponse setJammingResponse) {

        try {
            if (!setJammingResponse.getIsSucceeded()) {return;}
            buttonModel.setGPSL1(setJammingResponse.getGpsL1());
            buttonModel.setGPSL2(setJammingResponse.getGpsL2());
            buttonModel.setGLONASSL1(setJammingResponse.getGlonassL1());
            buttonModel.setGLONASSL2(setJammingResponse.getGlonassL2());
            buttonModel.setGNSSL1(setJammingResponse.getGnssL1());
            buttonModel.setGNSSL2(setJammingResponse.getGnssL2());

            BPSSWnd.ucBPSS.UpdateButtonsBPSS(buttonModel);

            Platform.runLater(() -> {
                if(clientKvetka != null) {
                    clientKvetka.getBpssLitterStateAsync();
                }
            });

        } catch (Exception ex) {

        }
    }


    @Override
    public void getBoraInfo(Kvetka.BoraInfoResponse boraInfoResponse) {

        // Установить в контролах comboBox----------------
        // attenuators
        try {
            ucPaneSpectr.getAttenuatorsControl().AddItemsList(boraInfoResponse.getControlResponceInfo().getAttenuatorValuesListList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Фильтр, кГц
        try {
            ucPaneSpectr.getFiltersControl().AddItemsList(boraInfoResponse.getSpectrumInfo().getFilterListList());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // modulation
        try {
            ucAnalogReconFWS.setModulationList(FXCollections.observableArrayList(boraInfoResponse.getSoundInfo().getDemodulationNameListList()));
            ucPaneSpectr.getSmallChart().addItemsModulation(FXCollections.observableArrayList(boraInfoResponse.getSoundInfo().getDemodulationNameListList()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // частота дискретизации
        try {
            ObservableList<Integer> list = FXCollections.observableArrayList(new ArrayList<>() {});
            for (int i = 0; i < boraInfoResponse.getSoundInfo().getSampleRateListList().size(); i++) {

                list.add(Integer.parseInt(String.valueOf(boraInfoResponse.getSoundInfo().getSampleRateListList().get(i))));
            }
            ucProperties.UpdateComboBox(list);
//            ucAnalogReconFWS.setSamplingFreqList(list);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        // полоса, Гц
        try {
            ucAnalogReconFWS.setBandAudioList(FXCollections.observableArrayList(boraInfoResponse.getSoundInfo().getBandWidthListList()));
            ucPaneSpectr.getSmallChart().addItemsLine(FXCollections.observableArrayList(boraInfoResponse.getSoundInfo().getBandWidthListList()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ----------------Установить в контролах comboBox
    }

    @Override
    public void setBoraFilter(Kvetka.DefaultResponse defaultResponse) {

        // Фильтр
        try {

            Platform.runLater(() -> {

               ucPaneSpectr.getFiltersControl().setItemInSignalState(defaultResponse.getIsSucceeded());
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void buttonFilterSelected(double v) {

        Platform.runLater(() -> {

            if (clientKvetka != null) {

                if(v > 0) {

                    clientKvetka.setBoraFilterAsync(Kvetka.SetBoraFilterRequest.newBuilder()
                            .setFilter(v).build());
                }
            }
        });
    }
    // endregion

    // region Точность пеленгования
    @Override
    public void OnDFAccuracyParams(double freq, float band) {

        Platform.runLater(() -> {

            clientKvetka.getBearing(Kvetka.BearingRequest.newBuilder()
                    .setStartFrequencyKhz((int) (freq - band / 2))
                    .setEndFrequencyKhz((int) (freq + band / 2)).build());
        });
    }

    @Override
    public void OnDFAccuracyParams(double freq, float band, int count) {

        Platform.runLater(() -> {

            clientKvetka.getBearings(Kvetka.MultibleBearingRequest.newBuilder()
                    .setBearingRequest(Kvetka.BearingRequest.newBuilder()
                            .setStartFrequencyKhz((int) (freq - band / 2))
                            .setEndFrequencyKhz((int) (freq + band / 2)).build())
                    .setBearingCount(count).build());
        });
    }
    // endregion

    // region Звук
    @Override
    public void getSound(Bora.SoundResponse soundResponse) {

        Platform.runLater(() -> {

//            ucAnalogReconFWS.UpdateTBListenSelect(soundResponse.getSoundInfo().getId(),
//                    soundResponse.getSoundInfo().getIsListen());

            if (soundResponse.getSoundInfo().getIsListen()) {

                ucPaneSpectr.getSmallChart().setPlayButtonAsSignal();

                // звук Дима
//                byte[] masAudioData = soundResponse.getAudioData().toByteArray();
//                soundBora.play(masAudioData);
            } else {

                ucPaneSpectr.getSmallChart().setPlayButtonAsNotSignal();

//                soundBora.stop();
            }
        });
    }

    @Override
    public void setTimeDomain(Bora.TimeDomainResponse timeDomainResponse) {

        Platform.runLater(() -> {

            ucAnalogReconFWS.UpdateTBWriteFileSelect(timeDomainResponse.getHandledRequestsList().get(0).getId(),
                    timeDomainResponse.getHandledRequestsList().get(0).getIsListen());

//            if (timeDomainResponse.getHandledRequestsList().get(0).getWriteFile()) {
//
//                ucPaneSpectr.getSmallChart().setRecordingButtonAsSignal();
//            } else {
//
//                ucPaneSpectr.getSmallChart().setRecordingButtonAsNotSignal();
//            }
        });
    }

    @Override
    public void Playing(AnalogReconFWSModel analogReconFWSModel, boolean isPlaying) {

        if (clientKvetka != null) {

            clientKvetka.getSoundRequestAsync(Bora.SoundRequest.newBuilder()
                    .setId(analogReconFWSModel.getId())
                    .setCentralFrequencyKhz(analogReconFWSModel.getFrequency())
                    .setDemodulation(analogReconFWSModel.getTypeSignal())
                    .setBandwidthKhz(analogReconFWSModel.getBandAudio())
                    .setSampleRate(PropertiesOperations.getLocalProperties().getSound().getSound())
                    .setMode(PropertiesOperations.getLocalProperties().getSound().getType() == 1 ? Bora.GainControl.Auto : Bora.GainControl.Manual)
                    .setAgcLevel(PropertiesOperations.getLocalProperties().getSound().getAGLLevel())
                    .setMgcCoef(PropertiesOperations.getLocalProperties().getSound().getMGCFactor())
                    .setStartTime(Timestamp.getDefaultInstance())
                    .setDurationMs(1000)
                    .setIsListen(isPlaying)
                    .build());

            narrowStart = (int) Math.floor(analogReconFWSModel.getFrequency() - analogReconFWSModel.getBandAudio());
            narrowEnd = (int) Math.ceil(analogReconFWSModel.getFrequency() + analogReconFWSModel.getBandAudio());

            if (narrowSpectrumTask == null) {

                Platform.runLater(() -> {

                narrowSpectrumTask = new Thread() {

                    @Override
                    public void run() {
                        while (true) {

                            if (clientKvetka != null) {

                                clientKvetka.getSpectrumAsync(Kvetka.SpectrumRequest.newBuilder()
                                        .setMode(Kvetka.DataOutputMode.Once).setStartFrequencyKhz(narrowStart).setEndFrequencyKhz(narrowEnd)
                                        .setStepHz(25).build());
                            }
                            try {
                                Thread.sleep(128);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };
                narrowSpectrumTask.start();
            });
            }
        }
    }

    //    @Override
//    public void Recording(AnalogReconFWSModel analogReconFWSModel, boolean isRecording) {
//
//        if (clientKvetka != null) {
//
//            clientKvetka.setTimeDomainAsync(Bora.SoundRequest.newBuilder()
//                    .setId(analogReconFWSModel.getId())
//                    .setCentralFrequencyKhz(analogReconFWSModel.getFrequency())
//                    .setDemodulation(analogReconFWSModel.getTypeSignal())
//                    .setBandwidthKhz(analogReconFWSModel.getBandAudio())
//                    .setSampleRate(PropertiesOperations.getLocalProperties().getSound().getSound())
//                    .setMode(PropertiesOperations.getLocalProperties().getSound().getType() == 1 ? Bora.GainControl.Auto : Bora.GainControl.Manual)
//                    .setAgcLevel(PropertiesOperations.getLocalProperties().getSound().getAGLLevel())
//                    .setMgcCoef(PropertiesOperations.getLocalProperties().getSound().getMGCFactor())
//                    .setStartTime(Timestamp.getDefaultInstance())
//                    .setDurationMs(1000)
//                    .setWriteFile(isRecording)
//                    .build());
//        }
//    }
    // endregion

}
