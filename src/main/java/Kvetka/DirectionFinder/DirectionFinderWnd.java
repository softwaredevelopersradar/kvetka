package Kvetka.DirectionFinder;

import Kvetka.Interfaces.IWndEvents;
import Kvetka.Map.MapOperations;
import Kvetka.Map.NumberFormat;
import Kvetka.Properties.PropertiesMap;
import Kvetka.Properties.PropertiesOperations;
import KvetkaModels.StationsModel;
import KvetkaRadarLib.*;
import KvetkaSpectrWithPelengsLib.Pane_SpectrAndPelengs;
import KvetkaSpectrWithPelengsLib.PointPelengs;
import ReconFWSControl.ReconFWSControl;
import ValuesCorrectLib.Converters;
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.layers.RasterLayer;
import com.esri.arcgisruntime.mapping.*;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.portal.Portal;
import com.esri.arcgisruntime.raster.Raster;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class DirectionFinderWnd extends AnchorPane implements Initializable {

    public static Stage WindowDirectionFinder;
    static Scene FrontEndDirectionFinder;

    private List<IWndEvents> listenersDirectionFinder = new ArrayList<IWndEvents>();
    public void addListenerDirectionFinder(IWndEvents toAdd) {
        listenersDirectionFinder.add(toAdd);
    }

    public static boolean isLocalUpdate = false;
    //region Controls, Containers
    @FXML public ToggleButton tbShowMap;
    @FXML public ToggleButton tbShowPanelPanorama;
    @FXML public Button bShowPanelsDirectionFinderDefault;

    @FXML public SplitPane splitCenterOfBorderPane;

    @FXML public AnchorPane anchorPaneMap;
    @FXML public AnchorPane anchorPanePanorama;

    @FXML public CheckBox chbResFWS;
    @FXML public CheckBox chbResFHSS;
    @FXML public CheckBox chbComplex;
    @FXML public CheckBox chbBearing;
    @FXML public CheckBox chbSignatures;

    @FXML public TextField tfLatitudeDF;
    @FXML public TextField tfLongitudeDF;
    @FXML public TextField tfSK42LatDF;
    @FXML public TextField tfSK42LonDF;
    @FXML public TextField tfSeaHeightDF;
    @FXML public TextField tfScaleDF;
    // endregion

    // region UserControls
    @FXML public ReconFWSControl ucReconFWS;
    @FXML public Pane_SpectrAndPelengs ucPaneSpectrPelengs;
    @FXML public OnlyChart_PolarWithArrow ucChartPolar;
    // endregion

    // region ArcGISS
    @FXML public SceneView sceneViewMapDF;

    private Basemap basemap;
    private Portal portal;
    ArcGISScene arcGISSceneDF;
    // endregion

    public DirectionFinderWnd() throws IOException {

       // ArcGISRuntimeEnvironment.setInstallDirectory("100.11.2");

        InitDirectionFinderWnd();
        InitMap();

        WindowDirectionFinder.setTitle("Пеленгатор");
        WindowDirectionFinder.getIcons().add(new Image(getClass().getResourceAsStream("Ico/ShowDirectionFinder.png")));
    }

    private void InitDirectionFinderWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("DirectionFinderWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndDirectionFinder = new Scene(fxmlLoader.load(), 800, 600);
            WindowDirectionFinder = new Stage();
            WindowDirectionFinder.initModality(Modality.WINDOW_MODAL);
            WindowDirectionFinder.setMaximized(true);
            WindowDirectionFinder.setResizable(true);
            WindowDirectionFinder.setScene(FrontEndDirectionFinder);
            WindowDirectionFinder.show();

            WindowDirectionFinder.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing
                    for (IWndEvents directionFinderEvent : listenersDirectionFinder)
                        directionFinderEvent.OnCloseDirectionFinder();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void InitMap() throws IOException {

        basemap = new Basemap();
        arcGISSceneDF = new ArcGISScene(basemap);
        sceneViewMapDF.setArcGISScene(arcGISSceneDF);

        //
        sceneViewMapDF.setInteractionListener(new SceneView.DefaultInteractionListener(sceneViewMapDF)
        {

            public void onMouseMoved(javafx.scene.input.MouseEvent e)
            {
                javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(e.getX()),
                        Math.round(e.getY()));
                com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMapDF.screenToBaseSurface(screenPoint);

                tfLatitudeDF.clear();
                tfLongitudeDF.clear();
                tfSK42LatDF.clear();
                tfSK42LonDF.clear();
                tfSeaHeightDF.clear();

                if (surfacePoint != null) {
                    tfLatitudeDF.appendText(Converters.LatitudeConverter(surfacePoint.getY()));
                    tfLongitudeDF.appendText(Converters.LongitudeConverter(surfacePoint.getX()));
//                    tfLatitudeDF.appendText(String.valueOf(NumberFormat.df6.format(surfacePoint.getY())));
//                    tfLongitudeDF.appendText(String.valueOf(NumberFormat.df6.format(surfacePoint.getX())));
                    tfSeaHeightDF.appendText(String.valueOf(((double) ((int) (surfacePoint.getZ() * 100.0))) / 100.0));
                    double[] LatLon = MapOperations.WGS84ToSK42Meters(surfacePoint.getY(), surfacePoint.getX(), 0);
                    tfSK42LatDF.appendText(String.valueOf(NumberFormat.df1.format(LatLon[0])));
                    tfSK42LonDF.appendText(String.valueOf(NumberFormat.df1.format(LatLon[1])));
                }
                e.consume();
            }
//            public void onMousePressed(javafx.scene.input.MouseEvent e)
//            {
//
//                if(e.getButton() == MouseButton.PRIMARY) {
//
//                    if((getAzimuth==true) && (clickAz == 0)) {
//                        javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(e.getX()),
//                                Math.round(e.getY()));
//                        com.esri.arcgisruntime.geometry.Point surfacePoint = sceneView.screenToBaseSurface(screenPoint);
//
//                        azLatitude1 = ((double) ((int) (surfacePoint.getY() * 100.0))) / 100.0;
//                        azLongitude1 = ((double) ((int) (surfacePoint.getX() * 100.0))) / 100.0;
//                        clickAz++;
//
//
//                    }
//                    else if(clickAz == 1)
//                    {
//                        javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(e.getX()),
//                                Math.round(e.getY()));
//                        com.esri.arcgisruntime.geometry.Point surfacePoint = sceneView.screenToBaseSurface(screenPoint);
//
//                        azLatitude2 = ((double) ((int) (surfacePoint.getY() * 100.0))) / 100.0;
//                        azLongitude2 = ((double) ((int) (surfacePoint.getX() * 100.0))) / 100.0;
//                        clickAz = 0;
//                        getAzimuth = false;
//                        azObj = new AzimuthModel(azLongitude1,azLatitude1,azLongitude2,azLatitude2);
//                        calculateAzimuth(azObj);
//                    }
//
//
////                    Viewpoint current = sceneView.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
////                    scale.appendText(Double.toString(current.getTargetScale()));
////                    e.consume();
//                }
//                else {
//                    // let the default listener you've overridden deal with other events
//                    super.onMousePressed(e);
//                }
//            }
            public void onScroll(javafx.scene.input.ScrollEvent e)
            {
                try {
                    tfScaleDF.clear();
                    Viewpoint current = sceneViewMapDF.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
                    double dScale = ((double) ((int) (current.getTargetScale() * 100.0))) / 100.0;
                    NumberFormat.df0.setMaximumFractionDigits(0);
                    tfScaleDF.appendText("1:" + NumberFormat.df0.format(dScale));
                    e.consume();
                    super.onScroll(e);
                }
                catch(Exception ex)
                {

                }
            }
        });

        if(!Files.exists(Paths.get(PropertiesOperations.getLocalProperties().getMap().getMapDFPath()))) {

            PropertiesOperations.getLocalProperties().getMap().setMapDFPath("");
            PropertiesOperations.getLocalProperties().getMap().setMatrixDFPath("");
            PropertiesOperations.YamlSave(PropertiesOperations.getLocalProperties());
            PropertiesOperations.setLocalProperties(PropertiesOperations.YamlLoad());

            isLocalUpdate = true;
        }

        if(PropertiesOperations.getLocalProperties().getMap().getMapDFPath() != "") {

            Raster raster = new Raster(PropertiesOperations.getLocalProperties().getMap().getMapDFPath());
            PropertiesMap.setRasterLayerMapDF(new RasterLayer(raster));
            arcGISSceneDF.getOperationalLayers().add(PropertiesMap.getRasterLayerMapDF());
        }
        if(PropertiesOperations.getLocalProperties().getMap().getMatrixDFPath() != "") {

            ArrayList listMatrixPath = new ArrayList();
            listMatrixPath.add(PropertiesOperations.getLocalProperties().getMap().getMatrixDFPath());
            PropertiesMap.setRasterElevationSourceMapDF(new RasterElevationSource(listMatrixPath));
            Surface surface = new Surface();
            surface.getElevationSources().add(PropertiesMap.getRasterElevationSourceMapDF());
            surface.setElevationExaggeration(1);
            arcGISSceneDF.setBaseSurface(surface);
        }
    }

    // region CheckBoxes Checked
    public void ResFWS_Click(ActionEvent actionEvent) {

        if(chbResFWS.isSelected()) {

            int i = 90;
        } else {


        }
    }

    public void ResFHSS_Click(ActionEvent actionEvent) {

        if(chbResFHSS.isSelected()) {

            int i = 90;
        } else {


        }
    }

    public void Complex_Click(ActionEvent actionEvent) {

        if(chbComplex.isSelected()) {

            MapOperations.AddStationsDF(sceneViewMapDF);
        } else {

            MapOperations.RemoveStationsDF(sceneViewMapDF);
        }
    }

    public void Bearing_Click(ActionEvent actionEvent) {

        if(chbBearing.isSelected()) {

            int i = 90;
        } else {


        }
    }

//    public void Signatures_Click(ActionEvent actionEvent) {
//
//        if(chbSignatures.isSelected()) {
//
//            int i = 90;
//        } else {
//
//        }
//    }
    // endregion

    public void ShowMap_Click(ActionEvent actionEvent) {

        if(tbShowMap.isSelected()) {

            anchorPaneMap.setVisible(false);
            anchorPanePanorama.setVisible(true);
        } else {

            anchorPanePanorama.setVisible(false);
            anchorPaneMap.setVisible(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        splitCenterOfBorderPane.getDividers().get(0).positionProperty().addListener((obs, oldPos, newPos) -> {

            setDividersPositionCenterDirectionFinder(new double[] { oldPos.doubleValue(),newPos.doubleValue() });
        });
        splitCenterOfBorderPane.getDividers().get(0).positionProperty().set(0.0);

        ShowPanelsDirectionFinderDefault_Click(new ActionEvent());

        tbShowMap.setSelected(true);

    }

    // region Markup DirectionFinderWnd
    public void ShowPanelPanorama_Click(ActionEvent actionEvent) {

        if(tbShowPanelPanorama.isSelected())
            splitCenterOfBorderPane.setDividerPosition(0, 0.6);
        else
            splitCenterOfBorderPane.setDividerPosition(0,1.0);
    }

    public void ShowPanelsDirectionFinderDefault_Click(ActionEvent actionEvent) {

        splitCenterOfBorderPane.setDividerPosition(0, 0.6);

        tbShowPanelPanorama.setSelected(true);
    }

    private double[] DividersPositionCenterDirectionFinder = new double[2];
    public double[] getDividersPositionCenterDirectionFinder() { return DividersPositionCenterDirectionFinder; }
    public void setDividersPositionCenterDirectionFinder(double[] dividersPositionCenterDirectionFinder) {

        DividersPositionCenterDirectionFinder = dividersPositionCenterDirectionFinder;
        UpdateMarkupCenterDirectionFinder();
    }

    private void UpdateMarkupCenterDirectionFinder() {

        double[] positionCenter = getDividersPositionCenterDirectionFinder();

        if(positionCenter[1] > 0.97 && positionCenter[1] <= 1.0) {

            tbShowPanelPanorama.setSelected(false);
        }
        else {
            tbShowPanelPanorama.setSelected(true);
        }
    }
    // endregion

    // region ButtonsClick of Map
    /**
     * Open MapDF
     * @param actionEvent
     */
    public void OpenMapDF_Click(ActionEvent actionEvent) {

        try {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Открыть карту");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Map Files", "*.tif"));
            File mapPath = fileChooser.showOpenDialog(WindowDirectionFinder);

            if (mapPath != null) {

                Raster raster = new Raster(mapPath.getPath());
                PropertiesMap.setRasterLayerMapDF(new RasterLayer(raster));
                arcGISSceneDF.getOperationalLayers().add(PropertiesMap.getRasterLayerMapDF());

                PropertiesMap.setMapDFPath(mapPath.getPath());

                for (IWndEvents directionFinderEvent : listenersDirectionFinder)
                    directionFinderEvent.OnChangeLocalMapPath();
            }
        } catch (Exception ex) {
            System.out.println("Ошибка!");
        }
    }

    /**
     * Open Matrix
     * @param actionEvent
     */
    public void OpenMatrixDF_Click(ActionEvent actionEvent) {

        try {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Открыть матрицу высот");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Matrix Files", "*.tif"));
            File matrixPath = fileChooser.showOpenDialog(WindowDirectionFinder);

            if (matrixPath != null) {

                ArrayList listMatrixPath = new ArrayList();
                listMatrixPath.add(matrixPath.getPath());
                PropertiesMap.setRasterElevationSourceMapDF(new RasterElevationSource(listMatrixPath));

                Surface surface = new Surface();
                surface.getElevationSources().add(PropertiesMap.getRasterElevationSourceMapDF());
                surface.setElevationExaggeration(1);
                arcGISSceneDF.setBaseSurface(surface);

                PropertiesMap.setMatrixDFPath(matrixPath.getPath());

                for (IWndEvents directionFinderEvent : listenersDirectionFinder)
                    directionFinderEvent.OnChangeLocalMapPath();
            }
        } catch (Exception ex) {
            System.out.println("Ошибка!");
        }
    }

    /**
     * Close Map
     * @param actionEvent
     */
    public void CloseMapDF_Click(ActionEvent actionEvent) {

        if(arcGISSceneDF.getOperationalLayers().contains(PropertiesMap.getRasterLayerMapDF())) {
            arcGISSceneDF.getOperationalLayers().remove(PropertiesMap.getRasterLayerMapDF());
        }
    }

    /**
     * Увеличить
     * @param actionEvent
     */
    public void ZoomInDF_Click(ActionEvent actionEvent) {

        Viewpoint current = sceneViewMapDF.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        Viewpoint zoomedIn = new Viewpoint((Point) current.getTargetGeometry(), current.getTargetScale() / 2.0);
        sceneViewMapDF.setViewpointAsync(zoomedIn);
    }

    /**
     * Уменьшить
     * @param actionEvent
     */
    public void ZoomOutDF_Click(ActionEvent actionEvent) {

        Viewpoint current = sceneViewMapDF.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        Viewpoint zoomedOut = new Viewpoint((Point) current.getTargetGeometry(), current.getTargetScale() * 2.0);
        sceneViewMapDF.setViewpointAsync(zoomedOut);
    }

    public void Scale1_1DF_Click(ActionEvent actionEvent) {

        StationsModel model = MapOperations.getListStationsModel().stream()
                .filter(x -> x.getIsOwn() == true)
                .findAny()
                .orElse(null);

        Viewpoint currentViewpoint = sceneViewMapDF.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        Viewpoint newViewPoint = new Viewpoint(model.getCoordinates().latitude, model.getCoordinates().longitude, currentViewpoint.getTargetScale());
        sceneViewMapDF.setViewpoint(newViewPoint);
    }
    // endregion
}
