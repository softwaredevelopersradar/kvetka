package Kvetka.Properties;

import Kvetka.Interfaces.IWndEvents;
import com.esri.arcgisruntime.layers.RasterLayer;
import com.esri.arcgisruntime.mapping.RasterElevationSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PropertiesMap {

    // region Map, Matrix
    public static String MapPath;
    public static String getMapPath() { return MapPath; }
    public static void setMapPath(String mapPath) throws IOException {

        MapPath = mapPath;
        PropertiesOperations.getLocalProperties().getMap().setMapPath(mapPath);
        PropertiesOperations.YamlSave(PropertiesOperations.getLocalProperties());
    }

    public static String MatrixPath;
    public static String getMatrixPath() { return MatrixPath; }
    public static void setMatrixPath(String matrixPath) throws IOException {

        MatrixPath = matrixPath;
        PropertiesOperations.getLocalProperties().getMap().setMatrixPath(matrixPath);
        PropertiesOperations.YamlSave(PropertiesOperations.getLocalProperties());
    }

    public static RasterLayer RasterLayerMap;
    public static RasterLayer getRasterLayerMap() { return RasterLayerMap; }
    public static void setRasterLayerMap(RasterLayer rasterLayerMap) { RasterLayerMap = rasterLayerMap; }

    public static RasterElevationSource RasterElevationSourceMap;
    public static RasterElevationSource getRasterElevationSourceMap() { return RasterElevationSourceMap; }
    public static void setRasterElevationSourceMap(RasterElevationSource rasterElevationSourceMap) { RasterElevationSourceMap = rasterElevationSourceMap; }
    // endregion

    // region MapDF, MatrixDF
    public static String MapDFPath;
    public static String getMapDFPath() { return MapDFPath; }
    public static void setMapDFPath(String mapDFPath) throws IOException {

        MapDFPath = mapDFPath;
        PropertiesOperations.getLocalProperties().getMap().setMapDFPath(mapDFPath);
        PropertiesOperations.YamlSave(PropertiesOperations.getLocalProperties());
    }

    public static String MatrixDFPath;
    public static String getMatrixDFPath() { return MatrixDFPath; }
    public static void setMatrixDFPath(String matrixDFPath) throws IOException {

        MatrixDFPath = matrixDFPath;
        PropertiesOperations.getLocalProperties().getMap().setMatrixDFPath(matrixDFPath);
        PropertiesOperations.YamlSave(PropertiesOperations.getLocalProperties());
    }

    public static RasterLayer RasterLayerMapDF;
    public static RasterLayer getRasterLayerMapDF() { return RasterLayerMapDF; }
    public static void setRasterLayerMapDF(RasterLayer rasterLayerMapDF) { RasterLayerMapDF = rasterLayerMapDF; }

    public static RasterElevationSource RasterElevationSourceMapDF;
    public static RasterElevationSource getRasterElevationSourceMapDF() { return RasterElevationSourceMapDF; }
    public static void setRasterElevationSourceMapDF(RasterElevationSource rasterElevationSourceMapDF) { RasterElevationSourceMapDF = rasterElevationSourceMapDF; }
    // endregion
}
