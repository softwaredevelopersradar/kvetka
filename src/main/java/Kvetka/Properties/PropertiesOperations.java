package Kvetka.Properties;

import PropertiesControl.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fazecast.jSerialComm.SerialPort;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class PropertiesOperations {

    public static ObjectMapper objectMapper;

    public static LocalSettings LocalProperties = new LocalSettings();

    public static LocalSettings getLocalProperties() {
        return LocalProperties;
    }

    public static void setLocalProperties(LocalSettings localProperties) throws IOException {

        LocalProperties = localProperties;
        YamlSave(LocalProperties);
    }

    /**
     * Загрузить из файла LocalProperties
     *
     * @return
     * @throws IOException
     */
    public static LocalSettings YamlLoad() throws IOException {

        File file = new File("LocalProperties.yaml");

        if (file.exists()) {

            setLocalProperties(LocalPropertiesLoad());
        } else {

            setLocalProperties(DefaultLocalProperties());
            YamlSave(getLocalProperties());
        }
        return getLocalProperties();
    }

    /**
     * Сохранить в файл LocalProperties
     *
     * @param localSettings
     * @throws IOException
     */
    public static void YamlSave(LocalSettings localSettings) throws IOException {

        objectMapper = new ObjectMapper(new YAMLFactory());
        objectMapper.writeValue(new File("LocalProperties.yaml"), getLocalProperties());
    }

    /**
     * Загрузить из файла LocalProperties
     *
     * @return
     * @throws IOException
     */
    public static LocalSettings LocalPropertiesLoad() throws IOException {

        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());

        LocalSettings localSettings = objectMapper.readValue(new File("LocalProperties.yaml"), LocalSettings.class);

        if (localSettings.getGeneral().getAccess() == AccessEnum.ADMIN) {
            localSettings.getGeneral().setAccess(AccessEnum.USER);
        }

        String[] stringArgsComNames = new String[SerialPort.getCommPorts().length];
        for (int i = 0; i < SerialPort.getCommPorts().length; i++) {
            stringArgsComNames[i] = SerialPort.getCommPorts()[i].getSystemPortName();
        }

        if (!Arrays.asList(stringArgsComNames).contains(localSettings.getBpss().getCom_Port()) && SerialPort.getCommPorts().length != 0) {
            localSettings.getBpss().setCom_Port(SerialPort.getCommPorts()[0].getSystemPortName());
            objectMapper.writeValue(new File("LocalProperties.yaml"), localSettings);
            objectMapper.readValue(new File("LocalProperties.yaml"), LocalSettings.class);
        }

        return localSettings;
    }

    /**
     * Записать в LocalProperties настройки по умолчанию
     *
     * @return
     */
    public static LocalSettings DefaultLocalProperties() {

        General general1 = new General(1, AccessEnum.USER, LanguageEnum.RUSSIAN, CoordinateViewEnum.DDDDDDDD);
        Map map1 = new Map("", "", "", "", 0);
        //Map mapDF = new Map("", "");
        TwoFields db1 = new TwoFields("127.0.0.1", 80);
        TwoFields df1 = new TwoFields("127.0.0.1", 80);
        ED ed1 = new ED(1, new FourFields(new TwoFields("127.0.0.1", 80), new TwoFields("127.0.0.1", 80)), new FourFields(new TwoFields("127.0.0.1", 80), new TwoFields("127.0.0.1", 80)), new FourFields(new TwoFields("127.0.0.1", 80), new TwoFields("127.0.0.1", 80)));
        PC pc1 = new PC(1, new TwoFields("127.0.0.1", 80), new TwoFields("127.0.0.1", 80));
        BPSS bpss1 = null;

        if (SerialPort.getCommPorts().length != 0) {

            bpss1 = new BPSS(SerialPort.getCommPorts()[0].getSystemPortName(), 2400);
        } else if (SerialPort.getCommPorts().length == 0) {

            bpss1 = new BPSS(2400);
        }

        Sound sound1 = new Sound(0, 0.0, 0.0, 1);
//        Sound sound1 = new Sound(0, 0.0, "", "");
//        return new LocalSettings(general1, map1, db1, df1, ed1, pc1, bpss1);
        return new LocalSettings(general1, map1, db1, df1, ed1, pc1, bpss1, sound1);
    }
}