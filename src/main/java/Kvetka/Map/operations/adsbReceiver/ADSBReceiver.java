package Kvetka.Map.operations.adsbReceiver;

import Kvetka.Map.MapMarkerStyle;
import KvetkaModels.AirplanesModel;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.arcgisservices.LabelingPlacement;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.labeling.ArcadeLabelExpression;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.ColorUtil;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import com.esri.arcgisruntime.symbology.TextSymbol;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class ADSBReceiver {

    Map<String, GraphicsOverlay> map = new HashMap<>();
    GraphicsOverlay graphicsOverlay;
    Graphic grAirPlane;
    static List<AirplanesModel> tempAir = new ArrayList<>();
    MapMarkerStyle styles = new MapMarkerStyle();

    static LinearUnit linearUnit = new LinearUnit(LinearUnitId.KILOMETERS);
    static AngularUnit angularUnit = new AngularUnit(AngularUnitId.DEGREES);

    /**
     *
     * @param data it is current airplanes
     * @param sceneView it is current map scene
     */
    public void setData(List<AirplanesModel> data, SceneView sceneView) {

        if (!map.isEmpty()) {
            for (int i = 0; i < data.size(); i++) {

                if (map.containsKey(data.get(i).getIcao())) {
                    for (Map.Entry m : map.entrySet()) {
                        if (m.getKey().equals(data.get(i).getIcao())) {
                            for (AirplanesModel t : tempAir) {
                                if (m.getKey().equals(t.getIcao())) {
                                    if (Double.compare(t.getCoordinates().longitude, data.get(i).getCoordinates().longitude) != 0 & Double.compare(t.getCoordinates().latitude, data.get(i).getCoordinates().latitude) != 0) {
                                        var currGrOv = new GraphicsOverlay();
                                        currGrOv.setLabelsEnabled(true);

                                        currGrOv = (GraphicsOverlay) m.getValue();
                                        var graphics = currGrOv.getGraphics();
                                        var lastEl = new Graphic();

                                        if (graphics.size() == 1) {
                                            lastEl = graphics.get(graphics.size() - 1);
                                        } else {
                                            lastEl = graphics.get(graphics.size() - 2);
                                        }

                                        currGrOv.getGraphics().remove(lastEl);

                                        if (graphics.size() > 5) {
                                            currGrOv.getGraphics().remove(0);
                                        }

                                        Point point = new Point(data.get(i).getCoordinates().longitude, data.get(i).getCoordinates().latitude, SpatialReferences.getWgs84());

                                        PointCollection collection = new PointCollection(SpatialReferences.getWgs84());
                                        collection.add(t.getCoordinates().longitude, t.getCoordinates().latitude);
                                        collection.add(point);

                                        double azimuthRez = 0;
                                        GeodeticDistanceResult distance = GeometryEngine.distanceGeodetic(collection.get(0), collection.get(1), linearUnit, angularUnit, GeodeticCurveType.GEODESIC);
                                        if (distance.getAzimuth1() > 0) {
                                            azimuthRez = distance.getAzimuth1();
                                        } else {
                                            azimuthRez = 360 + distance.getAzimuth1();
                                        }

                                        if(distance.getDistance() > 500){
                                            collection.remove(1);
                                            collection.add(t.getCoordinates().longitude, t.getCoordinates().latitude);
                                        }

                                        Polyline line = new Polyline(collection);

                                        PictureMarkerSymbol angleAir = new PictureMarkerSymbol(styles.air);
                                        angleAir.setAngle((float) azimuthRez);
                                        grAirPlane = new Graphic(point, angleAir);


                                        currGrOv.getGraphics().add(grAirPlane);

                                        grAirPlane.getAttributes().put("NAME", t.getIcao());
                                        LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                                        currGrOv.getLabelDefinitions().add(labelDefinition);

                                        grAirPlane = new Graphic(line, MapMarkerStyle.polylineSymbol);

                                        currGrOv.getGraphics().add(grAirPlane);

                                        t.getCoordinates().longitude = data.get(i).getCoordinates().longitude;
                                        t.getCoordinates().latitude = data.get(i).getCoordinates().latitude;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //ADD NEW AIRPLANE
                    Point point = new Point(data.get(i).getCoordinates().longitude, data.get(i).getCoordinates().latitude, SpatialReferences.getWgs84());

                    styles = new MapMarkerStyle();
                    grAirPlane = new Graphic(point, styles.airplane);
                    graphicsOverlay = new GraphicsOverlay();
                    graphicsOverlay.setLabelsEnabled(true);

                    graphicsOverlay.getGraphics().add(grAirPlane);

                    grAirPlane.getAttributes().put("NAME", data.get(i).getIcao());
                    LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                    graphicsOverlay.getLabelDefinitions().add(labelDefinition);

                    sceneView.getGraphicsOverlays().add(graphicsOverlay);

                    map.put(data.get(i).getIcao(), graphicsOverlay);
                    tempAir.add(data.get(i));
                }
            }
        } else {
            for (AirplanesModel airPlane : data) {
                Point point = new Point(airPlane.getCoordinates().longitude, airPlane.getCoordinates().latitude, SpatialReferences.getWgs84());

                styles = new MapMarkerStyle();
                grAirPlane = new Graphic(point, styles.airplane);
                graphicsOverlay = new GraphicsOverlay();
                graphicsOverlay.setLabelsEnabled(true);

                graphicsOverlay.getGraphics().add(grAirPlane);

                grAirPlane.getAttributes().put("NAME", airPlane.getIcao());
                LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                graphicsOverlay.getLabelDefinitions().add(labelDefinition);

                sceneView.getGraphicsOverlays().add(graphicsOverlay);

                map.put(airPlane.getIcao(), graphicsOverlay);
                tempAir.add(airPlane);
            }
        }
    }

    public void clearAir(SceneView sceneView){
        hideGraphics(sceneView);
        map = new HashMap<>();
        tempAir = new ArrayList<>();
    }

    public void deleteAir(AirplanesModel airs, SceneView sceneView){
        if(!map.isEmpty()){
            for(Map.Entry m : map.entrySet()){
                if(m.getKey().equals(airs.getIcao())){
                    sceneView.getGraphicsOverlays().remove((GraphicsOverlay) m.getValue());
                    map.remove(m.getKey());
                    tempAir.remove(airs);
                }
            }
        }
    }

    public void showGraphics(SceneView sceneView) {
        if (!map.isEmpty()) {
            for (Map.Entry m : map.entrySet()) {
                sceneView.getGraphicsOverlays().add((GraphicsOverlay) m.getValue());
            }
        }
    }

    public void hideGraphics(SceneView sceneView) {
        if (!map.isEmpty()) {
            for (Map.Entry m : map.entrySet()) {
                sceneView.getGraphicsOverlays().remove((GraphicsOverlay) m.getValue());
            }
        }
    }

    private static @NotNull LabelDefinition makeLabelDefinition(String party, String description, Color color) {
        // create text symbol for styling the label
        var textSymbol = new TextSymbol();
        textSymbol.setSize(12);
        textSymbol.setColor(ColorUtil.colorToArgb(color));
        textSymbol.setHaloColor(ColorUtil.colorToArgb(Color.WHITE));
        textSymbol.setHaloWidth(2);

        // create a label definition with an Arcade expression script
        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME");

/*        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME + \" (\" + left($feature.PARTY,1) + \")\\nDistrict \" + $feature.CDFIPS");  //*/
        var labelDefinition = new LabelDefinition(arcadeLabelExpression, textSymbol);

        labelDefinition.setPlacement(LabelingPlacement.POINT_ABOVE_CENTER);
        labelDefinition.setWhereClause("");  //String.format("PARTY = '%s'", party)

        return labelDefinition;
    }
}
