package Kvetka.Map.operations.zonesOfEnergy;

import Kvetka.Map.operations.RJStationsModel;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;

import java.util.List;

public class ZoneOperations {
    static LinearUnit meters = new LinearUnit(LinearUnitId.METERS);
    static AngularUnit ag = new AngularUnit(AngularUnitId.DEGREES);
    static Graphic symbolGraphic;

    public static Geometry getSector(double bisectorAngle, double angle, Point centerPoint, double arcRadius) {
        LinearUnit unit = new LinearUnit(LinearUnitId.KILOMETERS);

        GeodesicSectorParameters geodesicSectorParameters = new GeodesicSectorParameters();
        geodesicSectorParameters.setCenter(centerPoint);
        geodesicSectorParameters.setAxisDirection(0);
        //geodesicSectorParameters.setMaxPointCount(maxPointCountSpinner.getValue());
        geodesicSectorParameters.setMaxSegmentLength(10000);
        //geodesicSectorParameters.setGeometryType(geometryTypeComboBox.getSelectionModel().getSelectedItem());
        geodesicSectorParameters.setSectorAngle(angle);
        geodesicSectorParameters.setSemiAxis1Length(arcRadius);
        geodesicSectorParameters.setSemiAxis2Length(arcRadius);
        geodesicSectorParameters.setStartDirection(135 + 360 - bisectorAngle);
        geodesicSectorParameters.setLinearUnit(unit);

        // create the geodesic sector parameter
        Geometry sectorGeometry = GeometryEngine.sectorGeodesic(geodesicSectorParameters);

        return sectorGeometry;
    }

    public static Geometry CirclesIntersection(java.util.List<RJStationsModel> circlesCoordinates, double circlesRadius) {
        Polygon polygon1 = null;
        Polygon polygon2 = null;
        Geometry geomIntersection = null;
        LinearUnit km = new LinearUnit(LinearUnitId.KILOMETERS);

        for (int i = 0; i < circlesCoordinates.size(); i++) {
            com.esri.arcgisruntime.geometry.Point circlePoint = new com.esri.arcgisruntime.geometry.Point(circlesCoordinates.get(i).getLongitude(), circlesCoordinates.get(i).getLatitude(), SpatialReferences.getWgs84());
            com.esri.arcgisruntime.geometry.Point mapPoint = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(circlePoint, SpatialReferences.getWebMercator());
            polygon1 = polygon2;
            polygon2 = GeometryEngine.bufferGeodetic(mapPoint, circlesRadius, km, 10, GeodeticCurveType.GEODESIC);
            if (i == 1) {
                geomIntersection = GeometryEngine.intersection(polygon1, polygon2);
            }
            if (i > 1) {
                geomIntersection = GeometryEngine.intersection(geomIntersection, polygon2);
            }
        }
        return geomIntersection;
    }

    public static Geometry CirclesUnion(java.util.List<RJStationsModel> circlesCoordinates, double circlesRadius) {
        Polygon polygon1 = null;
        Polygon polygon2 = null;
        Geometry geomUnion = null;
        LinearUnit km = new LinearUnit(LinearUnitId.KILOMETERS);
        for (int i = 0; i < circlesCoordinates.size(); i++) {
            com.esri.arcgisruntime.geometry.Point circlePoint = new com.esri.arcgisruntime.geometry.Point(circlesCoordinates.get(i).getLongitude(), circlesCoordinates.get(i).getLatitude(), SpatialReferences.getWgs84());
            com.esri.arcgisruntime.geometry.Point mapPoint = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(circlePoint, SpatialReferences.getWebMercator());
            polygon1 = polygon2;
            polygon2 = GeometryEngine.bufferGeodetic(mapPoint, circlesRadius, meters, 10, GeodeticCurveType.GEODESIC);
            if (i == 1) {
                geomUnion = GeometryEngine.union(polygon1, polygon2);
            }
            if (i > 1) {
                geomUnion = GeometryEngine.union(geomUnion, polygon2);
            }
        }

        return geomUnion;
    }

    public static void AddCirclingImages(GraphicsOverlay graphicsOverlay, Point circleCenterPoint, Geometry restrictionZone, javafx.scene.image.Image image, double startAngle, double endAngle, int stepAngle, double distance) {

        for (int i = (int) startAngle; i <= endAngle; i = i + stepAngle) {
            com.esri.arcgisruntime.geometry.Point circleCenterPointMercator = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(circleCenterPoint, SpatialReferences.getWebMercator());
            Point imagePoint = GeometryEngine.moveGeodetic(circleCenterPointMercator, distance, meters, i, ag, GeodeticCurveType.GEODESIC);//проверяем входит ли точка в нужную зону, если не входит, не отрисовываем символ границы image в этой точке
            ;
            if (GeometryEngine.within(imagePoint, restrictionZone) == true) {
                PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(image);

                markerSymbol.setHeight(20);//выосту и ширину можно добавить в качестве парметров
                markerSymbol.setWidth(20);
                markerSymbol.setAngle(i);//поворачиваем картинку на текущий угол, что бы она имела направление от центра координат circleCenterPoint

                markerSymbol.loadAsync();

                markerSymbol.addDoneLoadingListener(() -> {
                    if (markerSymbol.getLoadStatus() == com.esri.arcgisruntime.loadable.LoadStatus.LOADED) {
                        //com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(longitude, latitude, SpatialReferences.getWgs84());
                        symbolGraphic = new Graphic(imagePoint, markerSymbol);
                        graphicsOverlay.getGraphics().add(symbolGraphic);

                    } else {
                        System.out.println("Failed to load: " + markerSymbol.getLoadError().getCause());
                    }
                });
            }
        }
    }

    public static void DrawBond(int stNumber, List<RJStationsModel> RPstations, GraphicsOverlay graphicsOverlay) {
        //Поиск станции для отрисовки по главной и подчинённой, главную считаем первой, нанесенной оператором на карту при решении задачи: RPstations[0]
        if (stNumber - 1 != 0) {
            PointCollection polylinePoints = new PointCollection(SpatialReferences.getWgs84());
            polylinePoints.add(new Point(RPstations.get(stNumber - 1).getLongitude(), RPstations.get(stNumber - 1).getLatitude()));
            polylinePoints.add(new Point(RPstations.get(0).getLongitude(), RPstations.get(0).getLatitude()));
            // create a polyline geometry from the point collection
            Polyline polyline = new Polyline(polylinePoints);
            // create a blue arrowline symbol for the polyline
            SimpleLineSymbol polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID,
                    0xFF0063FF, 1, SimpleLineSymbol.MarkerStyle.ARROW, SimpleLineSymbol.MarkerPlacement.BEGIN_AND_END);

            // create a polyline graphic with the polyline geometry and symbol
            Graphic arrowLine = new Graphic(polyline, polylineSymbol);
            // add the point graphic to the graphics overlay
            graphicsOverlay.getGraphics().add(arrowLine);
        }

    }
}
