package Kvetka.Map.operations.zonesOfEnergy;

import Kvetka.Map.MapMarkerStyle;
import Kvetka.Map.MapOperations;
import Kvetka.Map.operations.RJStationsModel;
import MapModels.EDPOVModel;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.symbology.SimpleFillSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;

import java.util.ArrayList;
import java.util.List;

public class CalculationAndDrawingOfZoneSurfaceWave extends ZoneEnergy {

    static EDPOVModel model = new EDPOVModel();
    public static List<RJStationsModel> stationsModels = new ArrayList<>();
    static Point mapPointEdpov;

    /*@Override
    public void addOverlay(SceneView sceneView) {
        if (zoneSurfaceWaveOverlay == null) {
            zoneSurfaceWaveOverlay = new GraphicsOverlay(GraphicsOverlay.RenderingMode.STATIC);
            sceneView.getGraphicsOverlays().add(zoneSurfaceWaveOverlay);
        }

        stationsModels.clear();
        int index = sceneView.getGraphicsOverlays().indexOf(zoneSurfaceWaveOverlay);
        sceneView.getGraphicsOverlays().get(index).getGraphics().clear();
    }*/

    @Override
    public void calculateParameters(EDPOVModel edpovModel) {
        model = edpovModel;
        deleteOverlay();

        DsvEdpov = model.getMaxSvyazDist();
        PnnEdpov = model.getPeredatchKompleksaPower();
        GnnEdpov = model.getKoefUsilPeredAntennaKompleksa();  //const
        GnpnEdpov = 1;  //const
        rassoglasKoefEdpov = 1;  //const
        PncEdpov = model.getPeredatchikProtivnPower();
        GncEdpov = model.getKoefUsilPeredAntennaProtivn();  //0.1-4  < USE THIS
        GnpcEdpov = 1;  //const
        KnEdpov = 1.2;  //const

        DrpEdpov = DsvEdpov * Math.pow(PnnEdpov * GnnEdpov * GnpnEdpov * rassoglasKoefEdpov / PncEdpov / GncEdpov / GnpcEdpov / KnEdpov, 0.25);

        //получить координаты комплекса из БД и отрисовать

        if (MapOperations.DetectedMyStation.isEmpty()) {
            latComplex = 53.901236;
            longComplex = 27.559818;
        } else {
            latComplex = MapOperations.DetectedMyStation.get(0).getCoordinates().latitude;
            longComplex = MapOperations.DetectedMyStation.get(0).getCoordinates().longitude;
        }

        SimpleLineSymbol simpleLineSymbolYellow = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFFFFFF00, 3);
        SimpleFillSymbol simpleFillSymbolZone = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x8899ff99, null);

        //добавляем окружность(а должен быть сектор), соответствующую зоне радиоподавления с радиусом равным Drp
        com.esri.arcgisruntime.geometry.Point circlePoint = new com.esri.arcgisruntime.geometry.Point(longComplex, latComplex, SpatialReferences.getWgs84());
        mapPointEdpov = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(circlePoint, SpatialReferences.getWebMercator());

        //рисуем сектор СБР
        sectorGeometry = ZoneOperations.getSector(edpovModel.getSbrBisectrix(), edpovModel.getSbrAngle(), mapPointEdpov, DrpEdpov); // / 1000
        Graphic sectorGraphic = new Graphic(sectorGeometry, simpleLineSymbolYellow);
        graphicsOverlay.getGraphics().add(sectorGraphic);//добавили сектор РП поверхностной волной для нашего комплекса

        draw();
    }

    @Override
    public void draw() {
        SimpleLineSymbol simpleLineSymbolYellow = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFFFFFF00, 3);
        SimpleFillSymbol simpleFillSymbolZone = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x8899ff99, null);

        Geometry dsvUnion = ZoneOperations.CirclesUnion(stationsModels, DsvEdpov * 1000);
        Geometry sectorSbr = ZoneOperations.getSector(model.getSbrBisectrix(), model.getSbrAngle(), mapPointEdpov, (DrpEdpov + DsvEdpov) / 1);  // /1000
        Geometry sectorIntersectsDsvUnion = GeometryEngine.intersection(dsvUnion, sectorSbr);
        Geometry IntersectionMinusSectorRp = GeometryEngine.difference(sectorIntersectsDsvUnion, sectorGeometry);
        Graphic resultZone = new Graphic(IntersectionMinusSectorRp, simpleLineSymbolYellow);
        graphicsOverlay.getGraphics().add(resultZone);

        //нанесение символов границ для границы зоны радиоподавления с текущей позиции нашей асп(черные вилки)
        ZoneOperations.AddCirclingImages(graphicsOverlay, new Point(longComplex, latComplex, SpatialReferences.getWgs84()), sectorGeometry, MapMarkerStyle.rj, 0, 360, 20, DrpEdpov * 1000 - 2000);

        //нанесение символов границ для границы зон радиоразведки(связи) противника(черные крестики)
        Geometry resultSum = GeometryEngine.union(sectorGeometry, IntersectionMinusSectorRp);
        Geometry sectorSbrMinusResultZoneAndSectorRP = GeometryEngine.difference(sectorSbr, resultSum);
        for (int i = 0; i < stationsModels.size(); i++) {
            ZoneOperations.AddCirclingImages(graphicsOverlay, new Point(stationsModels.get(i).getLongitude(), stationsModels.get(i).getLatitude(), SpatialReferences.getWgs84()), sectorSbrMinusResultZoneAndSectorRP, MapMarkerStyle.x, 0, 360, 20, DsvEdpov * 1000 + 2000);
        }
    }
}
