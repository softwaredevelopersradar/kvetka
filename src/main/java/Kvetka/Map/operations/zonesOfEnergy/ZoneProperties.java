package Kvetka.Map.operations.zonesOfEnergy;

import com.esri.arcgisruntime.geometry.Geometry;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;

public abstract class ZoneProperties {
    /**
     * @param sectorGeometry sbr sector
     * @param latComplex latitude own complex
     * @param longComplex longitude own complex
     */

    public static GraphicsOverlay graphicsOverlay;
    Geometry sectorGeometry;
    double latComplex;
    double longComplex;

    /**
     * @param DsvEdpov
     * @param PnnEdpov
     * @param GnnEdpov
     * @param GnpnEdpov
     * @param rassoglasKoefEdpov
     * @param PncEdpov
     * @param GncEdpov
     * @param GnpcEdpov
     * @param KnEdpov
     * @param DrpEdpov
     */

    static double DsvEdpov;
    static double PnnEdpov;
    static double GnnEdpov;
    static double GnpnEdpov;
    static double rassoglasKoefEdpov;
    static double PncEdpov;
    static double GncEdpov;
    static double GnpcEdpov;
    static double KnEdpov;
    static double DrpEdpov;

    /**
     * @param hmaxNb
     * @param Dsv
     * @param Pn
     * @param Gn
     * @param rassoglasKoef
     * @param Psv
     * @param Gsv
     * @param Kn
     * @param Drp
     */

    static double hmaxNb;
    static double Dsv;
    static double Pn;
    static double Gn;
    static double rassoglasKoef;
    static double Psv;
    static double Gsv;
    static double Kn;
    static double Drp;
}
