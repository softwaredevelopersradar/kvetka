package Kvetka.Map.operations.zonesOfEnergy;

import MapModels.EDPOVModel;
import MapModels.EDPRVModel;
import com.esri.arcgisruntime.mapping.view.SceneView;

public interface IDrawableZone {
    void addOverlay(SceneView sceneView);

    void calculateParameters(EDPOVModel edpovModel);

    void calculateParameters(EDPRVModel edprvModel);

    void draw();

    void deleteOverlay();

    void clearOverlay(SceneView sceneView);
}
