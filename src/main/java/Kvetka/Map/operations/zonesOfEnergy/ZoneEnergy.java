package Kvetka.Map.operations.zonesOfEnergy;

import MapModels.EDPOVModel;
import MapModels.EDPRVModel;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;

public class ZoneEnergy extends ZoneProperties implements IDrawableZone {
    //public static GraphicsOverlay graphicsOverlay;

    @Override
    public void addOverlay(SceneView sceneView) {
        if (graphicsOverlay == null) {
            graphicsOverlay = new GraphicsOverlay(GraphicsOverlay.RenderingMode.STATIC);
            sceneView.getGraphicsOverlays().add(graphicsOverlay);
        }
    }

    public void calculateParameters(EDPOVModel model) {

    }

    public void calculateParameters(EDPRVModel edprvModel) {

    }

    public void draw() {

    }

    @Override
    public void deleteOverlay() {
        if (graphicsOverlay != null) {
            if (!graphicsOverlay.getGraphics().isEmpty()) {
                graphicsOverlay.getGraphics().clear();
            }
        }
    }

    @Override
    public void clearOverlay(SceneView sceneView) {
        var index = sceneView.getGraphicsOverlays().indexOf(graphicsOverlay);
        if (index != -1) {
            sceneView.getGraphicsOverlays().get(index).getGraphics().removeAll(graphicsOverlay.getGraphics());
        }
    }
}
