package Kvetka.Map.operations.zonesOfEnergy;

import Kvetka.Map.MapMarkerStyle;
import Kvetka.Map.MapOperations;
import Kvetka.Map.operations.RJStationsModel;
import MapModels.EDPRVModel;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.symbology.*;

import java.util.ArrayList;
import java.util.List;

public class CalculationAndDrawingOfZoneSpaceWave extends ZoneEnergy{
    /**
     * @param edprvModel
     * @return
     */

    static EDPRVModel model = new EDPRVModel();
    public static List<RJStationsModel> rpStations = new ArrayList<>();
    static LinearUnit meters = new LinearUnit(LinearUnitId.METERS);
    static AngularUnit ag = new AngularUnit(AngularUnitId.DEGREES);

/*    public static void getEDPRV(SceneView sceneView) {
        if (zoneSpaceWaveOverlay == null) {
            zoneSpaceWaveOverlay = new GraphicsOverlay();
            sceneView.getGraphicsOverlays().add(zoneSpaceWaveOverlay);
        }

        rpStations.clear();
        int index = sceneView.getGraphicsOverlays().indexOf(zoneSpaceWaveOverlay);
        sceneView.getGraphicsOverlays().get(index).getGraphics().clear();
    }*/

    @Override
    public void calculateParameters(EDPRVModel edprvModel) {
        //rpStations = new ArrayList<>();
        model = edprvModel;
        deleteOverlay();
        hmaxNb = edprvModel.getIonosphHeightMax();
        Dsv = edprvModel.getMinSvyazDist();
        Pn = edprvModel.getPeredatchPomechPower();
        Gn = 6;
        rassoglasKoef = 1;
        Psv = edprvModel.getPeredatchikPower();
        Gsv = 2;
        Kn = 1.2;

        double x = (Math.pow(6371, 2) + Math.pow(6371 + hmaxNb, 2) - Math.pow(0.5 * (Math.sqrt(Math.pow(6371, 2) * Math.pow(Math.sin(90 * Dsv / Math.PI / 6371), 2) + Math.pow(hmaxNb, 2)) * Math.sqrt(Pn * Gn * rassoglasKoef / Psv / Gsv / Kn)), 2)) / 6371 / 2 / (6371 + hmaxNb);
        Drp = Math.PI * 6371 * Math.acos(x) / 90;//250

        //настройки графики для отрисовки
        SimpleLineSymbol simpleLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFF00FF00, 3);
        SimpleFillSymbol simpleFillSymbolRp = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x00000000, simpleLineSymbol);
        SimpleFillSymbol simpleFillSymbolZone = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x88FF0000, null);
        SimpleLineSymbol simpleLineSymbolBlack = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFF000000, 3);

        //получить координаты комплекса из БД и отрисовать

        if (MapOperations.DetectedMyStation.isEmpty()) {
            latComplex = 53.901236;
            longComplex = 27.559818;
        } else {
            latComplex = MapOperations.DetectedMyStation.get(0).getCoordinates().latitude;
            longComplex = MapOperations.DetectedMyStation.get(0).getCoordinates().longitude;
        }
        //добавить дугу между двумя

        com.esri.arcgisruntime.geometry.Point circlePoint = new com.esri.arcgisruntime.geometry.Point(longComplex, latComplex, SpatialReferences.getWgs84());
        com.esri.arcgisruntime.geometry.Point mapPoint = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(circlePoint, SpatialReferences.getWebMercator());
        sectorGeometry = ZoneOperations.getSector(edprvModel.getSbrBisectrix(), edprvModel.getSbrAngle(), mapPoint, Drp);
        Graphic kompleksGraphic = new Graphic(sectorGeometry, simpleFillSymbolRp);
        graphicsOverlay.getGraphics().add(kompleksGraphic);

        draw();
    }

    @Override
    public void draw() {
        //настройки графики для отрисовки
        SimpleLineSymbol simpleLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFF00FF00, 3);
        SimpleFillSymbol simpleFillSymbolRp = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x00000000, simpleLineSymbol);
        SimpleFillSymbol simpleFillSymbolZone = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x88FF0000, null);

        //отрисовываем зону
        Geometry drpIntersection = ZoneOperations.CirclesIntersection(rpStations, Drp);

        var zone0CenterPointMercator = drpIntersection.getExtent().getCenter();
        var y = drpIntersection.getExtent().getCenter().getY();
        var x = drpIntersection.getExtent().getCenter().getX();
        //Point zone0CenterPointWgs84 = new Point(zone0CenterPointMercator.getX(), zone0CenterPointMercator.getY(), SpatialReferences.getWgs84());
        Point endCutLinePoint1 = GeometryEngine.moveGeodetic(zone0CenterPointMercator, 990000, meters, model.getSbrBisectrix() + 90, ag, GeodeticCurveType.GEODESIC);
        Point endCutLinePoint2 = GeometryEngine.moveGeodetic(zone0CenterPointMercator, 990000, meters, model.getSbrBisectrix() - 90, ag, GeodeticCurveType.GEODESIC);

        PointCollection cutLinePoints = new PointCollection(SpatialReferences.getWebMercator());
        cutLinePoints.add(endCutLinePoint1);
        cutLinePoints.add(endCutLinePoint2);
        // create a polyline geometry from the point collection
        Polyline cutLinePolyline = new Polyline(cutLinePoints);

        List<Geometry> cuttedZoneParts = GeometryEngine.cut(drpIntersection, cutLinePolyline);

        //найдём ближайшую к АСП часть зоны и её и отобразим
        double closestDist = 99999999;
        int zonePartIndexResult = -1;
        for (int i = 0; i < cuttedZoneParts.size(); i++) {
            Point centerPartZonePoint = ((Point) GeometryEngine.project(new Point(cuttedZoneParts.get(i).getExtent().getCenter().getX(), cuttedZoneParts.get(i).getExtent().getCenter().getY(), SpatialReferences.getWebMercator()), SpatialReferences.getWgs84()));

            double distCenterToASP = GeometryEngine.distanceGeodetic(centerPartZonePoint, new Point(longComplex, latComplex, SpatialReferences.getWgs84()), meters, ag, GeodeticCurveType.GEODESIC).getDistance();

            if (distCenterToASP < closestDist) {
                closestDist = distCenterToASP;
                zonePartIndexResult = i;
            }
        }

        //отрисовываем зону энергодоступности пространственой волны
        Geometry dsvUnion = ZoneOperations.CirclesUnion(rpStations, Dsv * 1000);
        Geometry drpUnionAndDsvUnionDifference = GeometryEngine.difference(cuttedZoneParts.get(zonePartIndexResult), dsvUnion);
        //Geometry drpUnionAndDsvUnionDifferenceEnd = GeometryEngine.intersection(drpUnionAndDsvUnionDifference, zoneRestricterPolygon);
        Graphic resultZone = new Graphic(drpUnionAndDsvUnionDifference, simpleFillSymbolZone);
        graphicsOverlay.getGraphics().add(resultZone);

        //нанесение символов границ для границы зон радиоподавления по противникам(черные вилки)
        for (int i = 0; i < rpStations.size(); i++) {
            ZoneOperations.AddCirclingImages(graphicsOverlay, new Point(rpStations.get(i).getLongitude(), rpStations.get(i).getLatitude(), SpatialReferences.getWgs84()), drpUnionAndDsvUnionDifference, MapMarkerStyle.rj, 0, 360, 10, Drp * 1000 - 15);
        }
        //нанесение символов границ для границы зоны радиоподавления с текущей позиции нашей асп(черные вилки)
        ZoneOperations.AddCirclingImages(graphicsOverlay, new Point(longComplex, latComplex, SpatialReferences.getWgs84()), sectorGeometry, MapMarkerStyle.rj, 0, 360, 10, Drp * 1000 - 2000);
        //нанесение символов границ для границы зон радиоразведки(связи) противника(черные крестики)
        for (int i = 0; i < rpStations.size(); i++) {
            ZoneOperations.AddCirclingImages(graphicsOverlay, new Point(rpStations.get(i).getLongitude(), rpStations.get(i).getLatitude(), SpatialReferences.getWgs84()), drpUnionAndDsvUnionDifference, MapMarkerStyle.x, 0, 360, 10, Dsv * 1000 + 15);
        }
        //добавляем данные по радиусу(дистанции) подавления и радиусу(дистанции) связи противника в таблицу и выводим ее
        //tableEDPRV.getItems().add(new Double[]{((double)Math.round(Drp*100))/100,((double)Math.round(Dsv*100))/100});
        //stageEdprv.show();//открывает таблицу по окончанию вычислений

    }
}
