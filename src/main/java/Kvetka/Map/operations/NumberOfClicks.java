package Kvetka.Map.operations;

public enum NumberOfClicks {
    FIRST(0),
    MIDDLE(1),
    LAST(2);

    public int getNumber() {
        return number;
    }

    NumberOfClicks(int number) {
        this.number = number;
    }

    private final int number;
}
