package Kvetka.Map.operations.situation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SituationProperties {
    public static ObjectMapper mapper;
    public static SituationOnMap situation = new SituationOnMap();

    public static SituationOnMap getSituation() {
        return situation;
    }

    public static void setSituation(SituationOnMap situation) throws IOException {
        SituationProperties.situation = situation;
        YamlSave(situation);
    }

    public static void YamlSave(SituationOnMap situation) throws IOException {
        mapper = new ObjectMapper(new YAMLFactory());
        mapper.writeValue(new File("SituationOnMap.yaml"), getSituation());
    }

    public static SituationOnMap YamlLoad() throws IOException {
        File file = new File("SituationOnMap.yaml");

        if (file.exists()) {
            setSituation(SituationOnMapLoad());
        } else {
            setSituation(DefaultSituationOnMap());
            YamlSave(getSituation());
        }
        return getSituation();
    }

    public static SituationOnMap SituationOnMapLoad() throws IOException {
        mapper = new ObjectMapper(new YAMLFactory());
        SituationOnMap situation = mapper.readValue(new File("SituationOnMap.yaml"), SituationOnMap.class);

        return situation;
    }

    public static SituationOnMap DefaultSituationOnMap(){

        List<StationModel> listOwn = new ArrayList<>();
        StationsOwn stationsOwn = new StationsOwn(listOwn);

        List<StationModel> listEnemy = new ArrayList<>();
        StationsEnemy stationsEnemy = new StationsEnemy(listEnemy);

        return new SituationOnMap(stationsOwn, stationsEnemy);
    }
}
