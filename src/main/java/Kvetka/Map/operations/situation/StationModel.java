package Kvetka.Map.operations.situation;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class StationModel {

    public StationModel() {
    }


    public StationModel(String code, Double longitude, Double latitude, String signature) {
        this.code = code;
        this.longitude = longitude;
        this.latitude = latitude;
        this.signature = signature;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    String code = new String();
    Double longitude;
    Double latitude;
    String signature = new String();

}
