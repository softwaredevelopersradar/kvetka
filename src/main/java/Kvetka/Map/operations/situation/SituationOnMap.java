package Kvetka.Map.operations.situation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;

public class SituationOnMap {
    public SituationOnMap(StationsOwn stationsOwn, StationsEnemy stationsEnemy) {
        this.stationsOwn = stationsOwn;
        this.stationsEnemy = stationsEnemy;
    }

    public SituationOnMap() {
    }

    public StationsOwn getStationsOwn() {
        return stationsOwn;
    }

    public void setStationsOwn(StationsOwn stationsOwn) {
        this.stationsOwn = stationsOwn;
    }

    public StationsEnemy getStationsEnemy() {
        return stationsEnemy;
    }

    public void setStationsEnemy(StationsEnemy stationsEnemy) {
        this.stationsEnemy = stationsEnemy;
    }

    StationsOwn stationsOwn = new StationsOwn();
    StationsEnemy stationsEnemy = new StationsEnemy();
}
