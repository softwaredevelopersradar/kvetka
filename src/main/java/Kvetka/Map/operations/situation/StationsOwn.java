package Kvetka.Map.operations.situation;

import java.util.ArrayList;
import java.util.List;

public class StationsOwn {


    public StationsOwn() {
    }

    public List<StationModel> getStations() {
        return stations;
    }

    public void setStations(List<StationModel> stations) {
        this.stations = stations;
    }

    public StationsOwn(List<StationModel> stations) {
        this.stations = stations;
    }

    List<StationModel> stations = new ArrayList<>();
}
