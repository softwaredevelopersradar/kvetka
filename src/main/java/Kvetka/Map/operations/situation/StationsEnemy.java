package Kvetka.Map.operations.situation;

import java.util.ArrayList;
import java.util.List;

public class StationsEnemy {



    public StationsEnemy() {
    }

    public StationsEnemy(List<StationModel> stations) {
        this.stations = stations;
    }


    public List<StationModel> getStations() {
        return stations;
    }

    public void setStations(List<StationModel> stations) {
        this.stations = stations;
    }

    List<StationModel> stations = new ArrayList<>();

}
