package Kvetka.Map.operations;

import Kvetka.Map.models.ComplexCompositionEnemyModel;
import Kvetka.Map.models.ComplexCompositionOwnModel;
import Kvetka.Map.MapOperations;
import Kvetka.Map.operations.zonesOfEnergy.CalculationAndDrawingOfZoneSpaceWave;
import Kvetka.Map.operations.zonesOfEnergy.CalculationAndDrawingOfZoneSurfaceWave;
import Kvetka.Map.operations.zonesOfEnergy.ZoneOperations;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SelectGraphics {
    private static RJStationsModel rjStationsModel = new RJStationsModel();
    public static int counterRPEdprv=0;

    private static RJStationsModel stationForSurface = new RJStationsModel();
    public static int counterRPEdpov = 0;

    public static void selectForSpace(List<IdentifyGraphicsOverlayResult> results){
        List<List<Graphic>> graphics = new ArrayList<>();

        try {
            if (results != null) {
                for (int g = 0; g < results.size(); g++) {
                    graphics.add(results.get(g).getGraphics());
                }

                for (var gr :
                        graphics) {

                    for(Iterator<ComplexCompositionOwnModel> iterator = MapOperations.ListComplexCompositionOwn.iterator(); iterator.hasNext();){
                        ComplexCompositionOwnModel c = iterator.next();
                        for (int i = 0; i < gr.size(); i++) {
                            if (c.Graphic.equals(gr.get(i))) {
                                rjStationsModel = new RJStationsModel();
                                rjStationsModel.setLongitude(c.Longitude);
                                rjStationsModel.setLatitude(c.Latitude);

                                CalculationAndDrawingOfZoneSpaceWave.rpStations.add(rjStationsModel);

                                counterRPEdprv++;
                                if(CalculationAndDrawingOfZoneSpaceWave.rpStations.size()>=2){
                                    ZoneOperations.DrawBond(counterRPEdprv,CalculationAndDrawingOfZoneSpaceWave.rpStations,CalculationAndDrawingOfZoneSpaceWave.graphicsOverlay);
                                }
                            }
                        }
                    }

                    for(Iterator<ComplexCompositionEnemyModel> iterator = MapOperations.ListComplexCompositionEnemy.iterator(); iterator.hasNext();){
                        ComplexCompositionEnemyModel c = iterator.next();
                        for (int i = 0; i < gr.size(); i++) {
                            if (c.Graphic.equals(gr.get(i))) {
                                rjStationsModel = new RJStationsModel();
                                rjStationsModel.setLongitude(c.Longitude);
                                rjStationsModel.setLatitude(c.Latitude);

                                CalculationAndDrawingOfZoneSpaceWave.rpStations.add(rjStationsModel);

                                counterRPEdprv++;
                                if(CalculationAndDrawingOfZoneSpaceWave.rpStations.size()>=2){
                                    ZoneOperations.DrawBond(counterRPEdprv,CalculationAndDrawingOfZoneSpaceWave.rpStations,CalculationAndDrawingOfZoneSpaceWave.graphicsOverlay);
                                }
                            }
                        }
                    }
                }
            }
            else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void selectForSurface(List<IdentifyGraphicsOverlayResult> results){
        List<List<Graphic>> graphics = new ArrayList<>();

        try {
            if (results != null) {
                for (int g = 0; g < results.size(); g++) {
                    graphics.add(results.get(g).getGraphics());
                }

                for (var gr :
                        graphics) {

                    for(Iterator<ComplexCompositionOwnModel> iterator = MapOperations.ListComplexCompositionOwn.iterator(); iterator.hasNext();){
                        ComplexCompositionOwnModel c = iterator.next();
                        for (int i = 0; i < gr.size(); i++) {
                            if (c.Graphic.equals(gr.get(i))) {
                                stationForSurface = new RJStationsModel();
                                stationForSurface.setLongitude(c.Longitude);
                                stationForSurface.setLatitude(c.Latitude);

                                CalculationAndDrawingOfZoneSurfaceWave.stationsModels.add(stationForSurface);

                                counterRPEdpov++;
                                if(CalculationAndDrawingOfZoneSpaceWave.rpStations.size()>=2){
                                    ZoneOperations.DrawBond(counterRPEdpov,CalculationAndDrawingOfZoneSpaceWave.rpStations,CalculationAndDrawingOfZoneSpaceWave.graphicsOverlay);
                                }
                            }
                        }
                    }

                    for(Iterator<ComplexCompositionEnemyModel> iterator = MapOperations.ListComplexCompositionEnemy.iterator(); iterator.hasNext();){
                        ComplexCompositionEnemyModel c = iterator.next();
                        for (int i = 0; i < gr.size(); i++) {
                            if (c.Graphic.equals(gr.get(i))) {
                                stationForSurface = new RJStationsModel();
                                stationForSurface.setLongitude(c.Longitude);
                                stationForSurface.setLatitude(c.Latitude);

                                CalculationAndDrawingOfZoneSurfaceWave.stationsModels.add(stationForSurface);

                                counterRPEdpov++;
                                if(CalculationAndDrawingOfZoneSpaceWave.rpStations.size()>=2){
                                    ZoneOperations.DrawBond(counterRPEdpov,CalculationAndDrawingOfZoneSpaceWave.rpStations,CalculationAndDrawingOfZoneSpaceWave.graphicsOverlay);
                                }
                            }
                        }
                    }
                }
            }
            else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
