package Kvetka.Map.operations;

import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;
import com.esri.arcgisruntime.mapping.view.SceneView;
import javafx.geometry.Point2D;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class IdentifyGraphic {

    public static ListenableFuture<List<IdentifyGraphicsOverlayResult>> identify(Point2D point2D, SceneView sceneView) {
        final ListenableFuture<List<IdentifyGraphicsOverlayResult>> identifyGraphics = sceneView.identifyGraphicsOverlaysAsync(point2D,
                20, false);

        return identifyGraphics;
    }
}
