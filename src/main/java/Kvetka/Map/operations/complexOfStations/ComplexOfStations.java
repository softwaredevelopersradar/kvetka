package Kvetka.Map.operations.complexOfStations;

import Kvetka.Map.models.MapStationsModel;
import KvetkaModels.StationsModel;
import StationModels.ListOfMyPictures;
import StationModels.Picture;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import javafx.collections.ObservableList;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComplexOfStations {
    public Map<GraphicsOverlay, Graphic> complex = new HashMap<>();

    public void setComplex(List<StationsModel> listStationModel, SceneView sceneView){
        ObservableList<Picture> listOfImages = ListOfMyPictures.getListOfPictures();
        ImageView imageView = new ImageView();

        for (var p : listOfImages) {

            if (p.getCode().equals("SP_MY")) {

                imageView.setImage(p.getImage());
            }
        }

        if (!complex.isEmpty()) {
            for (Map.Entry m : complex.entrySet()) {
                var ind = sceneView.getGraphicsOverlays().indexOf((GraphicsOverlay) m.getKey());
                if(ind != -1){
                    sceneView.getGraphicsOverlays().remove((GraphicsOverlay) m.getKey());
                }
            }
        }

        // Image ---------------------------------------------------------------------------------

        complex = new HashMap<>();
        for (int i = 0; i < listStationModel.stream().count(); i++) {

            PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(imageView.getImage());

            markerSymbol.setHeight(20); //высоту и ширину можно добавить в качестве парметров
            markerSymbol.setWidth(20);

            com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                    listStationModel.get(i).getCoordinates().longitude,
                    listStationModel.get(i).getCoordinates().latitude,
                    SpatialReferences.getWgs84());

            Graphic graphic = new Graphic(symbolPoint, markerSymbol);
            GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
            graphicsOverlay.getGraphics().add(graphic);

            complex.put(graphicsOverlay, graphic);
        }
    }

    public void removeComplex(SceneView sceneView){
        if (!complex.isEmpty()) {
            for (Map.Entry m : complex.entrySet()) {
                var ind = sceneView.getGraphicsOverlays().indexOf((GraphicsOverlay) m.getKey());
                if(ind != -1){
                    sceneView.getGraphicsOverlays().remove((GraphicsOverlay) m.getKey());
                }
            }
        }
    }
    public void showGraphics(SceneView sceneView){
        if (!complex.isEmpty()) {
            for (Map.Entry m : complex.entrySet()) {
                sceneView.getGraphicsOverlays().add((GraphicsOverlay) m.getKey());
            }
        }
    }

    public void hideGraphics(SceneView sceneView){
        if (!complex.isEmpty()) {
            for (Map.Entry m : complex.entrySet()) {
                sceneView.getGraphicsOverlays().remove((GraphicsOverlay) m.getKey());
            }
        }
    }
}
