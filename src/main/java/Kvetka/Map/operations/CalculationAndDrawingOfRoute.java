package Kvetka.Map.operations;

import Kvetka.Map.NumberFormat;
import Kvetka.Map.models.ListRoutesModel;
import KvetkaModels.Coord;
import KvetkaModels.RoutePointModel;
import MapModels.RouteModel;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.arcgisservices.LabelingPlacement;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.labeling.ArcadeLabelExpression;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.ColorUtil;
import com.esri.arcgisruntime.symbology.TextSymbol;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.*;

import static Kvetka.Map.MapMarkerStyle.*;

public class CalculationAndDrawingOfRoute {
    static GraphicsOverlay graphicsOverlay;
    public static KvetkaModels.RouteModel model;

    static ArrayList<Graphic> rulerGraphics = new ArrayList<>();

    static double dist = 0;
    static double segment = 0;
    static double latituderulerOld = 0;
    static double longituderulerOld = 0;
    static int countPoints = 0;
    static short countRoutes = 0;
    static PointCollection polylinerulerPoints = new PointCollection(SpatialReferences.getWgs84());
    static LinearUnit unit = new LinearUnit(LinearUnitId.METERS);
    //static List<ListRoutesModel> listRoutes = new ArrayList<>();
    static Map<Short, GraphicsOverlay> listRoutes = new HashMap<>();  //Список объектов graphicOverlay
    static List<RoutePointModel> listPoints = new ArrayList<>(); //Формирование списка точек каждого маршрута
    public static List<Short> routeList = new ArrayList<>();  //Нумерация маршрутов
    private static List<KvetkaModels.RouteModel> routesDB = new ArrayList<>();  //Заполняется при подключении к БД

    /**
     * Построение маршрута
     *
     * @param routeModel
     * @param click
     * @return
     */
    public static GraphicsOverlay drawingRoute(@NotNull RouteModel routeModel, @NotNull NumberOfClicks click) {

        double latitude = DoubleRounder.round(routeModel.getLatitude(), 6);
        double longitude = DoubleRounder.round(routeModel.getLongitude(), 6);
        /*graphicsOverlay = new GraphicsOverlay();
        graphicsOverlay.setLabelsEnabled(true);*/

        polylinerulerPoints = new PointCollection(SpatialReferences.getWgs84());

        switch (click) {
            case FIRST -> {
                graphicsOverlay = new GraphicsOverlay();
                graphicsOverlay.setLabelsEnabled(true);
                //listRoutes.add(new ListRoutesModel((int) countRoutes, graphicsOverlay));
                //listRoutes.put(countRoutes, graphicsOverlay);

                //countRoutes++;
                latituderulerOld = 0;
                longituderulerOld = 0;
                countPoints = 1;
                dist = 0;
                listPoints.clear();
                GenericFirstNumberRoute(routeList);

                model = new KvetkaModels.RouteModel();
                model.setDistance(0);
                listPoints.add(new RoutePointModel(0, 0, (short) countPoints, new Coord(latitude, longitude, 100), dist));
                //model.setListPoints(listPoints);
                model.setNumberRoute(countRoutes);
                model.setName("Маршрут №" + String.valueOf(countRoutes));
                model.setId(0);
                model.setTime(date());

                routeList.add(countRoutes);

                Point startrulerPoint = new Point(longitude, latitude, SpatialReferences.getWgs84());
                Graphic startPointrulerGraphic = new Graphic(startrulerPoint, markerStartPoint);
                graphicsOverlay.getGraphics().add(startPointrulerGraphic);

                startPointrulerGraphic.getAttributes().put("NAME", NumberFormat.startFormatter.format(dist));
                startPointrulerGraphic.getAttributes().put("DESCRIPTION", String.valueOf(countPoints));
                LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                graphicsOverlay.getLabelDefinitions().add(labelDefinition);
            }
            case MIDDLE -> {
                countPoints++;

                Point midrulerPoint = new Point(longitude, latitude, SpatialReferences.getWgs84());
                Graphic midPointrulerGraphic = new Graphic(midrulerPoint, markerStartPoint);

                graphicsOverlay.getGraphics().add(midPointrulerGraphic);

                rulerGraphics.add(midPointrulerGraphic);

                polylinerulerPoints.add(new Point(longituderulerOld, latituderulerOld));
                polylinerulerPoints.add(new Point(longitude, latitude));

                Polyline polyline = new Polyline(polylinerulerPoints);
                Graphic line = new Graphic(polyline, polylineSymbol);

                graphicsOverlay.getGraphics().add(line);

                Geometry path = GeometryEngine.densifyGeodetic(polyline, 10, unit, GeodeticCurveType.GEODESIC);
                line.setGeometry(path);
                segment = GeometryEngine.lengthGeodetic(path, unit, GeodeticCurveType.GEODESIC);
                dist = dist + segment;

                midPointrulerGraphic.getAttributes().put("NAME", NumberFormat.formatter.format(dist));
                midPointrulerGraphic.getAttributes().put("DESCRIPTION", String.valueOf(countPoints));
                LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                graphicsOverlay.getLabelDefinitions().add(labelDefinition);

                //model = new KvetkaModels.RouteModel();
                //model.setDistance(dist);
                listPoints.add(new RoutePointModel(0, 0, (short) countPoints, new Coord(latitude, longitude, 100), segment));
                //model.setListPoints(listPoints);
                //model.setNumberRoute((short) countRoutes);
                //model.setName("Маршрут №" + String.valueOf(countRoutes));
                //model.setId(0);
                //model.setTime(date());

            }
            case LAST -> {
                countPoints++;

                Point endrulerPoint = new Point(longitude, latitude, SpatialReferences.getWgs84());
                Graphic endPointrulerGraphic = new Graphic(endrulerPoint, markerStopPoint);

                graphicsOverlay.getGraphics().add(endPointrulerGraphic);
                //graphicsOverlay.setLabelsEnabled(true);
                rulerGraphics.add(endPointrulerGraphic);

                polylinerulerPoints.add(new Point(longituderulerOld, latituderulerOld));
                polylinerulerPoints.add(new Point(longitude, latitude));

                Polyline polyline = new Polyline(polylinerulerPoints);
                Graphic line = new Graphic(polyline, polylineSymbol);
                graphicsOverlay.getGraphics().add(line);

                Geometry path = GeometryEngine.densifyGeodetic(polyline, 10, unit, GeodeticCurveType.GEODESIC);
                line.setGeometry(path);
                segment = GeometryEngine.lengthGeodetic(path, unit, GeodeticCurveType.GEODESIC);
                dist = dist + segment;

                endPointrulerGraphic.getAttributes().put("NAME", NumberFormat.formatter.format(dist));
                endPointrulerGraphic.getAttributes().put("DESCRIPTION", String.valueOf(countPoints));
                LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                graphicsOverlay.getLabelDefinitions().add(labelDefinition);

                //model = new KvetkaModels.RouteModel();
                model.setDistance(dist);
                listPoints.add(new RoutePointModel(0, 0, (short) countPoints, new Coord(latitude, longitude, 100), segment));
                model.setListPoints(listPoints);
                //model.setNumberRoute((short) countRoutes);
                //model.setName("Маршрут №" + String.valueOf(countRoutes));
                //model.setId(0);
                model.setTime(date());

                dist = 0;
            }
            default -> {
            }
        }

        latituderulerOld = latitude;
        longituderulerOld = longitude;

        return graphicsOverlay;
    }

    private static @NotNull LabelDefinition makeLabelDefinition(String party, String description, Color color) {
        // create text symbol for styling the label
        var textSymbol = new TextSymbol();
        textSymbol.setSize(12);
        textSymbol.setColor(ColorUtil.colorToArgb(color));
        textSymbol.setHaloColor(ColorUtil.colorToArgb(Color.WHITE));
        textSymbol.setHaloWidth(2);

        // create a label definition with an Arcade expression script
        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME+ \" (\" + left($feature.DESCRIPTION,1) + \") \"");

/*        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME + \" (\" + left($feature.PARTY,1) + \")\\nDistrict \" + $feature.CDFIPS");  //*/
        var labelDefinition = new LabelDefinition(arcadeLabelExpression, textSymbol);

        labelDefinition.setPlacement(LabelingPlacement.POINT_ABOVE_CENTER);
        labelDefinition.setWhereClause("");  //String.format("PARTY = '%s'", party)

        return labelDefinition;
    }

    /**
     * Обновление маршрутов в таблице -> обновление/удаление
     *
     * @param routes
     * @param sceneView
     */
    public static void UpdateRoutes(@NotNull List<KvetkaModels.RouteModel> routes, SceneView sceneView) {

        if (routes.size() == 0) {
            for (Map.Entry m : listRoutes.entrySet()) {
                var index = sceneView.getGraphicsOverlays().indexOf(m.getValue());
                sceneView.getGraphicsOverlays().remove(index);
            }
            listRoutes.clear();
            countRoutes = 0;
        } else {
            if (routes.size() < routesDB.size()) {  //Deleted

                Map<Short, GraphicsOverlay> temporary = new HashMap<>();
                temporary = Map.copyOf(listRoutes);

                try {
                    for (var r : routes) {
                        if (temporary.containsKey((short) r.getId())) {
                            temporary.remove((short) r.getId());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Map.Entry m : listRoutes.entrySet()) {
                    for (Map.Entry mt : temporary.entrySet()) {
                        if (m.getKey().equals(mt.getKey())) {
                            var index = sceneView.getGraphicsOverlays().indexOf(mt.getValue());
                            sceneView.getGraphicsOverlays().remove(index);
                            listRoutes.remove(mt.getKey());
                        }
                    }
                }
            } else {

                for (Iterator<KvetkaModels.RouteModel> iterator = routes.iterator(); iterator.hasNext(); ) {
                    KvetkaModels.RouteModel rt = iterator.next();

                    if (!listRoutes.containsKey((short) rt.getId())) {
                        listRoutes.put((short) rt.getId(), graphicsOverlay);
                    }
                }
            }
        }

        routesDB = routes;
    }

    /**
     * Получение маршрутов из базы пр первом подключении к ней
     *
     * @param routes
     * @param sceneView
     * @return
     */
    public static @NotNull GraphicsOverlay DrawRoutesFromDB(List<KvetkaModels.RouteModel> routes, SceneView sceneView) {
        var graphicsOverlay = new GraphicsOverlay();
        graphicsOverlay.setLabelsEnabled(true);

        routeList.clear();
        routesDB = List.copyOf(routes);

        double prevLongitude = 0.0;
        double prevLatitude = 0.0;

        for (Iterator<KvetkaModels.RouteModel> iterator = routes.iterator(); iterator.hasNext(); ) {
            KvetkaModels.RouteModel route = iterator.next();

            routeList.add(route.getNumberRoute());

            graphicsOverlay = new GraphicsOverlay();
            graphicsOverlay.setLabelsEnabled(true);

            for (Iterator<RoutePointModel> iterator1 = route.getListPoints().iterator(); iterator1.hasNext(); ) {
                RoutePointModel point = iterator1.next();

                if (point != route.getListPoints().get(route.getListPoints().size() - 1)) {

                    Point startPoint = new Point(point.getCoordinates().longitude, point.getCoordinates().latitude, SpatialReferences.getWgs84());
                    Graphic startGraphic = new Graphic(startPoint, markerStartPoint);
                    graphicsOverlay.getGraphics().add(startGraphic);

                    if (prevLongitude != 0) {

                        var polylinePoints = new PointCollection(SpatialReferences.getWgs84());
                        polylinePoints.add(new Point(prevLongitude, prevLatitude));
                        polylinePoints.add(new Point(point.getCoordinates().longitude, point.getCoordinates().latitude));

                        Polyline polyline = new Polyline(polylinePoints);
                        Graphic line = new Graphic(polyline, polylineSymbol);
                        graphicsOverlay.getGraphics().add(line);

                        startGraphic.getAttributes().put("NAME", NumberFormat.formatter.format(point.getSegmentLength()));
                    } else {
                        startGraphic.getAttributes().put("NAME", NumberFormat.startFormatter.format(point.getSegmentLength()));
                    }
                    prevLongitude = point.getCoordinates().longitude;
                    prevLatitude = point.getCoordinates().latitude;


                    startGraphic.getAttributes().put("DESCRIPTION", String.valueOf(point.getNumberPoint()));
                    LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                    graphicsOverlay.getLabelDefinitions().add(labelDefinition);
                } else {
                    Point endPoint = new Point(point.getCoordinates().longitude, point.getCoordinates().latitude, SpatialReferences.getWgs84());
                    Graphic endGraphic = new Graphic(endPoint, markerStopPoint);
                    graphicsOverlay.getGraphics().add(endGraphic);

                    var polylinePoints = new PointCollection(SpatialReferences.getWgs84());
                    polylinePoints.add(new Point(prevLongitude, prevLatitude));
                    polylinePoints.add(new Point(point.getCoordinates().longitude, point.getCoordinates().latitude));

                    Polyline polyline = new Polyline(polylinePoints);
                    Graphic line = new Graphic(polyline, polylineSymbol);
                    graphicsOverlay.getGraphics().add(line);

                    endGraphic.getAttributes().put("NAME", NumberFormat.formatter.format(route.getDistance()));
                    endGraphic.getAttributes().put("DESCRIPTION", String.valueOf(point.getNumberPoint()));
                    LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
                    graphicsOverlay.getLabelDefinitions().add(labelDefinition);
                    sceneView.getGraphicsOverlays().add(graphicsOverlay);

                    prevLongitude = 0.0;
                    prevLatitude = 0.0;
                }
            }
            listRoutes.put((short) route.getId(), graphicsOverlay);
        }

        GenericFirstNumberRoute(routeList);

        return graphicsOverlay;
    }

    private static void GenericFirstNumberRoute(@NotNull List<Short> values) {

        if (values.size() == 0) {
            countRoutes = 1;
        } else {

            Collections.sort(values);
            short index = 0;
            short first = values.get(index);

            if (first != 1) {
                countRoutes = 1;
            } else {

                for (short i = 0; i < values.size(); i++) {

                    var difference = values.get(i) - first;
                    if (difference == 1) {
                        first = values.get(i);
                        countRoutes = (short) (first + 1);
                    } else {
                        countRoutes++;
                        break;
                    }
                }
            }
        }
    }

    private static @NotNull Date date() {
        Date date = new Date();
        date.getTime();
        return date;
    }
}
