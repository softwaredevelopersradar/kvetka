package Kvetka.Map.operations;

import Kvetka.Map.models.ComplexCompositionEnemyModel;
import Kvetka.Map.models.ComplexCompositionOwnModel;
import Kvetka.Map.MapOperations;
import Kvetka.Map.operations.situation.StationModel;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;
import com.esri.arcgisruntime.mapping.view.SceneView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RemoveGraphics {
    public static void remove(List<IdentifyGraphicsOverlayResult> results, SceneView sceneView){
        List<List<Graphic>> graphics = new ArrayList<>();

        try {
            if (results != null) {
                for (int g = 0; g < results.size(); g++) {
                    graphics.add(results.get(g).getGraphics());
                }

                for (var gr :
                        graphics) {

                    for(Iterator<ComplexCompositionOwnModel> iterator = MapOperations.ListComplexCompositionOwn.iterator(); iterator.hasNext();){
                        ComplexCompositionOwnModel c = iterator.next();
                        for (int i = 0; i < gr.size(); i++) {
                            if (c.Graphic.equals(gr.get(i))) {
                                var index = sceneView.getGraphicsOverlays().indexOf(c.GraphicsOverlay);

                                for(Iterator<StationModel> iterator1 = MapOperations.stationsOwnModels.iterator(); iterator1.hasNext();){
                                    StationModel s = iterator1.next();

                                    if(Double.compare(c.Longitude, s.getLongitude()) == 0 & Double.compare(c.Latitude, s.getLatitude()) == 0){
                                        iterator1.remove();
                                        MapOperations.saveSituation();
                                    }
                                }

                                sceneView.getGraphicsOverlays().get(index).getGraphics().remove(gr.get(i));
                                iterator.remove();
                                System.out.println("Delete");
                            }
                        }
                    }

                    for(Iterator<ComplexCompositionEnemyModel> iterator = MapOperations.ListComplexCompositionEnemy.iterator(); iterator.hasNext();){
                        ComplexCompositionEnemyModel c = iterator.next();
                        for (int i = 0; i < gr.size(); i++) {
                            if (c.Graphic.equals(gr.get(i))) {
                                var index = sceneView.getGraphicsOverlays().indexOf(c.GraphicsOverlay);

                                for(Iterator<StationModel> iterator1 = MapOperations.stationEnemyModels.iterator(); iterator1.hasNext();){
                                    StationModel s = iterator1.next();

                                    if(Double.compare(c.Longitude, s.getLongitude()) == 0 & Double.compare(c.Latitude, s.getLatitude()) == 0){
                                        iterator1.remove();
                                        MapOperations.saveSituation();
                                    }
                                }

                                sceneView.getGraphicsOverlays().get(index).getGraphics().remove(gr.get(i));
                                iterator.remove();
                                System.out.println("Delete");
                            }
                        }
                    }
                }
            }
             else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
