package Kvetka.Map.operations.azimuth;

import Kvetka.Map.MapMarkerStyle;
import Kvetka.Map.NumberFormat;
import MapModels.AzimuthModel;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.arcgisservices.LabelingPlacement;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.labeling.ArcadeLabelExpression;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.ColorUtil;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.symbology.TextSymbol;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class CalculationAndDrawingOfAzimuth {
    static GraphicsOverlay azimuthOverlay;
    static Graphic azimuthGraphic = new Graphic();
    static Graphic startPointAzimuthGraphic = new Graphic();
    static Graphic endPointAzimuthGraphic = new Graphic();

    static Graphic graphicP = new Graphic();
    static Graphic grLine = new Graphic();

    static LinearUnit linearUnit = new LinearUnit(LinearUnitId.METERS);
    static AngularUnit angularUnit = new AngularUnit(AngularUnitId.DEGREES);

    public double getAzimuthRez() {
        return azimuthRez;
    }

    public void setAzimuthRez(double azimuthRez) {
        this.azimuthRez = azimuthRez;
    }

    double azimuthRez;

    private static double firstLon = 0;
    private static double firstLat = 0;
    private static double endLon = 0;
    private static double endLat = 0;


    /**
     * Азимут
     *
     * @param azimuthModel
     * @return
     */
    public static GraphicsOverlay CalculateAzimuth(AzimuthModel azimuthModel) {

        double Lat1 = azimuthModel.getLatitude1();
        double Lat2 = azimuthModel.getLatitude2();
        double Lon1 = azimuthModel.getLongitude1();
        double Lon2 = azimuthModel.getLongitude2();

        //удаляем отрисованный раньше азимут, если есть такой
        if (azimuthOverlay != null) {
            azimuthOverlay.getGraphics().clear();
        }

        //как вариант можно воспользоваться встроенным функционалом arcgis runtime sdk https://community.esri.com/t5/arcgis-runtime-sdk-for-net-questions/get-azimuth-value-from-point-a-to-point-b/td-p/644506
        double azimuth = 0;
        double tn = Math.abs(Lon2 - Lon1) / Math.abs(Lat2 - Lat1);
        double arctan = Math.toDegrees(Math.atan(tn));

        if ((Lon2 > Lon1) && (Lat2 > Lat1)) {
            azimuth = 90 - arctan;
        } else if ((Lon2 > Lon1) && (Lat2 < Lat1)) {
            azimuth = 180 - arctan;
        } else if ((Lon2 < Lon1) && (Lat2 < Lat1)) {
            azimuth = 180 + arctan;
        } else if ((Lon2 < Lon1) && (Lat2 > Lat1)) {
            azimuth = 270 + arctan;
        } else {
            azimuth = -1;//две точки совпадают
        }

        //Графика азимута

        azimuthOverlay = new GraphicsOverlay();

        //отрисовка стартовой и конечной точки

        MapMarkerStyle.simpleMarkerSymbol.setOutline(MapMarkerStyle.blueOutlineSymbol);

        //1ая точка
        // create a point geometry with a location and spatial reference
        Point startAzimuthPoint = new Point(Lon1, Lat1, SpatialReferences.getWgs84());
        startPointAzimuthGraphic = new Graphic(startAzimuthPoint, MapMarkerStyle.simpleMarkerSymbol);
        // add the point graphic to the graphics overlay
        azimuthOverlay.getGraphics().add(startPointAzimuthGraphic);

        //2 точка
        // create a point geometry with a location and spatial reference
        Point endAzimuthPoint = new Point(Lon2, Lat2, SpatialReferences.getWgs84());
        endPointAzimuthGraphic = new Graphic(endAzimuthPoint, MapMarkerStyle.simpleMarkerSymbol);
        // add the point graphic to the graphics overlay
        azimuthOverlay.getGraphics().add(endPointAzimuthGraphic);

        GeodeticDistanceResult distance = GeometryEngine.distanceGeodetic(startAzimuthPoint, endAzimuthPoint, linearUnit, angularUnit, GeodeticCurveType.GEODESIC);
        //azimuthRez = distance.getAzimuth1();

        //отрисовка азимута
        // create a point collection with a spatial reference, and add three points to it
        PointCollection polylinePoints = new PointCollection(SpatialReferences.getWgs84());
        polylinePoints.add(new Point(Lon1, Lat1));
        polylinePoints.add(new Point(Lon2, Lat2));
        // create a polyline geometry from the point collection
        Polyline polyline = new Polyline(polylinePoints);

        // create a polyline graphic with the polyline geometry and symbol
        azimuthGraphic = new Graphic(polyline, MapMarkerStyle.polylineSymbol);

        azimuthOverlay.getGraphics().add(azimuthGraphic);

        return azimuthOverlay;
    }

    public void setFirstPoint(AzimuthModel azimuthModel, SceneView sceneView) {
        if (azimuthOverlay != null) {
            azimuthOverlay.getGraphics().clear();
        } else {
            azimuthOverlay = new GraphicsOverlay();
            sceneView.getGraphicsOverlays().add(azimuthOverlay);
        }
        azimuthOverlay.setLabelsEnabled(true);

        Point firstP = new Point(azimuthModel.getLongitude1(), azimuthModel.getLatitude1(), SpatialReferences.getWgs84());
        SimpleMarkerSymbol redCircleSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0xFF29a98b, 10);
        Graphic graphic = new Graphic(firstP, redCircleSymbol);
        azimuthOverlay.getGraphics().add(graphic);

        graphic.getAttributes().put("NAME", "1");
        LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
        azimuthOverlay.getLabelDefinitions().add(labelDefinition);

        firstLon = azimuthModel.getLongitude1();
        firstLat = azimuthModel.getLatitude1();
    }

    public void setSecondPoint(AzimuthModel azimuthModel, SceneView sceneView) {
        if (graphicP != null) {
            var index = azimuthOverlay.getGraphics().indexOf(graphicP);
            if (index != -1) {
                azimuthOverlay.getGraphics().remove(index);
            }
        }
        Point secondP = new Point(azimuthModel.getLongitude2(), azimuthModel.getLatitude2(), SpatialReferences.getWgs84());

        SimpleMarkerSymbol redCircleSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0xFF29a98b, 10);
        graphicP = new Graphic(secondP, redCircleSymbol);
        azimuthOverlay.getGraphics().add(graphicP);

        graphicP.getAttributes().put("NAME", "2");
        LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
        azimuthOverlay.getLabelDefinitions().add(labelDefinition);
    }

    public void updateLine(double lon, double lat, SceneView sceneView) {
        if (grLine != null) {
            var index = azimuthOverlay.getGraphics().indexOf(grLine);
            if (index != -1) {
                azimuthOverlay.getGraphics().remove(index);
            }
        }
        PointCollection collection = new PointCollection(SpatialReferences.getWgs84());
        collection.add(firstLon, firstLat);
        collection.add(new Point(lon, lat));

        GeodeticDistanceResult distance = GeometryEngine.distanceGeodetic(collection.get(0), collection.get(1), linearUnit, angularUnit, GeodeticCurveType.GEODESIC);
        if (distance.getAzimuth1() > 0) {
            azimuthRez = distance.getAzimuth1();
        } else {
            azimuthRez = 360 + distance.getAzimuth1();
        }


        Polyline line = new Polyline(collection);

        grLine = new Graphic(line, MapMarkerStyle.polylineSymbol);

        azimuthOverlay.getGraphics().add(grLine);
    }

    public void clearAzimuth(SceneView sceneView) {
        var index = sceneView.getGraphicsOverlays().indexOf(azimuthOverlay);
        if (index != -1) {
            sceneView.getGraphicsOverlays().get(index).getGraphics().removeAll(azimuthOverlay.getGraphics());
            //sceneView.getGraphicsOverlays().remove(azimuthOverlay);
        }
    }

    private static @NotNull LabelDefinition makeLabelDefinition(String party, String description, Color color) {
        // create text symbol for styling the label
        var textSymbol = new TextSymbol();
        textSymbol.setSize(12);
        textSymbol.setColor(ColorUtil.colorToArgb(color));
        textSymbol.setHaloColor(ColorUtil.colorToArgb(Color.WHITE));
        textSymbol.setHaloWidth(2);

        // create a label definition with an Arcade expression script
        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME");

/*        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME + \" (\" + left($feature.PARTY,1) + \")\\nDistrict \" + $feature.CDFIPS");  //*/
        var labelDefinition = new LabelDefinition(arcadeLabelExpression, textSymbol);

        labelDefinition.setPlacement(LabelingPlacement.POINT_ABOVE_CENTER);
        labelDefinition.setWhereClause("");  //String.format("PARTY = '%s'", party)

        return labelDefinition;
    }

}
