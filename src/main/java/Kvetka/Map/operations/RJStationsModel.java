package Kvetka.Map.operations;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;

public class RJStationsModel {
    public RJStationsModel(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public RJStationsModel() {
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    Double longitude;
    Double latitude;

    public SimpleListProperty<Double> coordinates(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        return coordinates(longitude, latitude);
    }
}
