package Kvetka.Map.operations.responsibilityBand;

import Kvetka.Map.MapMarkerStyle;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;

public class ResponsibilityBand {
    GraphicsOverlay graphicsOverlay;
    CountDots dots = CountDots.FIRST;
    Graphic lineTop = new Graphic();
    Graphic lineBottom = new Graphic();

    static LinearUnit linearUnit = new LinearUnit(LinearUnitId.METERS);
    static AngularUnit angularUnit = new AngularUnit(AngularUnitId.DEGREES);

    double azimuth;
    private static double firstLon = 0;
    private static double firstLat = 0;

    public static double getSecondLon() {
        return secondLon;
    }

    public static void setSecondLon(double secondLon) {
        ResponsibilityBand.secondLon = secondLon;
    }

    public static double getSecondLat() {
        return secondLat;
    }

    public static void setSecondLat(double secondLat) {
        ResponsibilityBand.secondLat = secondLat;
    }

    private static double secondLon = 0;
    private static double secondLat = 0;

    public void showBand(){

    }

    public void hideBand(){

    }

    public void drawBand(CountDots dots, SceneView sceneView, double latitude, double longitude){
        switch (dots){
            case NONE -> {

            }
            case FIRST -> {
                if (graphicsOverlay != null) {
                    graphicsOverlay.getGraphics().clear();
                } else {
                    graphicsOverlay = new GraphicsOverlay();
                    sceneView.getGraphicsOverlays().add(graphicsOverlay);
                }
                Point firstP = new Point(longitude, latitude, SpatialReferences.getWgs84());
                azimuth = 0;

                MapMarkerStyle.arrowTL.setAngle((float) azimuth + 15);
                Graphic point = new Graphic(firstP, MapMarkerStyle.arrowTL);
                graphicsOverlay.getGraphics().add(point);
                //sceneView.getGraphicsOverlays().add(graphicsOverlay);
                firstLon = longitude;
                firstLat = latitude;
            }
            case SECOND -> {
                Point secondP = new Point(secondLon, secondLat, SpatialReferences.getWgs84());

                MapMarkerStyle.arrowTR.setAngle((float) azimuth + 250);
                Graphic point = new Graphic(secondP, MapMarkerStyle.arrowTR);
                graphicsOverlay.getGraphics().add(point);

            }
            case THIRD -> {
                Point thirdP = new Point(longitude, latitude, SpatialReferences.getWgs84());

                MapMarkerStyle.arrowBL.setAngle((float) azimuth +240);
                Graphic point = new Graphic(thirdP, MapMarkerStyle.arrowBL);
                graphicsOverlay.getGraphics().add(point);

                firstLon = longitude;
                firstLat = latitude;
            }
            case FOURTH -> {
                Point fourthP = new Point(secondLon, secondLat, SpatialReferences.getWgs84());

                MapMarkerStyle.arrowBR.setAngle((float) azimuth + 250);
                Graphic point = new Graphic(fourthP, MapMarkerStyle.arrowBR);
                graphicsOverlay.getGraphics().add(point);
            }
        }


    }

    public void updateLineTop(double lon, double lat) {
        if ( lineTop!= null) {
            var index = graphicsOverlay.getGraphics().indexOf(lineTop);
            if (index != -1) {
                graphicsOverlay.getGraphics().remove(index);
            }
        }
        PointCollection collection = new PointCollection(SpatialReferences.getWgs84());
        collection.add(firstLon, firstLat);
        collection.add(new Point(lon, lat));

        GeodeticDistanceResult distance = GeometryEngine.distanceGeodetic(collection.get(0), collection.get(1), linearUnit, angularUnit, GeodeticCurveType.GEODESIC);
        if (distance.getAzimuth1() > 0) {
            azimuth = distance.getAzimuth1();
        } else {
            azimuth = 360 + distance.getAzimuth1();
        }


        Polyline line = new Polyline(collection);

        lineTop = new Graphic(line, MapMarkerStyle.polylineSymbol);

        graphicsOverlay.getGraphics().add(lineTop);
    }

    public void updateLineBot(double lon, double lat) {
        if ( lineBottom!= null) {
            var index = graphicsOverlay.getGraphics().indexOf(lineBottom);
            if (index != -1) {
                graphicsOverlay.getGraphics().remove(index);
            }
        }
        PointCollection collection = new PointCollection(SpatialReferences.getWgs84());
        collection.add(firstLon, firstLat);
        collection.add(new Point(lon, lat));

        GeodeticDistanceResult distance = GeometryEngine.distanceGeodetic(collection.get(0), collection.get(1), linearUnit, angularUnit, GeodeticCurveType.GEODESIC);
        if (distance.getAzimuth1() > 0) {
            azimuth = distance.getAzimuth1();
        } else {
            azimuth = 360 + distance.getAzimuth1();
        }


        Polyline line = new Polyline(collection);

        lineBottom = new Graphic(line, MapMarkerStyle.polylineSymbol);

        graphicsOverlay.getGraphics().add(lineBottom);
    }
}
