package Kvetka.Map.operations.responsibilityBand;

public enum CountDots {
    NONE,
    FIRST,
    SECOND,
    THIRD,
    FOURTH
}
