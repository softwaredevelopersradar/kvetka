package Kvetka.Map.operations.signalSources;

import Kvetka.Map.models.ComplexCompositionEnemyModel;
import KvetkaModels.FHSSModel;
import KvetkaModels.ReconFWSDistribModel;
import StationModels.ListOfEnemyPictures;
import StationModels.Picture;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class SignalSources implements ISignalSources {
    static GraphicsOverlay graphicsOverlay;
    static Map<Integer, GraphicsOverlay> map = new HashMap<>();
    static List<ComplexCompositionEnemyModel> properties = new ArrayList<>();

    static PictureMarkerSymbol symbol() {
        ObservableList<Picture> pictures = ListOfEnemyPictures.getListOfPicturesAdditionally();
        PictureMarkerSymbol symbolIRI = null;

        for (var p : pictures) {
            if (p.getCode().equals("IRI")) {
                symbolIRI = new PictureMarkerSymbol(p.getImage());
            }
        }
        return symbolIRI;    }


    @Override
    public void setSourcesFWS(List<ReconFWSDistribModel> model) {

    }


    @Override
    public void setSourcesFHSS(List<FHSSModel> models) {

    }


    @Override
    public void updateSourcesFWS(List<ReconFWSDistribModel> models, SceneView sceneView) {

    }


    @Override
    public void updateSourcesFHSS(List<FHSSModel> models, SceneView sceneView) {

    }

    @Override
    public void updateBearing(BearingParameters parameters, SceneView sceneView, int bearingLength){

    }


    @Override
    public void showGraphics(SceneView sceneView) {

    }


    @Override
    public void hideGraphics(SceneView sceneView) {

    }
}
