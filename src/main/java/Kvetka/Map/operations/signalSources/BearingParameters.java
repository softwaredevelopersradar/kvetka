package Kvetka.Map.operations.signalSources;

public abstract class BearingParameters {

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    public int getBearingLength() {
        return bearingLength;
    }

    public void setBearingLength(int bearingLength) {
        this.bearingLength = bearingLength;
    }

    private double longitude = 0;
    private double latitude = 0;
    private float bearing = 0;
    private int bearingLength = 0;
}
