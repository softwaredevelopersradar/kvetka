package Kvetka.Map.operations.signalSources;

import Kvetka.Map.MapMarkerStyle;
import KvetkaModels.StationsModel;
import MapModels.AzimuthModel;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

public class DrawBearing {

    private static List<StationsModel> myStation = new ArrayList<>();

    private static Graphic line = new Graphic();
    private static GraphicsOverlay overlay = new GraphicsOverlay();
    private static Graphic linePrev = new Graphic();
    private static GraphicsOverlay overlayPrev = new GraphicsOverlay();

    public static void setDetectionStation(List<StationsModel> models) {
        myStation = List.copyOf(models);
    }

    public static void DrawBearingToTarget(BearingParameters parameters,SceneView sceneView) {   //double azimuth

        var index = sceneView.getGraphicsOverlays().indexOf(overlayPrev);

        if (index != -1) {
            sceneView.getGraphicsOverlays().get(index).getGraphics().remove(overlayPrev.getGraphics());
            sceneView.getGraphicsOverlays().remove(index);
        }

        Point center = new com.esri.arcgisruntime.geometry.Point(
                myStation.get(0).getCoordinates().longitude,
                myStation.get(0).getCoordinates().latitude,
                SpatialReferences.getWgs84());

        LinearUnit unit = new LinearUnit(LinearUnitId.KILOMETERS);
        AngularUnit angularUnit = new AngularUnit(AngularUnitId.DEGREES);

        Point end = GeometryEngine.moveGeodetic(center, parameters.getBearingLength(), unit, parameters.getBearing(), angularUnit, GeodeticCurveType.GEODESIC);

        PointCollection polylinePoints = new PointCollection(SpatialReferences.getWgs84());
        polylinePoints.add(center);
        polylinePoints.add(end);
        // create a polyline geometry from the point collection
        Polyline polyline = new Polyline(polylinePoints);

        line = new Graphic(polyline, MapMarkerStyle.polylineSymbol);


        overlay = new GraphicsOverlay();
        overlay.getGraphics().add(line);
        sceneView.getGraphicsOverlays().add(overlay);
        OldGraphics();
    }

    private static void OldGraphics() {
        linePrev = line;
        overlayPrev = overlay;
    }
}
