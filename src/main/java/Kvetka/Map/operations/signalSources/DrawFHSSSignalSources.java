package Kvetka.Map.operations.signalSources;

import Kvetka.Map.models.ComplexCompositionEnemyModel;
import KvetkaModels.FHSSModel;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DrawFHSSSignalSources extends SignalSources{
    @Override
    public void setSourcesFHSS(@NotNull List<FHSSModel> target) {
        for (var t : target) {
            //t.getListSubscribers().get(0).getPosition().getCoordinates().latitude;
            graphicsOverlay = new GraphicsOverlay();
            if(t.getListSubscribers().size() > 0) {

                Point point = new Point(t.getListSubscribers().get(0).getPosition().getCoordinates().longitude,
                        t.getListSubscribers().get(0).getPosition().getCoordinates().latitude, SpatialReferences.getWgs84());

                Graphic graphic = new Graphic(point, symbol());
                graphicsOverlay.getGraphics().add(graphic);
                map.put(t.getId(), graphicsOverlay);
                properties.add(new ComplexCompositionEnemyModel(graphicsOverlay, graphic, t.getListSubscribers().get(0).getPosition().getCoordinates().longitude,
                        t.getListSubscribers().get(0).getPosition().getCoordinates().latitude));
            }
        }
    }

    @Override
    public void updateSourcesFHSS(@NotNull List<FHSSModel> target, @NotNull SceneView sceneView) {
        if (!map.isEmpty()) {
            for (Map.Entry m : map.entrySet()) {
                var index = sceneView.getGraphicsOverlays().indexOf(m.getValue());

                if (index != -1) {
                    sceneView.getGraphicsOverlays().remove(index);
                }
            }
        }

        properties.clear();
        map.clear();
        setSourcesFHSS(target);
    }

    @Override
    public void showGraphics(SceneView sceneView) {
        if (!properties.isEmpty()) {
            for (int i = 0; i < properties.stream().count(); i++) {
                sceneView.getGraphicsOverlays().add(properties.get(i).GraphicsOverlay);
            }
        }
    }

    @Override
    public void hideGraphics(SceneView sceneView) {
        if (!properties.isEmpty()) {
            for (int i = 0; i < properties.stream().count(); i++) {
                sceneView.getGraphicsOverlays().remove(properties.get(i).GraphicsOverlay);
                sceneView.getGraphicsOverlays().remove(properties.get(i).Graphic);
            }
        }
    }

}
