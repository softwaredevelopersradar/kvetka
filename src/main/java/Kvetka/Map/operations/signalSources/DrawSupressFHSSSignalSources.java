package Kvetka.Map.operations.signalSources;

import Kvetka.Map.MapMarkerStyle;
import Kvetka.Map.NumberFormat;
import Kvetka.Map.models.ComplexCompositionEnemyModel;
import KvetkaModels.SuppressFHSSModel;
import KvetkaModels.SuppressFWSModel;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.arcgisservices.LabelingPlacement;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.labeling.ArcadeLabelExpression;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.ColorUtil;
import com.esri.arcgisruntime.symbology.TextSymbol;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class DrawSupressFHSSSignalSources {
    static Map<Integer, GraphicsOverlay> map = new HashMap<>();
    static GraphicsOverlay graphicsOverlay;
    static List<ComplexCompositionEnemyModel> properties = new ArrayList<>();

    public void showGraphics(SceneView sceneView){
        if (!properties.isEmpty()) {
            for (int i = 0; i < properties.stream().count(); i++) {
                sceneView.getGraphicsOverlays().add(properties.get(i).GraphicsOverlay);
            }
        }
    }

    public void hideGraphics(SceneView sceneView){
        if (!properties.isEmpty()) {
            for (int i = 0; i < properties.stream().count(); i++) {
                sceneView.getGraphicsOverlays().remove(properties.get(i).GraphicsOverlay);
                sceneView.getGraphicsOverlays().remove(properties.get(i).Graphic);
            }
        }
    }

    public void setSourcesFHSS(List<SuppressFHSSModel> target){
        for (var t : target) {
            /*graphicsOverlay = new GraphicsOverlay();
            graphicsOverlay.setLabelsEnabled(true);

            Point point = new Point(t..getCoordinates().longitude, t.getCoordinates().latitude, SpatialReferences.getWgs84());

            Graphic graphic = new Graphic(point, MapMarkerStyle.arrowSuppress);

            graphicsOverlay.getGraphics().add(graphic);
            map.put(t.getId(), graphicsOverlay);
            properties.add(new ComplexCompositionEnemyModel(graphicsOverlay, graphic, t.getCoordinates().longitude, t.getCoordinates().latitude));

            graphic.getAttributes().put("NAME", NumberFormat.df3.format(t.getFrequency()));
            LabelDefinition labelDefinition = makeLabelDefinition("NAME", "DESCRIPTION", Color.BLUE);
            graphicsOverlay.getLabelDefinitions().add(labelDefinition);*/
        }
    }

    public void updateSourcesFHSS(List<SuppressFHSSModel> target, SceneView sceneView){
        if (!map.isEmpty()) {
            for (Map.Entry m : map.entrySet()) {
                var index = sceneView.getGraphicsOverlays().indexOf(m.getValue());

                if (index != -1) {
                    sceneView.getGraphicsOverlays().remove(index);
                }
            }
        }

        properties.clear();
        map.clear();
        setSourcesFHSS(target);
    }

    private static @NotNull LabelDefinition makeLabelDefinition(String party, String description, Color color) {
        var textSymbol = new TextSymbol();
        textSymbol.setSize(12);
        textSymbol.setColor(ColorUtil.colorToArgb(color));
        textSymbol.setHaloColor(ColorUtil.colorToArgb(Color.WHITE));
        textSymbol.setHaloWidth(2);

        var arcadeLabelExpression =
                new ArcadeLabelExpression("$feature.NAME");

        var labelDefinition = new LabelDefinition(arcadeLabelExpression, textSymbol);

        labelDefinition.setPlacement(LabelingPlacement.POINT_CENTER_RIGHT);

        return labelDefinition;
    }

    private static @NotNull Date date() {
        Date date = new Date();
        date.getTime();
        return date;
    }
}
