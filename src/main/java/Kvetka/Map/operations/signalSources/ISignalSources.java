package Kvetka.Map.operations.signalSources;

import KvetkaModels.FHSSModel;
import KvetkaModels.ReconFWSDistribModel;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.mapping.view.SceneView;
import javafx.scene.paint.Color;

import java.util.List;

public interface ISignalSources {
    void setSourcesFWS(List<ReconFWSDistribModel> model);
    void setSourcesFHSS(List<FHSSModel> models);

    void updateSourcesFWS(List<ReconFWSDistribModel> models, SceneView sceneView);
    void updateSourcesFHSS(List<FHSSModel> models, SceneView sceneView);

    void updateBearing(BearingParameters parameters, SceneView sceneView, int bearingLength);
    void showGraphics(SceneView sceneView);
    void hideGraphics(SceneView sceneView);
}
