package Kvetka.Map;

public enum OpenWindow {
    NONE,
    AZIMUTH,
    SPACEWAVE,
    SURFACEWAVE;
}
