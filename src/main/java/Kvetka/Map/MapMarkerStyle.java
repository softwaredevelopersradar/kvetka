package Kvetka.Map;

import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.awt.*;
import java.util.Objects;

public class MapMarkerStyle {

    // create an opaque orange (0xFFFF5733) point symbol with a blue (0xFF0063FF) outline symbol
    public static SimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0xFFFF5733, 5);
    public static SimpleLineSymbol blueOutlineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0xFF0063FF, 1);

    // create a blue line symbol for the polyline
    public static SimpleLineSymbol polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, getIntFromColor(0, 0, 0), 1);

    public static SimpleMarkerSymbol simpleMarkerSymbolStart =
            new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0xFF29a98b, 10);
    // create an opaque orange (0xFFFF5733) point symbol with a blue (0xFF0063FF) outline symbol
    public static SimpleMarkerSymbol simpleMarkerSymbolEnd =
            new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0xFFFF0000, 10);

    public static SimpleMarkerSymbol simpleMarkerSymbolMid =
            new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0xFF29a98b, 5);

    static Image start = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/StartPointRoute.png")));
    public static PictureMarkerSymbol markerStartPoint = new PictureMarkerSymbol(start);

    static Image stop = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/StopPointRoute.png")));
    public static PictureMarkerSymbol markerStopPoint = new PictureMarkerSymbol(stop);

    public static Image rj = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/37.png")));

    public static Image x =  new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/x.png")));

    public static int getIntFromColor(int Red, int Green, int Blue){
        Red = (Red << 16) & 0x00FF0000; //Shift red 16-bits and mask out other stuff
        Green = (Green << 8) & 0x0000FF00; //Shift Green 8-bits and mask out other stuff
        Blue = Blue & 0x000000FF; //Mask out anything not blue.

        return 0xFF000000 | Red | Green | Blue; //0xFF000000 for 100% Alpha. Bitwise OR everything together.
    }

    static Image arrowBotRight = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/arrowBottomRight.png")));
    public static PictureMarkerSymbol arrowBR = new PictureMarkerSymbol(arrowBotRight);

    static Image arrowBotLeft = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/arrowBottomLeft.png")));
    public static PictureMarkerSymbol arrowBL = new PictureMarkerSymbol(arrowBotLeft);

    static Image arrowTopRight = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/arrowTopRight.png")));
    public static PictureMarkerSymbol arrowTR = new PictureMarkerSymbol(arrowTopRight);

    static Image arrowTopLeft = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/arrowTopLeft.png")));
    public static PictureMarkerSymbol arrowTL = new PictureMarkerSymbol(arrowTopLeft);

    static Image arrowSuppr = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/arrowSuppr.png")));
    public static PictureMarkerSymbol arrowSuppress = new PictureMarkerSymbol(arrowSuppr);

    public Image air = new Image(Objects.requireNonNull(MapMarkerStyle.class.getResourceAsStream("Ico/Airpl10.png")));
    public PictureMarkerSymbol airplane = new PictureMarkerSymbol(air);

}
