package Kvetka.Map;

import AirplanesControl.AirplanesControl;
import AirplanesControl.ITableAirplanesEvent;
import ConnectionControl.ConnectionControl;
import ConnectionControl.IConnectionEvent;
import Kvetka.Interfaces.IMapRouteEvents;
import Kvetka.Interfaces.IMapTasksWndEvents;
import Kvetka.Interfaces.IWndEvents;
import Kvetka.Map.operations.*;
import Kvetka.Map.operations.adsbReceiver.ADSBReceiver;
import Kvetka.Map.operations.azimuth.CalculationAndDrawingOfAzimuth;
import Kvetka.Map.operations.complexOfStations.ComplexOfStations;
import Kvetka.Map.operations.responsibilityBand.CountDots;
import Kvetka.Map.operations.responsibilityBand.ResponsibilityBand;
import Kvetka.Map.operations.signalSources.*;
import Kvetka.Map.operations.situation.SituationOnMap;
import Kvetka.Map.operations.zonesOfEnergy.CalculationAndDrawingOfZoneSpaceWave;
import Kvetka.Map.operations.zonesOfEnergy.CalculationAndDrawingOfZoneSurfaceWave;
import Kvetka.Map.operations.zonesOfEnergy.IDrawableZone;
import Kvetka.MapTasks.*;
import Kvetka.Properties.PropertiesMap;
import Kvetka.Properties.PropertiesOperations;
import KvetkaModels.*;

import MapModels.*;
import MapModels.RouteModel;
import RoutesControl.RoutesControl;
import ValuesCorrectLib.Converters;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.layers.RasterLayer;
import com.esri.arcgisruntime.mapping.*;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.portal.Portal;

import com.esri.arcgisruntime.raster.Raster;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import receive.ADSBModel;
import receive.IDataADSB;
import receive.ReceiveWnd;


import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MapWnd extends AnchorPane implements ITableAirplanesEvent, IConnectionEvent, IDataADSB, IMapTasksWndEvents, Initializable {

    public static Stage WindowMap;
    static Scene FrontEndMap;

    private final List<IWndEvents> listenersMap = new ArrayList<>();
    private final List<IMapRouteEvents> listenersRoute = new ArrayList<>();

    public static boolean isLocalUpdate = false;

    public void addListenerMap(IWndEvents toAdd) {
        listenersMap.add(toAdd);
    }

    //region Controls, Containers
    @FXML
    public SplitPane splitMap;
    @FXML
    public ToggleButton tbComplexCompositionOwnWnd;
    @FXML
    public ToggleButton tbComplexCompositionEnemyWnd;
    @FXML
    public ToggleButton tbAzimuthWnd;
    @FXML
    public ToggleButton tbZoneSpaceWaveWnd;
    @FXML
    public ToggleButton tbZoneSurfaceWaveWnd;
    @FXML
    public ToggleButton tbShowPanelMap;
//    @FXML public Button bShowPanelsMapDefault;

    @FXML
    public TextField tfLatitude;
    @FXML
    public TextField tfLongitude;
    @FXML
    public TextField tfSK42Lat;
    @FXML
    public TextField tfSK42Lon;
    @FXML
    public TextField tfSeaHeight;
    @FXML
    public TextField tfScale;

    @FXML
    public CheckBox chbResFWS;
    @FXML
    public CheckBox chbResFHSS;
    @FXML
    public CheckBox chbComplex;
    @FXML
    public CheckBox chbOwn;
    @FXML
    public CheckBox chbEnemy;
    @FXML
    public CheckBox chbBearing;
    @FXML
    public CheckBox chbSector;
    @FXML
    public CheckBox chbZoneRI;
    @FXML
    public CheckBox chbZoneRS;
    @FXML
    public CheckBox chbCommCenters;
    @FXML
    public CheckBox chbNetworks;
    //    @FXML public CheckBox chbSignatures;
    @FXML
    public CheckBox chbAirSituation;
    // endregion

    // region UserControls
    @FXML
    public AirplanesControl ucAirplanes;
    @FXML
    public RoutesControl ucRoutes;
    @FXML
    public ConnectionControl ucADSBConnection;
    //   @FXML public MapControl ucMap;// = new MapControl();
    // endregion

    // region Lists
    public List<AirplanesModel> listAirplanes = new ArrayList<>();
    // endregion

    public ReceiveWnd ADSBReceiver = new ReceiveWnd();
    private boolean IsConnectADSB = true;

    // region ArcGISS
    @FXML
    public SceneView sceneViewMap;// = new SceneView();

    private Basemap basemap;
    private Portal portal;
    public ArcGISScene arcGISScene;

    ContextMenu contextMenu;
    IdentifyGraphicsOverlayResult identifyGraphicsOverlayResult;
    List<List<Graphic>> graphics = null;

    MyStationsModel myStationsModel;
    EnemyStationModel enemyStationModel;
    RouteModel routeModel;
    AzimuthModel azimuthModel = new AzimuthModel();

    // endregion

    // region Windows of tasks
    AzimuthWnd azimuthWnd;
    ComplexCompositionOwnWnd complexCompositionOwnWnd;
    ComplexCompositionEnemyWnd complexCompositionEnemyWnd;
    ZoneSpaceWaveWnd zoneSpaceWaveWnd;
    ZoneSurfaceWaveWnd zoneSurfaceWaveWnd;

    IDrawableZone surfaceWave;
    IDrawableZone spaceWave;

    ISignalSources signalFWSSources;
    ISignalSources signalFHSSSources;

    DrawSupressFWSSignalSources supressFWSSignalSources;
    DrawSupressFHSSSignalSources supressFHSSSignalSources;

    CalculationAndDrawingOfAzimuth drawAzimuth;

    ADSBReceiver adsbReceiver;

    ResponsibilityBand responsibilityBand;

    ComplexOfStations complexOfStations;

    OpenWindow openWindow = OpenWindow.NONE;
    NumberOfClicks numberOfClicks = NumberOfClicks.FIRST;
    byte click = 0;
    boolean azPressed = false;

    byte clickBand = 0;
    boolean bandPressed = false;
    boolean bandPartT = false;
    boolean bandPartB = false;
    // endregion

    public MapWnd() {

        //ArcGISRuntimeEnvironment.setInstallDirectory("100.11.2");

        InitMapWnd();
        InitMap();
        InitContextMenu();

        // ucMap = new MapControl();
        // ucMap.InitMap();

        WindowMap.setTitle("Карта");
        WindowMap.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("Ico/ShowMap.png"))));
    }

    private void InitMapWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MapWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndMap = new Scene(fxmlLoader.load(), 800, 600);
            WindowMap = new Stage();
            WindowMap.initModality(Modality.WINDOW_MODAL);
            WindowMap.setMaximized(true);
            WindowMap.setResizable(true);
            WindowMap.setScene(FrontEndMap);
            WindowMap.show();

            WindowMap.setOnCloseRequest(we -> {

                WindowMap.hide();

                // Stage is closing
                for (IWndEvents mapEvent : listenersMap)
                    mapEvent.OnCloseMap();

                CloseTasksWnd();
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void InitMap() {

        basemap = new Basemap();
        arcGISScene = new ArcGISScene(basemap);
        sceneViewMap.setArcGISScene(arcGISScene);

        surfaceWave = new CalculationAndDrawingOfZoneSurfaceWave();
        spaceWave = new CalculationAndDrawingOfZoneSpaceWave();

        signalFWSSources = new DrawFWSSignalSources();
        signalFHSSSources = new DrawFHSSSignalSources();

        supressFWSSignalSources = new DrawSupressFWSSignalSources();
        supressFHSSSignalSources = new DrawSupressFHSSSignalSources();

        drawAzimuth = new CalculationAndDrawingOfAzimuth();

        adsbReceiver = new ADSBReceiver();

        responsibilityBand = new ResponsibilityBand();

        complexOfStations = new ComplexOfStations();

        sceneViewMap.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {

            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {

                contextMenu.show(sceneViewMap, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
                javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(contextMenuEvent.getX()),
                        Math.round(contextMenuEvent.getY()));
                com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

                myStationsModel = new MyStationsModel();
                myStationsModel.setLatitude(surfacePoint.getY());
                myStationsModel.setLongitude(surfacePoint.getX());

                enemyStationModel = new EnemyStationModel();
                enemyStationModel.setLatitude(surfacePoint.getY());
                enemyStationModel.setLongitude(surfacePoint.getX());

                routeModel = new RouteModel();
                routeModel.setLatitude(surfacePoint.getY());
                routeModel.setLongitude(surfacePoint.getX());

                if (sceneViewMap.getGraphicsOverlays().isEmpty()) {
                    System.out.println("NoN Images");
                }
            }
        });

        sceneViewMap.setInteractionListener(new SceneView.DefaultInteractionListener(sceneViewMap) {

            public void onMouseMoved(javafx.scene.input.MouseEvent e) {

                javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(e.getX()),
                        Math.round(e.getY()));
                com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

                tfLatitude.clear();
                tfLongitude.clear();
                tfSK42Lat.clear();
                tfSK42Lon.clear();
                tfSeaHeight.clear();

                if (surfacePoint != null) {
                    tfLatitude.appendText(Converters.LatitudeConverter(surfacePoint.getY()));
                    tfLongitude.appendText(Converters.LongitudeConverter(surfacePoint.getX()));
//                    tfLatitude.appendText(String.valueOf(NumberFormat.df6.format(surfacePoint.getY())));
//                    tfLongitude.appendText(String.valueOf(NumberFormat.df6.format(surfacePoint.getX())));
                    tfSeaHeight.appendText(String.valueOf(((double) ((int) (surfacePoint.getZ() * 100.0))) / 100.0));
                    double[] LatLon = MapOperations.WGS84ToSK42Meters(surfacePoint.getY(), surfacePoint.getX(), 0);
                    tfSK42Lat.appendText(String.valueOf(NumberFormat.df1.format(LatLon[0])));
                    tfSK42Lon.appendText(String.valueOf(NumberFormat.df1.format(LatLon[1])));

                    if(azPressed){
                        azimuthModel.setLatitude2(surfacePoint.getY());
                        azimuthModel.setLongitude2(surfacePoint.getX());
                        azimuthWnd.UpdateCoordinatesTarget(azimuthModel);
                        drawAzimuth.updateLine(surfacePoint.getX(), surfacePoint.getY(), sceneViewMap);
                        azimuthModel.setAzimuth(drawAzimuth.getAzimuthRez());
                        azimuthWnd.UpdateAzimuth(azimuthModel);
                    }
                    if(bandPressed){
                        if(bandPartT){
                            responsibilityBand.updateLineTop(surfacePoint.getX(), surfacePoint.getY());
                        }
                        if(bandPartB){
                            responsibilityBand.updateLineBot(surfacePoint.getX(), surfacePoint.getY());
                        }

                        responsibilityBand.setSecondLat(surfacePoint.getY());
                        responsibilityBand.setSecondLon(surfacePoint.getX());
                    }
                }
                e.consume();
            }

            public void onMousePressed(javafx.scene.input.MouseEvent e) {
                if (e.getButton() == MouseButton.PRIMARY) {
                    if (contextMenu.isShowing()) contextMenu.hide();
                    if (openWindow != OpenWindow.NONE) {
                        switch (openWindow) {
                            case AZIMUTH -> {
                                CalculateAzimuth(e);
                            }
                            case SPACEWAVE, SURFACEWAVE -> {
                                SelectStationsRJ(e);
                            }
                            default -> {
                                break;
                            }
                        }
                    }
//                    Viewpoint current = sceneView.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
//                    scale.appendText(Double.toString(current.getTargetScale()));
//                    e.consume();
                } else {
                    // let the default listener you've overridden deal with other events
                    super.onMousePressed(e);
                }
            }

            public void onScroll(javafx.scene.input.ScrollEvent e) {
                try {
                    tfScale.clear();
                    Viewpoint current = sceneViewMap.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
                    double dScale = ((double) ((int) (current.getTargetScale() * 100.0))) / 100.0;
                    NumberFormat.df0.setMaximumFractionDigits(0);
                    tfScale.appendText("1:" + NumberFormat.df0.format(dScale));
                    e.consume();
                    super.onScroll(e);
                } catch (Exception ex) {

                }
            }
        });

        try {
            //Raster raster = new Raster("E:/СПО/KvetkaProject/Pictures/One File/1.tif"); // Сергей
            //Raster raster = new Raster("D:/Map/One File/1.tif"); // Лина

            if(!Files.exists(Paths.get(PropertiesOperations.getLocalProperties().getMap().getMapPath()))) {

                PropertiesOperations.getLocalProperties().getMap().setMapPath("");
                PropertiesOperations.getLocalProperties().getMap().setMatrixPath("");
                PropertiesOperations.YamlSave(PropertiesOperations.getLocalProperties());
                PropertiesOperations.setLocalProperties(PropertiesOperations.YamlLoad());

                isLocalUpdate = true;
            }

            if (PropertiesOperations.getLocalProperties().getMap().getMapPath() != "") {

                Raster raster = new Raster(PropertiesOperations.getLocalProperties().getMap().getMapPath());
                PropertiesMap.setRasterLayerMap(new RasterLayer(raster));
                arcGISScene.getOperationalLayers().add(PropertiesMap.getRasterLayerMap());
            }
            if (PropertiesOperations.getLocalProperties().getMap().getMatrixPath() != "") {

                ArrayList<String> listMatrixPath = new ArrayList<String>();
                listMatrixPath.add(PropertiesOperations.getLocalProperties().getMap().getMatrixPath());
                PropertiesMap.setRasterElevationSourceMap(new RasterElevationSource(listMatrixPath));
                Surface surface = new Surface();
                surface.getElevationSources().add(PropertiesMap.getRasterElevationSourceMap());
                surface.setElevationExaggeration(1);
                arcGISScene.setBaseSurface(surface);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SelectStationsRJ(MouseEvent event) {
        javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(event.getX()),
                Math.round(event.getY()));

        var identifyGraphics = IdentifyGraphic.identify(screenPoint, sceneViewMap);

        identifyGraphics.addDoneListener(() -> {
            try {
                var rezIdentify = identifyGraphics.get();
                switch (openWindow) {
                    case SPACEWAVE -> {
                        SelectGraphics.selectForSpace(rezIdentify);
                    }
                    case SURFACEWAVE -> {
                        SelectGraphics.selectForSurface(rezIdentify);
                    }
                    default -> {
                    }
                }
                //IsSelectedGraphic(rezIdentify);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            } catch (ExecutionException executionException) {
                executionException.printStackTrace();
            }
        });
    }

    private void IsSelectedGraphic(List<IdentifyGraphicsOverlayResult> results) {
        var firstEl = results.get(0).getGraphics().get(0);
        firstEl.setSelected(true);
        //String style = ((SimpleMarkerSymbol)firstEl.getSymbol()).getStyle().toString();

    }

    private void CalculateAzimuth(MouseEvent event) {

        if ((click == 0)) {  //(getAzimuth == true) &&
            azPressed = true;
            javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(event.getX()),
                    Math.round(event.getY()));
            com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

            azimuthModel.setLatitude1(surfacePoint.getY());
            azimuthModel.setLongitude1(surfacePoint.getX());
            azimuthWnd.UpdateCoordinatesASP(azimuthModel);
            drawAzimuth.setFirstPoint(azimuthModel, sceneViewMap);
            click++;

        } else if (click == 1) {
            azPressed = false;
            javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(event.getX()),
                    Math.round(event.getY()));
            com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

            azimuthModel.setLatitude2(surfacePoint.getY());
            azimuthModel.setLongitude2(surfacePoint.getX());
            drawAzimuth.setSecondPoint(azimuthModel, sceneViewMap);
            azimuthWnd.UpdateCoordinatesTarget(azimuthModel);
            click = 0;
        }
    }

    public void AddListenerRoute(IMapRouteEvents listener) {
        listenersRoute.add(listener);
    }

    private void NotifyListenersRoute() {
        for (IMapRouteEvents listener : listenersRoute) {
            listener.onSubmittedRoute(CalculationAndDrawingOfRoute.model);
        }
    }

    private void ShowWindowOwn() {

        if (!tbComplexCompositionOwnWnd.isSelected()) {

            tbComplexCompositionOwnWnd.setSelected(true);
            complexCompositionOwnWnd = new ComplexCompositionOwnWnd();
            complexCompositionOwnWnd.addListenerComplexCompositionOwn(this);

            complexCompositionOwnWnd.UpdateCoordinates(myStationsModel);
        } else {

            tbComplexCompositionOwnWnd.setSelected(false);
            complexCompositionOwnWnd.WindowComplexCompositionOwn.close();
            complexCompositionOwnWnd.listenersComplexCompositionOwn.clear();
        }
    }

    private void ShowWindowEnemy() {

        if (!tbComplexCompositionEnemyWnd.isSelected()) {

            tbComplexCompositionEnemyWnd.setSelected(true);
            complexCompositionEnemyWnd = new ComplexCompositionEnemyWnd();
            complexCompositionEnemyWnd.addListenerComplexCompositionEnemy(this);

            complexCompositionEnemyWnd.UpdateCoordinates(enemyStationModel);
        } else {

            tbComplexCompositionEnemyWnd.setSelected(false);
            complexCompositionEnemyWnd.WindowComplexCompositionEnemy.close();
            complexCompositionEnemyWnd.listenersComplexCompositionEnemy.clear();
        }
    }

    // region ContextMenu
    private void InitContextMenu() {
        final int[] counter = {0};
        contextMenu = new ContextMenu();

        ImageView startPoint = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("Ico/StartPointRoute.png"))));
        ImageView stopPoint = new ImageView(new Image(Objects.requireNonNull(getClass().getResourceAsStream("Ico/StopPointRoute.png"))));


        Menu add = new Menu("Добавить");
        MenuItem childAddOwn = new MenuItem("Свой");
        MenuItem childAddEnemy = new MenuItem("Противник");
        add.getItems().addAll(childAddOwn, childAddEnemy);

        Menu route = new Menu("Маршрут");
        MenuItem childStart = new MenuItem("Новая точка", startPoint);
        MenuItem childEnd = new MenuItem("Конечная точка", stopPoint);
        route.getItems().addAll(childStart, childEnd);

        Menu band = new Menu("Полоса");
        MenuItem childNew = new MenuItem("Новая точка");
        band.getItems().add(childNew);

        MenuItem change = new MenuItem("Изменить");

        MenuItem delete = new MenuItem("Удалить");

        childAddOwn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                ShowWindowOwn();
            }
        });

        childAddEnemy.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                ShowWindowEnemy();
            }
        });

        change.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

            }
        });

        childStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                switch (numberOfClicks) {
                    case FIRST -> {
                        sceneViewMap.getGraphicsOverlays().add(CalculationAndDrawingOfRoute.drawingRoute(routeModel, numberOfClicks));
                        numberOfClicks = NumberOfClicks.MIDDLE;
                        counter[0] = 0;
                    }
                    case MIDDLE -> {
                        CalculationAndDrawingOfRoute.drawingRoute(routeModel, numberOfClicks);
                        //sceneViewMap.getGraphicsOverlays().add(CalculationAndDrawingOfRoute.drawingRoute(routeModel, numberOfClicks));
                    }
                    default -> {
                    }
                }
                //NotifyListenersRoute();
            }
        });

        childEnd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                if (counter[0] == 0) {
                    if (numberOfClicks != NumberOfClicks.LAST) {
                        numberOfClicks = NumberOfClicks.LAST;
                        CalculationAndDrawingOfRoute.drawingRoute(routeModel, numberOfClicks);
                        //sceneViewMap.getGraphicsOverlays().add(CalculationAndDrawingOfRoute.drawingRoute(routeModel, numberOfClicks));
                        numberOfClicks = NumberOfClicks.FIRST;
                        counter[0]++;
                    } else {
                    }
                } else {
                }
                NotifyListenersRoute();
            }
        });

        childNew.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(clickBand ==0){
                    CountDots dot = CountDots.FIRST;
                    bandPressed = true;
                    bandPartT = true;

                    javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(contextMenu.getX()),
                            Math.round(contextMenu.getY()));
                    com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

                    responsibilityBand.drawBand(dot,sceneViewMap, surfacePoint.getY(), surfacePoint.getX());
                    clickBand++;
                }else if(clickBand == 1){
                    CountDots dot = CountDots.SECOND;
                    bandPressed = false;
                    bandPartT = false;
                    javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(contextMenu.getX()),
                            Math.round(contextMenu.getY()));
                    com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

                    responsibilityBand.drawBand(dot,sceneViewMap, surfacePoint.getY(), surfacePoint.getX());
                    clickBand++;
                } else if(clickBand == 2){
                    CountDots dot = CountDots.THIRD;

                    javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(contextMenu.getX()),
                            Math.round(contextMenu.getY()));
                    com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

                    responsibilityBand.drawBand(dot,sceneViewMap, surfacePoint.getY(), surfacePoint.getX());
                    bandPressed = true;
                    bandPartB = true;
                    clickBand++;
                } else if(clickBand == 3){
                    CountDots dot = CountDots.FOURTH;
                    bandPressed = false;
                    bandPartB = false;
                    javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(contextMenu.getX()),
                            Math.round(contextMenu.getY()));
                    com.esri.arcgisruntime.geometry.Point surfacePoint = sceneViewMap.screenToBaseSurface(screenPoint);

                    responsibilityBand.drawBand(dot,sceneViewMap, surfacePoint.getY(), surfacePoint.getX());
                    clickBand = 0;
                } else if(clickBand > 3){

                }


            }
        });

        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                javafx.geometry.Point2D screenPoint = new javafx.geometry.Point2D(Math.round(contextMenu.getX()),
                        Math.round(contextMenu.getY()));

                var x = 0.0;
                var sizeWnd = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
                var widthScreen = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth();
                if(screenPoint.getX() % sizeWnd < 0){
                    x = screenPoint.getX() % sizeWnd + widthScreen;
                }
                else {
                    x = screenPoint.getX() % sizeWnd;
                }

                javafx.geometry.Point2D Point = new javafx.geometry.Point2D(Math.round(x),
                        Math.round(contextMenu.getY()));

                var identifyGraphics = IdentifyGraphic.identify(Point, sceneViewMap);

                identifyGraphics.addDoneListener(() -> {
                    try {
                        var rezIdentify = identifyGraphics.get();
                        IsSelectedGraphic(rezIdentify);

                        RemoveGraphics.remove(rezIdentify, sceneViewMap);

                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    } catch (ExecutionException executionException) {
                        executionException.printStackTrace();
                    }
                });
            }
        });

        contextMenu.getItems().addAll(add, route, band, delete);
    }

    public void UpdateGraphicsFromDB(List<KvetkaModels.RouteModel> routeModel) {
        try {
            //sceneViewMap.getGraphicsOverlays().add(CalculationAndDrawingOfRoute.DrawRoutesFromDB(routeModel, sceneViewMap)) ;
            CalculationAndDrawingOfRoute.DrawRoutesFromDB(routeModel, sceneViewMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void RemoveGraphicsFromDB(List<KvetkaModels.RouteModel> routeModel) {
        CalculationAndDrawingOfRoute.UpdateRoutes(routeModel, sceneViewMap);
    }

    public void UpdateReconFWSDistribOnMap(List<ReconFWSDistribModel> model) {
        signalFWSSources.updateSourcesFWS(model, sceneViewMap);
        if (chbResFWS.isSelected()) {
            signalFWSSources.showGraphics(sceneViewMap);
        }
    }

    public void SetReconFWSDistribOnMap(List<ReconFWSDistribModel> model) {
        signalFWSSources.setSourcesFWS(model);
        if (chbResFWS.isSelected()) {
            signalFWSSources.showGraphics(sceneViewMap);
        }
    }

    public void updateSupressFWSSignalSourcesOnMap(List<SuppressFWSModel> models){
        supressFWSSignalSources.updateSourcesFWS(models, sceneViewMap);
        if (chbResFWS.isSelected()) {
            supressFWSSignalSources.showGraphics(sceneViewMap);
        }
    }

    public void setSupressFWSSignalSourcesOnMap(List<SuppressFWSModel> models){
        supressFWSSignalSources.setSourcesFWS(models);
        if (chbResFWS.isSelected()) {
            supressFWSSignalSources.showGraphics(sceneViewMap);
        }
    }

    public void DrawingBearing(BearingParameters parameters){
        if (chbResFWS.isSelected()) {
            //signalFWSSources.updateBearing(parameters, sceneViewMap, bearingLength);
            DrawBearing.DrawBearingToTarget(parameters, sceneViewMap);
        }
    }

    public void UpdateReconFHSS(List<FHSSModel> models){
        signalFHSSSources.updateSourcesFHSS(models, sceneViewMap);
        if (chbResFHSS.isSelected()) {
            signalFHSSSources.showGraphics(sceneViewMap);
        }
    }

    public void SetReconFHSS(List<FHSSModel> models){
        signalFHSSSources.setSourcesFHSS(models);
        if (chbResFHSS.isSelected()) {
            signalFHSSSources.showGraphics(sceneViewMap);
        }
    }

    public void setListStationsModel(List<StationsModel> listStationModel){
        complexOfStations.removeComplex(sceneViewMap);
        complexOfStations.complex = MapOperations.complex;
        complexOfStations.setComplex(listStationModel, sceneViewMap);
        if(chbComplex.isSelected()){
            complexOfStations.showGraphics(sceneViewMap);
        }
    }

    public void updateSupressFHSSSignalSourcesOnMap(List<SuppressFWSModel> models){
        /*supressFHSSSignalSources.updateSourcesFWS(models, sceneViewMap);
        if (chbResFWS.isSelected()) {
            supressFHSSSignalSources.showGraphics(sceneViewMap);
        }*/
    }

    public void setSupressFHSSSignalSourcesOnMap(List<SuppressFHSSModel> models){
        /*supressFHSSSignalSources.setSourcesFWS(models);
        if (chbResFWS.isSelected()) {
            supressFHSSSignalSources.showGraphics(sceneViewMap);
        }*/
    }

    // endregion

    // region CheckBoxes Checked
    public void ResFWS_Click(ActionEvent actionEvent) {

        if (chbResFWS.isSelected()) {
            signalFWSSources.showGraphics(sceneViewMap);
            supressFWSSignalSources.showGraphics(sceneViewMap);
        } else {
            signalFWSSources.hideGraphics(sceneViewMap);
            supressFWSSignalSources.hideGraphics(sceneViewMap);
        }
    }

    public void ResFHSS_Click(ActionEvent actionEvent) {

        if (chbResFHSS.isSelected()) {
            signalFHSSSources.showGraphics(sceneViewMap);
        } else {
            signalFHSSSources.hideGraphics(sceneViewMap);
        }
    }

    public void Complex_Click(ActionEvent actionEvent) {


        if (chbComplex.isSelected()) {
            setListStationsModel(MapOperations.ListStationsModel);
            //MapOperations.showComplexStations(sceneViewMap);
            //complexOfStations.showGraphics(sceneViewMap);
        } else {
            complexOfStations.hideGraphics(sceneViewMap);
            //MapOperations.hideComplexStations(sceneViewMap);
        }
    }

    public void Own_Click(ActionEvent actionEvent) {

        if (chbOwn.isSelected()) {

            MapOperations.AddImagesOwn(sceneViewMap);
        } else {

            MapOperations.RemoveImagesOwn(sceneViewMap);
        }
    }

    public void Enemy_Click(ActionEvent actionEvent) {

        if (chbEnemy.isSelected()) {

            MapOperations.AddImagesEnemy(sceneViewMap);
        } else {

            MapOperations.RemoveImagesEnemy(sceneViewMap);
        }
    }

    public void Bearing_Click(ActionEvent actionEvent) {

        if (chbBearing.isSelected()) {

            int i = 90;
        } else {


        }
    }

    public void Sector_Click(ActionEvent actionEvent) {

        if (chbSector.isSelected()) {

            int i = 90;
        } else {


        }
    }

    public void ZoneRI_Click(ActionEvent actionEvent) {

        if (chbZoneRI.isSelected()) {

            int i = 90;
        } else {


        }
    }

    public void ZoneRS_Click(ActionEvent actionEvent) {

        MapOperations.ShowZoneRS(sceneViewMap, chbZoneRS.isSelected());
    }

    public void CommCenters_Click(ActionEvent actionEvent) {

        if (chbCommCenters.isSelected()) {

            int i = 90;
        } else {


        }
    }

    public void Networks_Click(ActionEvent actionEvent) {

        if (chbNetworks.isSelected()) {

            int i = 90;
        } else {


        }
    }

//    public void Signatures_Click(ActionEvent actionEvent) {
//
//
//    }

    public void AirSituation_Click(ActionEvent actionEvent) {

        if (chbAirSituation.isSelected()) {
            adsbReceiver.showGraphics(sceneViewMap);
        } else {
            adsbReceiver.hideGraphics(sceneViewMap);
        }
    }
    // endregion

    // region Buttons Show Windows
    public void ShowComplexCompositionOwnWnd_Click(ActionEvent actionEvent) {

        if (tbComplexCompositionOwnWnd.isSelected()) {

            complexCompositionOwnWnd = new ComplexCompositionOwnWnd();
            complexCompositionOwnWnd.addListenerComplexCompositionOwn(this);

        } else {

            complexCompositionOwnWnd.WindowComplexCompositionOwn.close();
            complexCompositionOwnWnd.listenersComplexCompositionOwn.clear();
        }
    }

    public void ShowComplexCompositionEnemyWnd_Click(ActionEvent actionEvent) {

        if (tbComplexCompositionEnemyWnd.isSelected()) {

            complexCompositionEnemyWnd = new ComplexCompositionEnemyWnd();
            complexCompositionEnemyWnd.addListenerComplexCompositionEnemy(this);

        } else {

            complexCompositionEnemyWnd.WindowComplexCompositionEnemy.close();
            complexCompositionEnemyWnd.listenersComplexCompositionEnemy.clear();
        }
    }

    public void ShowAzimuthWnd_Click(ActionEvent actionEvent) {

        if (tbAzimuthWnd.isSelected()) {
            azimuthWnd = new AzimuthWnd();
            azimuthWnd.addListenerAzimuth(this);

            drawAzimuth = new CalculationAndDrawingOfAzimuth();

            openWindow = OpenWindow.AZIMUTH;
        } else {
            azimuthWnd.listenersAzimuth.clear();
            azimuthWnd.WindowAzimuth.close();

            openWindow = OpenWindow.NONE;
            drawAzimuth.clearAzimuth(sceneViewMap);
        }
    }

    public void ShowZoneSpaceWaveWnd_Click(ActionEvent actionEvent) {

        if (tbZoneSpaceWaveWnd.isSelected()) {

            zoneSpaceWaveWnd = new ZoneSpaceWaveWnd();
            zoneSpaceWaveWnd.addListenerZoneSpaceWave(this);
            openWindow = OpenWindow.SPACEWAVE;
            spaceWave.addOverlay(sceneViewMap);

        } else {

            zoneSpaceWaveWnd.WindowZoneSpaceWave.close();
            zoneSpaceWaveWnd.listenersZoneSpaceWave.clear();
            openWindow = OpenWindow.NONE;
            spaceWave.clearOverlay(sceneViewMap);
        }
    }

    public void ShowZoneSurfaceWaveWnd_Click(ActionEvent actionEvent) {

        if (tbZoneSurfaceWaveWnd.isSelected()) {

            zoneSurfaceWaveWnd = new ZoneSurfaceWaveWnd();
            zoneSurfaceWaveWnd.addListenerZoneSurfaceWave(this);
            openWindow = OpenWindow.SURFACEWAVE;
            surfaceWave.addOverlay(sceneViewMap);

        } else {

            zoneSurfaceWaveWnd.WindowZoneSurfaceWave.close();
            zoneSurfaceWaveWnd.listenersZoneSurfaceWave.clear();
            openWindow = OpenWindow.NONE;
            surfaceWave.clearOverlay(sceneViewMap);
        }
    }
    // endregion

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        // region Markup
        splitMap.getDividers().get(0).positionProperty().addListener((obs, oldPos, newPos) -> {

            setDividersPositionCenterMap(new double[]{oldPos.doubleValue(), newPos.doubleValue()});
        });
        splitMap.getDividers().get(0).positionProperty().set(0.0);

        ShowPanelsMapDefault_Click(new ActionEvent());
        // endregion

        //   ArcGISRuntimeEnvironment.setLicense("runtimelite,1000,rud3966618973,none,MJJC7XLS10PB003AD226");

        // region Add Listeners
        ucADSBConnection.addListener(this);
        ucAirplanes.addListener(this);
        ADSBReceiver.addListenerDataADSB(this);
        // endregion
    }

    // region Markup WndMap
    public void ShowPanelMap_Click(ActionEvent actionEvent) {

        if (tbShowPanelMap.isSelected())
            splitMap.setDividerPosition(0, 0.8);
        else
            splitMap.setDividerPosition(0, 1.0);
    }

    public void ShowPanelsMapDefault_Click(ActionEvent actionEvent) {

        splitMap.setDividerPosition(0, 0.8);

        tbShowPanelMap.setSelected(true);
    }

    private double[] DividersPositionCenterMap = new double[2];

    public double[] getDividersPositionCenterMap() {
        return DividersPositionCenterMap;
    }

    public void setDividersPositionCenterMap(double[] dividersPositionCenterMap) {

        DividersPositionCenterMap = dividersPositionCenterMap;
        UpdateMarkupCenterMap();
    }

    private void UpdateMarkupCenterMap() {

        double[] positionCenter = getDividersPositionCenterMap();

        if (positionCenter[1] > 0.97 && positionCenter[1] <= 1.0) {

            tbShowPanelMap.setSelected(false);
        } else {
            tbShowPanelMap.setSelected(true);
        }
    }

    // endregion

    // region Airplanes
    @Override
    public void OnDeleteRecord(AirplanesModel airplanesModel) {

        Platform.runLater(() -> {

            boolean isRemove = listAirplanes.remove(airplanesModel);
            if (isRemove) {

                ucAirplanes.UpdateAirplanes(listAirplanes);
                adsbReceiver.deleteAir(airplanesModel, sceneViewMap);
            }
        });
    }

    @Override
    public void OnClearRecords() {

        Platform.runLater(() -> {

            listAirplanes = new ArrayList<>();
            ucAirplanes.UpdateAirplanes(listAirplanes);
            adsbReceiver.clearAir(sceneViewMap);
        });
    }
    // endregion

    // region OnClose events
    @Override
    public void OnCloseAzimuthWnd() {

        tbAzimuthWnd.setSelected(false);
        openWindow = OpenWindow.NONE;
        drawAzimuth.clearAzimuth(sceneViewMap);
    }

    @Override
    public void OnCloseComplexCompositionOwnWnd() {

        tbComplexCompositionOwnWnd.setSelected(false);
    }

    @Override
    public void OnCloseComplexCompositionEnemyWnd() {

        tbComplexCompositionEnemyWnd.setSelected(false);
    }

    @Override
    public void OnCloseZoneSpaceWaveWnd() {

        tbZoneSpaceWaveWnd.setSelected(false);
        spaceWave.clearOverlay(sceneViewMap);
    }

    @Override
    public void OnCloseZoneSurfaceWaveWnd() {

        tbZoneSurfaceWaveWnd.setSelected(false);
        surfaceWave.clearOverlay(sceneViewMap);
    }

    private void CloseTasksWnd() {

        if (complexCompositionOwnWnd != null) {

            complexCompositionOwnWnd.listenersComplexCompositionOwn.clear();
            complexCompositionOwnWnd.WindowComplexCompositionOwn.close();
            OnCloseComplexCompositionOwnWnd();
        }

        if (complexCompositionEnemyWnd != null) {

            complexCompositionEnemyWnd.listenersComplexCompositionEnemy.clear();
            complexCompositionEnemyWnd.WindowComplexCompositionEnemy.close();
            OnCloseComplexCompositionEnemyWnd();
        }

        if (azimuthWnd != null) {

            azimuthWnd.listenersAzimuth.clear();
            azimuthWnd.WindowAzimuth.close();
            OnCloseAzimuthWnd();
        }

        if (zoneSpaceWaveWnd != null) {

            zoneSpaceWaveWnd.listenersZoneSpaceWave.clear();
            zoneSpaceWaveWnd.WindowZoneSpaceWave.close();
            OnCloseZoneSpaceWaveWnd();
        }

        if (zoneSurfaceWaveWnd != null) {

            zoneSurfaceWaveWnd.listenersZoneSurfaceWave.clear();
            zoneSurfaceWaveWnd.WindowZoneSurfaceWave.close();
            OnCloseZoneSurfaceWaveWnd();
        }
    }
    // endregion

    // region OnSend events
    @Override
    public void OnSendComplexCompositionOwn(MyStationsModel complexCompositionOwnModel) throws IOException {

        MapOperations.setMyStationsModel(complexCompositionOwnModel);
        MapOperations.saveSituation();

        if (chbOwn.isSelected()) {

            MapOperations.AddImagesOwn(sceneViewMap);
        }
    }

    public void setSituationProperties(SituationOnMap situation) throws IOException {
        MapOperations.setYamlLoadCompositionOwn(situation);
    }

    @Override
    public void OnSendComplexCompositionEnemy(EnemyStationModel complexCompositionEnemyModel) throws IOException {

        MapOperations.setEnemyStationModel(complexCompositionEnemyModel);
        MapOperations.saveSituation();

        if (chbEnemy.isSelected()) {

            MapOperations.AddImagesEnemy(sceneViewMap);
        }
    }

    @Override
    public void OnSendAzimuth(AzimuthModel azimuthModel) {

        drawAzimuth.setFirstPoint(azimuthModel, sceneViewMap);
        drawAzimuth.setSecondPoint(azimuthModel, sceneViewMap);

        drawAzimuth.updateLine(azimuthModel.getLongitude2(), azimuthModel.getLatitude2(), sceneViewMap);
        azimuthModel.setAzimuth(drawAzimuth.getAzimuthRez());
        azimuthWnd.UpdateAzimuth(azimuthModel);

        //sceneViewMap.getGraphicsOverlays().add(CalculationAndDrawingOfAzimuth.CalculateAzimuth(azimuthModel));

        //azimuthModel.setAzimuth(CalculationAndDrawingOfAzimuth.azimuthRez);
        //azimuthWnd.UpdateAzimuth(azimuthModel);
    }

    @Override
    public void OnSendZoneSpaceWave(EDPRVModel zoneSpaceWaveModel) {
        spaceWave.calculateParameters(zoneSpaceWaveModel);
        //zoneSpaceWaveWnd.UpdateRezults()
    }

    @Override
    public void OnSendZoneSurfaceWave(EDPOVModel zoneSurfaceWaveModel) {
        surfaceWave.calculateParameters(zoneSurfaceWaveModel);
    }
    // endregion

    // region ButtonsClick of Map

    /**
     * Open Map
     *
     * @param actionEvent
     */
    public void OpenMap_Click(ActionEvent actionEvent) {

        try {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Открыть карту");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Map Files", "*.tif"));
            File mapPath = fileChooser.showOpenDialog(WindowMap);

            if (mapPath != null) {

                Raster raster = new Raster(mapPath.getPath());
                PropertiesMap.setRasterLayerMap(new RasterLayer(raster));
                arcGISScene.getOperationalLayers().add(PropertiesMap.getRasterLayerMap());

                PropertiesMap.setMapPath(mapPath.getPath());

                for (IWndEvents mapEvent : listenersMap)
                    mapEvent.OnChangeLocalMapPath();

            }
        } catch (Exception ex) {
            System.out.println("Ошибка!");
        }
    }

    /**
     * Open Matrix
     *
     * @param actionEvent
     */
    public void OpenMatrix_Click(ActionEvent actionEvent) {

        try {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Открыть матрицу высот");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Matrix Files", "*.tif"));
            File matrixPath = fileChooser.showOpenDialog(WindowMap);

            if (matrixPath != null) {

                ArrayList listMatrixPath = new ArrayList();
                listMatrixPath.add(matrixPath.getPath());
                PropertiesMap.setRasterElevationSourceMap(new RasterElevationSource(listMatrixPath));

                Surface surface = new Surface();
                surface.getElevationSources().add(PropertiesMap.getRasterElevationSourceMap());
                surface.setElevationExaggeration(1);
                arcGISScene.setBaseSurface(surface);

                PropertiesMap.setMatrixPath(matrixPath.getPath());

                for (IWndEvents mapEvent : listenersMap)
                    mapEvent.OnChangeLocalMapPath();
            }
        } catch (Exception ex) {
            System.out.println("Ошибка!");
        }
    }

    /**
     * Close Map
     *
     * @param actionEvent
     */
    public void CloseMap_Click(ActionEvent actionEvent) {

        if (arcGISScene.getOperationalLayers().contains(PropertiesMap.getRasterLayerMap())) {
            arcGISScene.getOperationalLayers().remove(PropertiesMap.getRasterLayerMap());
        }
    }

    /**
     * Увеличить
     *
     * @param actionEvent
     */
    public void ZoomIn_Click(ActionEvent actionEvent) {

        Viewpoint current = sceneViewMap.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        Viewpoint zoomedIn = new Viewpoint((Point) current.getTargetGeometry(), current.getTargetScale() / 2.0);
        sceneViewMap.setViewpointAsync(zoomedIn);
    }

    /**
     * Уменьшить
     *
     * @param actionEvent
     */
    public void ZoomOut_Click(ActionEvent actionEvent) {

        Viewpoint current = sceneViewMap.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        Viewpoint zoomedOut = new Viewpoint((Point) current.getTargetGeometry(), current.getTargetScale() * 2.0);
        sceneViewMap.setViewpointAsync(zoomedOut);
    }

    public void Scale1_1_Click(ActionEvent actionEvent) {

        StationsModel model = MapOperations.getListStationsModel().stream()
                .filter(x -> x.getIsOwn() == true)
                .findAny()
                .orElse(null);

//        Envelope mapExtent = PropertiesMap.getRasterLayerMap().getFullExtent();
//        Viewpoint mapViewPoint = new Viewpoint(mapExtent);
//        sceneViewMap.setViewpoint(mapViewPoint);
        Viewpoint currentViewpoint = sceneViewMap.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        Viewpoint newViewPoint = new Viewpoint(model.getCoordinates().latitude, model.getCoordinates().longitude, currentViewpoint.getTargetScale());
        sceneViewMap.setViewpoint(newViewPoint);
    }
    // endregion

    // region Connection ADSB
    @Override
    public void OnButtonClick(String nameServer) {

        try {

            switch (nameServer) {

                case "ADSBReceiver":

                    if (!IsConnectADSB) {

                        try {
                           ADSBReceiver.disconnectADSB();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        ADSBReceiver.connectADSB();
                    }
                    break;
            }
        } catch (Exception ex) {

        }

    }

    @Override
    public void OnConnected(Boolean aBoolean) {

        if(aBoolean) {

            IsConnectADSB = false;
            ucADSBConnection.ShowConnect();

            try {
                ADSBReceiver.getData();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            IsConnectADSB = true;
            ucADSBConnection.ShowDisconnect();

            // TEST ---------------------------------------------
//             ADSBReceiver.getDataTEST();
            // --------------------------------------------- TEST
        }
    }

    @Override
    public void OnPropertiesData(ADSBModel adsbModel) {

        Platform.runLater(() -> {

            if (adsbModel.getLatitude() == 500 || adsbModel.getLongitude() == 500) { return; }

            AirplanesModel model = new AirplanesModel();
            model.setIcao(adsbModel.getICAO());
            model.setCoordinates(new Coord(adsbModel.getLatitude(), adsbModel.getLongitude(), (float) (adsbModel.getAltitude() * 0.3048)));
            model.setTime(adsbModel.getCurrentDateTime());

            int ind = -1;
            for (int i = 0; i < listAirplanes.size(); i++) {

                if (listAirplanes.get(i).getIcao().equals(model.getIcao())) {

                    ind = i;
                }
            }

            if(ind != -1) {

                listAirplanes.set(ind, model);
            } else {

                listAirplanes.add(model);
            }

            ucAirplanes.UpdateAirplanes(listAirplanes);

            adsbReceiver.setData(listAirplanes, sceneViewMap);
            /*if(chbAirSituation.isSelected()){
                adsbReceiver.showGraphics(sceneViewMap);
            }else {
                adsbReceiver.hideGraphics(sceneViewMap);
            }*/
        });
    }
    // endregion
}
