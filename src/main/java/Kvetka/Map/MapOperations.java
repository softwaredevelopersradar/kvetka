package Kvetka.Map;

import Kvetka.Map.models.ComplexCompositionEnemyModel;
import Kvetka.Map.models.ComplexCompositionOwnModel;
import Kvetka.Map.models.MapStationsModel;
import Kvetka.Map.operations.signalSources.DrawBearing;
import Kvetka.Map.operations.situation.*;
import KvetkaModels.StationsModel;
import MapModels.*;
import StationModels.ListOfPictures;
import StationModels.Picture;
import com.esri.arcgisruntime.geometry.*;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.SceneView;
import com.esri.arcgisruntime.symbology.*;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import java.util.List;

import StationModels.ListOfMyPictures;
import javafx.scene.image.ImageView;

public class MapOperations {

    // region Add ComplexCompositionOwn
    public static List<ComplexCompositionOwnModel> ListComplexCompositionOwn = new ArrayList<>();
    public static List<StationModel> stationsOwnModels = new ArrayList<>();

    public static MyStationsModel MyStationsModel;

    public static void setMyStationsModel(MyStationsModel myStationsModel) throws IOException {

        MyStationsModel = myStationsModel;
        UpdateListComplexCompositionOwn();
    }

    private static void UpdateListComplexCompositionOwn() throws IOException {

        // Image ---------------------------------------------------------------------------------
        PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(AddSignature(MyStationsModel.getImage().getImage(), MyStationsModel.getSignature()));

        markerSymbol.setHeight(150); //высоту и ширину можно добавить в качестве парметров
        markerSymbol.setWidth(200);

        com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                MyStationsModel.getLongitude(),
                MyStationsModel.getLatitude(),
                SpatialReferences.getWgs84());

        Graphic graphic = new Graphic(symbolPoint, markerSymbol);
        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
        graphicsOverlay.getGraphics().add(graphic);

        ListComplexCompositionOwn.add(new ComplexCompositionOwnModel(graphicsOverlay, graphic, MyStationsModel.getLongitude(), MyStationsModel.getLatitude()));
        stationsOwnModels.add(new StationModel(MyStationsModel.getCode(), MyStationsModel.getLongitude(), MyStationsModel.getLatitude(), MyStationsModel.getSignature()));
    }

    public static void setYamlLoadCompositionOwn(SituationOnMap situation){
        var stOwn = situation.getStationsOwn().getStations();
        var stEnemy = situation.getStationsEnemy().getStations();

        ObservableList<Picture> pictures = ListOfPictures.getListOfPictures();
        ObservableList<Picture> picturesOwn = ListOfMyPictures.getListOfPictures();

        List<String> temp = new ArrayList<>();
        for(var pp : picturesOwn){
            temp.add(pp.getCode());
        }

        if (!stOwn.isEmpty()) {
            for (var st : stOwn){
                for(var p : pictures){
                    if (st.getCode().equals(p.getCode())) {

                        PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(AddSignature(p.getImage(), st.getSignature()));

                        markerSymbol.setHeight(150); //высоту и ширину можно добавить в качестве парметров
                        markerSymbol.setWidth(200);

                        com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                                st.getLongitude(),
                                st.getLatitude(),
                                SpatialReferences.getWgs84());

                        Graphic graphic = new Graphic(symbolPoint, markerSymbol);
                        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
                        graphicsOverlay.getGraphics().add(graphic);

                        ListComplexCompositionOwn.add(new ComplexCompositionOwnModel(graphicsOverlay, graphic, st.getLongitude(), st.getLatitude()));
                        stationsOwnModels.add(new StationModel(st.getCode(), st.getLongitude(), st.getLatitude(), st.getSignature()));
                    }
                }
            }
        }

        if (!stEnemy.isEmpty()) {
            for (var s : stEnemy){
                for (var p : pictures) {

                    if (s.getCode().equals(p.getCode())) {

                        PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(AddSignature(p.getImage(), s.getSignature()));

                        markerSymbol.setHeight(150); //высоту и ширину можно добавить в качестве парметров
                        markerSymbol.setWidth(200);

                        com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                                s.getLongitude(),
                                s.getLatitude(),
                                SpatialReferences.getWgs84());

                        Graphic graphic = new Graphic(symbolPoint, markerSymbol);
                        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
                        graphicsOverlay.getGraphics().add(graphic);

                        if(!temp.contains(p.getCode())){
                            ListComplexCompositionEnemy.add(new ComplexCompositionEnemyModel(graphicsOverlay, graphic, s.getLongitude(), s.getLatitude()));
                            stationEnemyModels.add(new StationModel(s.getCode(), s.getLongitude(), s.getLatitude(), s.getSignature()));
                        }
                    }
                }
            }
        }
    }

    public static void AddImagesOwn(SceneView sceneViewMap) {

        RemoveImagesOwn(sceneViewMap);

        for (int i = 0; i < ListComplexCompositionOwn.stream().count(); i++) {

            sceneViewMap.getGraphicsOverlays().add(ListComplexCompositionOwn.get(i).GraphicsOverlay);
        }
    }

    public static void RemoveImagesOwn(SceneView sceneViewMap) {

        for (int i = 0; i < ListComplexCompositionOwn.stream().count(); i++) {

            sceneViewMap.getGraphicsOverlays().remove(ListComplexCompositionOwn.get(i).GraphicsOverlay);
            sceneViewMap.getGraphicsOverlays().remove(ListComplexCompositionOwn.get(i).Graphic);
        }
    }

    public static void saveSituation() throws IOException {
        var own = new StationsOwn(stationsOwnModels);
        var enemy = new StationsEnemy(stationEnemyModels);

        var situation = new SituationOnMap(own, enemy);
        SituationProperties.setSituation(situation);
    }
    // endregion

    // region Add ComplexCompositionEnemy
    public static List<ComplexCompositionEnemyModel> ListComplexCompositionEnemy = new ArrayList<>();

    public static List<StationModel> stationEnemyModels = new ArrayList<>();

    public static EnemyStationModel EnemyStationModel;

    public static void setEnemyStationModel(EnemyStationModel enemyStationModel) {

        EnemyStationModel = enemyStationModel;
        UpdateListComplexCompositionEnemy();
    }

    public static void UpdateListComplexCompositionEnemy() {

        // Image ---------------------------------------------------------------------------------
        PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(AddSignature(EnemyStationModel.getImage().getImage(), EnemyStationModel.getSignature()));

        markerSymbol.setHeight(150); //высоту и ширину можно добавить в качестве парметров
        markerSymbol.setWidth(200);

        com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                EnemyStationModel.getLongitude(),
                EnemyStationModel.getLatitude(),
                SpatialReferences.getWgs84());

        Graphic graphic = new Graphic(symbolPoint, markerSymbol);
        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
        graphicsOverlay.getGraphics().add(graphic);

        ListComplexCompositionEnemy.add(new ComplexCompositionEnemyModel(graphicsOverlay, graphic, EnemyStationModel.getLongitude(), EnemyStationModel.getLatitude()));
        stationEnemyModels.add(new StationModel(EnemyStationModel.getCode(), EnemyStationModel.getLongitude(), EnemyStationModel.getLatitude(), EnemyStationModel.getSignature()));
    }

    public static void AddImagesEnemy(SceneView sceneViewMap) {

        RemoveImagesEnemy(sceneViewMap);

        for (int i = 0; i < ListComplexCompositionEnemy.stream().count(); i++) {
            sceneViewMap.getGraphicsOverlays().add(ListComplexCompositionEnemy.get(i).GraphicsOverlay);
        }
    }

    public static void RemoveImagesEnemy(SceneView sceneViewMap) {

        for (int i = 0; i < ListComplexCompositionEnemy.stream().count(); i++) {

            sceneViewMap.getGraphicsOverlays().remove(ListComplexCompositionEnemy.get(i).GraphicsOverlay);
            sceneViewMap.getGraphicsOverlays().remove(ListComplexCompositionEnemy.get(i).Graphic);
        }
    }
    // endregion

    // region Add Stations
    /**
     * Wnd Map
     */
    public static Map<GraphicsOverlay, Graphic> complex = new HashMap<>();
    public static List<MapStationsModel> ListMapStations = new ArrayList<>();
    public static List<MapStationsModel> ListZoneRS = new ArrayList<>();
    private static List<MapStationsModel> ListStationModelPrev = new ArrayList<>();

    public static List<StationsModel> ListStationsModel;
    public static List<StationsModel> DetectedMyStation = new ArrayList<>();

    public static List<StationsModel> getListStationsModel() {
        return ListStationsModel;
    }

    public static void setListStationsModel(List<StationsModel> listStationModel) {

        ListStationsModel = listStationModel;

        UpdateListMapStations();

        DetectMyStation(listStationModel);
        DrawBearing.setDetectionStation(DetectedMyStation);
    }

    private static void DetectMyStation(List<StationsModel> station) {

        for (Iterator<StationsModel> iterator = station.iterator(); iterator.hasNext(); ) {
            StationsModel st = iterator.next();
            if (st.getIsOwn()) {
                DetectedMyStation = station;
                UpdateZoneRS();
            }
        }
    }

    private static void UpdateZoneRS() {

        com.esri.arcgisruntime.geometry.Point center = new com.esri.arcgisruntime.geometry.Point(
                DetectedMyStation.get(0).getCoordinates().longitude,
                DetectedMyStation.get(0).getCoordinates().latitude,
                SpatialReferences.getWgs84());

        Graphic sectorGraphic = new Graphic();
        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
        Graphic lineOne = new Graphic();
        Graphic lineTwo = new Graphic();
        //graphicsOverlay.getGraphics().add(sectorGraphic);

        SimpleFillSymbol sectorFillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0x8800FF00, null);
        SimpleLineSymbol sectorLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, 0x8800FF00, 3);
        SimpleMarkerSymbol sectorMarkerSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, 0x8800FF00, 3);

        LinearUnit unit = new LinearUnit(LinearUnitId.KILOMETERS);
        AngularUnit angularUnit = new AngularUnit(AngularUnitId.DEGREES);

        Point first = GeometryEngine.moveGeodetic(center, 1000, unit, DetectedMyStation.get(0).getAntennaAngle() - 40, angularUnit, GeodeticCurveType.GEODESIC);
        Point two = GeometryEngine.moveGeodetic(center, 1000, unit, DetectedMyStation.get(0).getAntennaAngle() + 40, angularUnit, GeodeticCurveType.GEODESIC);

        MapMarkerStyle.arrowBL.setAngle(DetectedMyStation.get(0).getAntennaAngle() + 45);
        MapMarkerStyle.arrowTL.setAngle(DetectedMyStation.get(0).getAntennaAngle() - 250);

        Graphic graphic1 = new Graphic(first, MapMarkerStyle.arrowBL);
        Graphic graphic2 = new Graphic(two, MapMarkerStyle.arrowTL);

        PointCollection polylinePointsOne = new PointCollection(SpatialReferences.getWgs84());
        polylinePointsOne.add(center);
        polylinePointsOne.add(first);
        Polyline polylineOne = new Polyline(polylinePointsOne);
        lineOne = new Graphic(polylineOne, MapMarkerStyle.polylineSymbol);

        PointCollection polylinePointsTwo = new PointCollection(SpatialReferences.getWgs84());
        polylinePointsTwo.add(center);
        polylinePointsTwo.add(two);
        Polyline polylineTwo = new Polyline(polylinePointsTwo);
        lineTwo = new Graphic(polylineTwo, MapMarkerStyle.polylineSymbol);

        graphicsOverlay.getGraphics().add(lineOne);
        graphicsOverlay.getGraphics().add(lineTwo);
        graphicsOverlay.getGraphics().add(graphic1);
        graphicsOverlay.getGraphics().add(graphic2);

/*
        GeodesicSectorParameters geodesicSectorParameters = new GeodesicSectorParameters();
        geodesicSectorParameters.setCenter(center);
        geodesicSectorParameters.setAxisDirection(0);
        geodesicSectorParameters.setMaxSegmentLength(10000);
        geodesicSectorParameters.setGeometryType(GeometryType.POLYLINE);
        geodesicSectorParameters.setSectorAngle(100);
        geodesicSectorParameters.setSemiAxis1Length(500);
        geodesicSectorParameters.setSemiAxis2Length(500);
        geodesicSectorParameters.setStartDirection(90 - DetectedMyStation.get(0).getAntennaAngle() + geodesicSectorParameters.getSectorAngle() / 2);  //

        geodesicSectorParameters.setLinearUnit(unit);
        geodesicSectorParameters.setAngularUnit(angularUnit);

        Geometry sectorGeometry = GeometryEngine.sectorGeodesic(geodesicSectorParameters);
        sectorGraphic.setGeometry(sectorGeometry);
        switch (sectorGeometry.getGeometryType()) {
            case MULTIPOINT:
                sectorGraphic.setSymbol(sectorMarkerSymbol);
                break;
            case POLYGON:
                sectorGraphic.setSymbol(sectorFillSymbol);
                break;
            case POLYLINE:
                sectorGraphic.setSymbol(sectorLineSymbol);
                break;
        }

        GeodesicEllipseParameters geodesicEllipseParameters = new GeodesicEllipseParameters(center, 300, 300);
        geodesicEllipseParameters.setAxisDirection(0);
        Geometry ellipseGeometry = GeometryEngine.ellipseGeodesic(geodesicEllipseParameters);
        */

        ListZoneRS.add(new MapStationsModel(graphicsOverlay, sectorGraphic));
    }

    public static void UpdateZoneRS(SceneView sceneView) {
        RemoveZoneRS((sceneView));
        sceneView.getGraphicsOverlays().add(ListZoneRS.get(0).GraphicsOverlay);
    }

    public static void ShowZoneRS(SceneView sceneView, boolean selected) {
        if (selected) {
            sceneView.getGraphicsOverlays().add(ListZoneRS.get(0).GraphicsOverlay);
        } else {
            var index = sceneView.getGraphicsOverlays().indexOf(ListZoneRS.get(0).GraphicsOverlay);
            //sceneView.getGraphicsOverlays().get(index).getGraphics().remove(ListZoneRS.get(0).Graphic);
            sceneView.getGraphicsOverlays().remove(index);
        }
    }

    public static void RemoveZoneRS(SceneView sceneView) {
        if (!ListZoneRS.isEmpty()) {
            for (Iterator<MapStationsModel> iterator = ListZoneRS.iterator(); iterator.hasNext(); ) {
                MapStationsModel zone = iterator.next();

                var index = sceneView.getGraphicsOverlays().indexOf(zone.GraphicsOverlay);

                if (index != -1) {
                    sceneView.getGraphicsOverlays().get(index).getGraphics().clear();  //remove(zone.Graphic)
                    sceneView.getGraphicsOverlays().remove(index);
                    iterator.remove();
                }
            }
        }
    }

    public static void UpdateListMapStations() {

        ObservableList<Picture> listOfImages = ListOfMyPictures.getListOfPictures();
        ImageView imageView = new ImageView();
        ListStationModelPrev = ListMapStations;

        for (var p : listOfImages) {

            if (p.getCode().equals("SP_MY")) {

                imageView.setImage(p.getImage());
            }
        }

        // Image ---------------------------------------------------------------------------------
        ListMapStations = new ArrayList<>();
        complex = new HashMap<>();
        for (int i = 0; i < ListStationsModel.stream().count(); i++) {

            PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(imageView.getImage());

            markerSymbol.setHeight(20); //высоту и ширину можно добавить в качестве парметров
            markerSymbol.setWidth(20);

            com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                    ListStationsModel.get(i).getCoordinates().longitude,
                    ListStationsModel.get(i).getCoordinates().latitude,
                    SpatialReferences.getWgs84());

            Graphic graphic = new Graphic(symbolPoint, markerSymbol);
            GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
            graphicsOverlay.getGraphics().add(graphic);

            ListMapStations.add(new MapStationsModel(graphicsOverlay, graphic));
            complex.put(graphicsOverlay, graphic);
        }
    }

    /**
     * Wnd DirectionFinder
     */
    public static List<MapStationsModel> ListMapStationsDF = new ArrayList<>();

    public static List<StationsModel> ListStationsDFModel;

    public static void setListStationsDFModel(List<StationsModel> listStationDFModel) {

        ListStationsDFModel = listStationDFModel;
        UpdateListMapStationsDF();
    }

    public static void UpdateListMapStationsDF() {

        ObservableList<Picture> listOfImages = ListOfMyPictures.getListOfPictures();
        ImageView imageView = new ImageView();

        for (var p : listOfImages) {

            if (p.getCode().equals("SP_MY")) {

                imageView.setImage(p.getImage());
            }
        }

        // Image ---------------------------------------------------------------------------------
        ListMapStationsDF = new ArrayList<>();
        for (int i = 0; i < ListStationsDFModel.stream().count(); i++) {

            PictureMarkerSymbol markerSymbol = new PictureMarkerSymbol(imageView.getImage());

            markerSymbol.setHeight(20); //высоту и ширину можно добавить в качестве парметров
            markerSymbol.setWidth(20);

            com.esri.arcgisruntime.geometry.Point symbolPoint = new com.esri.arcgisruntime.geometry.Point(
                    ListStationsDFModel.get(i).getCoordinates().longitude,
                    ListStationsDFModel.get(i).getCoordinates().latitude,
                    SpatialReferences.getWgs84());

            Graphic graphic = new Graphic(symbolPoint, markerSymbol);
            GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
            graphicsOverlay.getGraphics().add(graphic);

            ListMapStationsDF.add(new MapStationsModel(graphicsOverlay, graphic));
        }

    }

    public static void AddStationsDF(SceneView sceneViewMap) {

        RemoveStationsDF(sceneViewMap);

        for (int i = 0; i < ListMapStationsDF.stream().count(); i++) {

            sceneViewMap.getGraphicsOverlays().add(ListMapStationsDF.get(i).GraphicsOverlay);
        }
    }

    public static void RemoveStationsDF(SceneView sceneViewMap) {

        for (int i = 0; i < ListMapStationsDF.stream().count(); i++) {

            sceneViewMap.getGraphicsOverlays().remove(ListMapStationsDF.get(i).GraphicsOverlay);
            sceneViewMap.getGraphicsOverlays().remove(ListMapStationsDF.get(i).Graphic);
        }
    }
    // endregion

    private static Image AddSignature(Image image, String signature) {

        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
        java.awt.Image newSizeImage = bufferedImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        BufferedImage newSizeBufferedImage = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = newSizeBufferedImage.createGraphics();
        g2d.drawImage(newSizeImage, 0, 0, null);
        g2d.dispose();

        BufferedImage mainBufferedImage = new BufferedImage(200, 150, BufferedImage.TYPE_INT_ARGB);
        Graphics g1 = mainBufferedImage.getGraphics();
        g1.drawImage(newSizeBufferedImage, (mainBufferedImage.getWidth() - newSizeBufferedImage.getWidth()) / 2, (mainBufferedImage.getHeight() - newSizeBufferedImage.getHeight()) / 2 - 20, null);

        Graphics g2 = mainBufferedImage.getGraphics();
        Font myFont = new Font("Arial", Font.BOLD, 12);

        FontMetrics metrics = g2.getFontMetrics(myFont);//для получения ширины полученного текста и дальнейшего размещения подписи в центре по X

        g2.setFont(myFont);
        g2.setColor(new Color(0, 198, 255)); //цвет текста
        g2.drawString(signature, (int) (mainBufferedImage.getWidth() - metrics.stringWidth(signature)) / 2, 75);
        g2.dispose();

        return SwingFXUtils.toFXImage(mainBufferedImage, null);
    }

    /**
     * Перевод географических координат (широты и долготы) точки в прямоугольные
     *
     * @param latWgs84    Широта
     * @param longWgs84   Долгота
     * @param heightWgs84 Высота
     * @return
     */
    public static double[] WGS84ToSK42Meters(double latWgs84, double longWgs84, double heightWgs84) {
        double ro = 206264.8062;// ' Число угловых секунд в радиане

        //' Эллипсоид Красовского
        double aP = 6378245; //' Большая полуось
        double alP = 1 / 298.3; //' Сжатие
        double e2P = 2 * alP - Math.pow(alP, 2); //' Квадрат эксцентриситета

        //' Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        double aW = 6378137; //' Большая полуось
        double alW = 1 / 298.257223563; //' Сжатие
        double e2W = 2 * alW - Math.pow(alW, 2); //' Квадрат эксцентриситета

        //' Вспомогательные значения для преобразования эллипсоидов
        double a1 = (aP + aW) / 2;
        double e21 = (e2P + e2W) / 2;
        double da = aW - aP;
        double de2 = e2W - e2P;

        //' Линейные элементы трансформирования, в метрах
        double dx = 23.92;
        double dy = -141.27;
        double dz = -80.9;

        //' Угловые элементы трансформирования, в секундах
        double wx = 0;
        double wy = 0;
        double wz = 0;

        //' Дифференциальное различие масштабов
        double ms = 0;

        double B, L, M11, N1;
        B = latWgs84 * Math.PI / 180;
        L = longWgs84 * Math.PI / 180;
        M11 = a1 * (1 - e21) / Math.pow((1 - e21 * Math.pow(Math.sin(B), 2)), 1.5);
        N1 = a1 * Math.pow((1 - e21 * Math.pow(Math.sin(B), 2)), -0.5);
        double dB = ro / (M11 + heightWgs84) * (N1 / a1 * e21 * Math.sin(B) * Math.cos(B) * da + (Math.pow(N1, 2) / Math.pow(a1, 2) + 1) * N1 * Math.sin(B) * Math.cos(B) * de2 / 2 - (dx * Math.cos(L) + dy * Math.sin(L)) * Math.sin(B) + dz * Math.cos(B)) - wx * Math.sin(L) * (1 + e21 * Math.cos(2 * B)) + wy * Math.cos(L) * (1 + e21 * Math.cos(2 * B)) - ro * ms * e21 * Math.sin(B) * Math.cos(B);

        double SK42_LatDegrees = latWgs84 - dB / 3600;//широта в ск42 в градусах

        B = latWgs84 * Math.PI / 180;
        L = longWgs84 * Math.PI / 180;
        N1 = a1 * Math.pow((1 - e21 * Math.pow(Math.sin(B), 2)), -0.5);
        double dL = ro / ((N1 + heightWgs84) * Math.cos(B)) * (-dx * Math.sin(L) + dy * Math.cos(L)) + Math.tan(B) * (1 - e21) * (wx * Math.cos(L) + wy * Math.sin(L)) - wz;

        double SK42_LongDegrees = longWgs84 - dL / 3600;//долгота в ск42 в градусах

        // Перевод географических координат (широты и долготы) точки в прямоугольные

        // Номер зоны Гаусса-Крюгера
        int zone = (int) (SK42_LongDegrees / 6.0 + 1);

        // Параметры эллипсоида Красовского
        double a = 6378245.0;          // Большая (экваториальная) полуось
        double b = 6356863.019;        // Малая (полярная) полуось
        double e2 = (Math.pow(a, 2) - Math.pow(b, 2)) / Math.pow(a, 2);  // Эксцентриситет
        double n = (a - b) / (a + b);        // Приплюснутость

        // Параметры зоны Гаусса-Крюгера
        double F = 1.0;                   // Масштабный коэффициент
        double Lat0 = 0.0;                // Начальная параллель (в радианах)
        double Lon0 = (zone * 6 - 3) * Math.PI / 180;  // Центральный меридиан (в радианах)
        double N0 = 0.0;                  // Условное северное смещение для начальной параллели
        double E0 = zone * 1e6 + 500000.0;    // Условное восточное смещение для центрального меридиана

        // Перевод широты и долготы в радианы
        double Lat = SK42_LatDegrees * Math.PI / 180.0;
        double Lon = SK42_LongDegrees * Math.PI / 180.0;

        // Вычисление переменных для преобразования
        double sinLat = Math.sin(Lat);
        double cosLat = Math.cos(Lat);
        double tanLat = Math.tan(Lat);

        double v = a * F * Math.pow(1 - e2 * Math.pow(sinLat, 2), -0.5);
        double p = a * F * (1 - e2) * Math.pow(1 - e2 * Math.pow(sinLat, 2), -1.5);
        double n2 = v / p - 1;
        double M1 = (1 + n + 5.0 / 4.0 * Math.pow(n, 2) + 5.0 / 4.0 * Math.pow(n, 3)) * (Lat - Lat0);
        double M2 = (3 * n + 3 * Math.pow(n, 2) + 21.0 / 8.0 * Math.pow(n, 3)) * Math.sin(Lat - Lat0) * Math.cos(Lat + Lat0);
        double M3 = (15.0 / 8.0 * Math.pow(n, 2) + 15.0 / 8.0 * Math.pow(n, 3)) * Math.sin(2 * (Lat - Lat0)) * Math.cos(2 * (Lat + Lat0));
        double M4 = 35.0 / 24.0 * Math.pow(n, 3) * Math.sin(3 * (Lat - Lat0)) * Math.cos(3 * (Lat + Lat0));
        double M = b * F * (M1 - M2 + M3 - M4);
        double I = M + N0;
        double II = v / 2 * sinLat * cosLat;
        double III = v / 24 * sinLat * Math.pow(cosLat, 3) * (5 - Math.pow(tanLat, 2) + 9 * n2);
        double IIIA = v / 720 * sinLat * Math.pow(cosLat, 5) * (61 - 58 * Math.pow(tanLat, 2) + Math.pow(tanLat, 4));
        double IV = v * cosLat;
        double V = v / 6 * Math.pow(cosLat, 3) * (v / p - Math.pow(tanLat, 2));
        double VI = v / 120 * Math.pow(cosLat, 5) * (5 - 18 * Math.pow(tanLat, 2) + Math.pow(tanLat, 4) + 14 * n2 - 58 * Math.pow(tanLat, 2) * n2);

        // Вычисление северного и восточного смещения (в метрах)
        double N = I + II * Math.pow(Lon - Lon0, 2) + III * Math.pow(Lon - Lon0, 4) + IIIA * Math.pow(Lon - Lon0, 6);
        double E = E0 + IV * (Lon - Lon0) + V * Math.pow(Lon - Lon0, 3) + VI * Math.pow(Lon - Lon0, 5);

        return new double[]{N, E};
    }

//    public static Viewpoint SetNewViewPoint(double longitude, double latitude)
//    {
//        Envelope mapExtent = PropertiesMap.getRasterLayerMap().getFullExtent();
//        Viewpoint mapViewPoint = new Viewpoint(mapExtent);
//        sceneView.setViewpoint(mapViewPoint);
//        Viewpoint currentViewpoint = sceneView.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
//        Viewpoint newViewPoint = new Viewpoint(longitude,latitude, currentViewpoint.getTargetScale());
//        sceneView.setViewpoint(newViewPoint);
//
//    }
}
