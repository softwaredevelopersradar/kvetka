package Kvetka.Map.models;

import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;

public class ListRoutesModel {
    public ListRoutesModel(Integer number, GraphicsOverlay graphicsOverlay) {
        Number = number;
        this.graphicsOverlay = graphicsOverlay;
    }

    public Integer Number;
    public GraphicsOverlay graphicsOverlay;

}
