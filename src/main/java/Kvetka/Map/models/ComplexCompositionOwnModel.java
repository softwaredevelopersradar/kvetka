package Kvetka.Map.models;

import MapModels.MyStationsModel;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import javafx.beans.property.SimpleDoubleProperty;

public class ComplexCompositionOwnModel extends MyStationsModel {

    public GraphicsOverlay GraphicsOverlay;
    public Graphic Graphic;
    public Double Longitude;
    public Double Latitude;

    public ComplexCompositionOwnModel(GraphicsOverlay graphicsOverlay, Graphic graphic, Double longitude, Double latitude)
    {
        GraphicsOverlay = graphicsOverlay;
        Graphic = graphic;
        Longitude = longitude;
        Latitude = latitude;
    }
}

