package Kvetka.Map.models;

import MapModels.EnemyStationModel;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import javafx.beans.property.SimpleDoubleProperty;

public class ComplexCompositionEnemyModel extends EnemyStationModel {

    public com.esri.arcgisruntime.mapping.view.GraphicsOverlay GraphicsOverlay;
    public com.esri.arcgisruntime.mapping.view.Graphic Graphic;

    public Double Longitude;
    public Double Latitude;

    public ComplexCompositionEnemyModel(GraphicsOverlay graphicsOverlay, Graphic graphic, Double longitude, Double latitude)
    {
        GraphicsOverlay = graphicsOverlay;
        Graphic = graphic;
        Longitude = longitude;
        Latitude = latitude;
    }
}
