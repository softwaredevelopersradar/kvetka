package Kvetka.Map.models;

import KvetkaModels.StationsModel;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;

public class MapStationsModel extends StationsModel {

    public com.esri.arcgisruntime.mapping.view.GraphicsOverlay GraphicsOverlay;
    public com.esri.arcgisruntime.mapping.view.Graphic Graphic;

    public MapStationsModel(GraphicsOverlay graphicsOverlay, Graphic graphic)
    {
        GraphicsOverlay = graphicsOverlay;
        Graphic = graphic;
    }
}
