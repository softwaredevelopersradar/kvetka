package Kvetka.Map;

import java.text.DecimalFormat;

public class NumberFormat {

    public static DecimalFormat df0 = new DecimalFormat("#");
    public static DecimalFormat df1 = new DecimalFormat("#.#");
    public static DecimalFormat df2 = new DecimalFormat("#.##");
    public static DecimalFormat df3 = new DecimalFormat("#.###");
    public static DecimalFormat df4 = new DecimalFormat("#.####");
    public static DecimalFormat df5 = new DecimalFormat("#.#####");
    public static DecimalFormat df6 = new DecimalFormat("#.######");
    public static DecimalFormat formatter = new DecimalFormat("#0.00 м");
    public static DecimalFormat startFormatter = new DecimalFormat("#0 м");

}
