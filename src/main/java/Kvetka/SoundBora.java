package Kvetka;

import javafx.fxml.Initializable;
import javafx.scene.media.AudioClip;
import org.apache.commons.io.IOUtils;

import javax.sound.sampled.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class SoundBora {

    private Clip clip = null;

    public SoundBora() {

    }

    public void play(byte[] byteData) {

        try {

            InputStream inputStream = new ByteArrayInputStream(byteData);
            BufferedInputStream bufferedInputStream = IOUtils.buffer(inputStream);
            AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 8000f, 16, 1, 2, 8000f, false);
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, new AudioInputStream(bufferedInputStream, format, byteData.length));

            clip = AudioSystem.getClip();
            clip.open(audioInputStream);

            clip.stop();
            clip.setFramePosition(0);
            clip.start();
        } catch (IOException | LineUnavailableException exc) {
            exc.printStackTrace();
        }
    }

    public void stop() {

        try {
            if (clip != null) {
                clip.stop();
            }
        } catch (Exception ex) {

        }

    }
}

//public class SoundBora implements AutoCloseable {
//
//    private Clip clip = null;
//    private boolean playing = false;
//
//    public boolean isPlaying() {
//        return playing;
//    }
//
//    @Override
//    public void close() throws Exception {
//
//        if (clip != null)
//            clip.close();
//    }
//
//    public SoundBora(byte[] array){
//
//        try {
////            byte[] cutted = Arrays.copyOf(array, array.length - 9);
////
////            InputStream inputStream = new ByteArrayInputStream(cutted);
//            AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 8000f, 16, 1, 2, 8000f, false);
//
//            InputStream inputStream = new ByteArrayInputStream(array);
//            BufferedInputStream bufferedInputStream = IOUtils.buffer(inputStream);
//
//
//            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, new AudioInputStream(bufferedInputStream, format, array.length));
//
//            ////////////////////////////////////////
////            var info = Port.Info.HEADPHONE;
////            if(AudioSystem.isLineSupported(info)){
////                var line = (Port)AudioSystem.getLine(Port.Info.HEADPHONE);
////            }
////            AudioSystem.getSourceLineInfo(info);
//            ///////////////////////
//            clip = AudioSystem.getClip();
//            clip.open(audioInputStream);
//
//        } catch (LineUnavailableException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void playSound(byte[] array) {
//
//        SoundBora soundBora = new SoundBora(array);
//
//        soundBora.play(true);
//    }
//
//    public void play(boolean breakOld) {
//
//        if (breakOld) {
//
//            clip.stop();
//            clip.setFramePosition(0);
//            clip.start();
//            playing = true;
//        } else if (!isPlaying()) {
//
//            clip.setFramePosition(0);
//            clip.start();
//            playing = true;
//        }
//    }
//
//    public void stop() {
//
//        if (playing) {
//
//            clip.stop();
//        }
//    }
//
//    public static void playAudioUsingByteArray(byte[] byteArr, AudioFormat format) {
//        try (Clip clip = AudioSystem.getClip()) {
//            clip.open(format, byteArr, 0, byteArr.length);
//            clip.start();
//            clip.drain();
//            Thread.sleep(clip.getMicrosecondLength());
//        }
//        catch (LineUnavailableException | InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//}
