package Kvetka.Interfaces;

import KvetkaModels.RouteModel;

public interface IMapRouteEvents {
    public void onSubmittedRoute(RouteModel routeModel);
}
