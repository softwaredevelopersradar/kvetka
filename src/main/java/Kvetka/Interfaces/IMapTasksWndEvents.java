package Kvetka.Interfaces;

import MapModels.*;

import java.io.IOException;

public interface IMapTasksWndEvents {

    public void OnCloseAzimuthWnd();
    public void OnCloseComplexCompositionOwnWnd();
    public void OnCloseComplexCompositionEnemyWnd();
    public void OnCloseZoneSpaceWaveWnd();
    public void OnCloseZoneSurfaceWaveWnd();

    public void OnSendComplexCompositionOwn(MyStationsModel complexCompositionOwnModel) throws IOException;
    public void OnSendComplexCompositionEnemy(EnemyStationModel complexCompositionEnemyModel) throws IOException;
    public void OnSendAzimuth(AzimuthModel azimuthModel);
    public void OnSendZoneSpaceWave(EDPRVModel zoneSpaceWaveModel);
    public void OnSendZoneSurfaceWave(EDPOVModel zoneSurfaceWaveModel);
}
