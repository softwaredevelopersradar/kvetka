package Kvetka.Interfaces;

import PropertiesControl.LocalSettings;

import java.io.IOException;

public interface IWndEvents {

    public void OnCloseMap();
    public void OnCloseDirectionFinder();
    public void OnCloseBPSS();
    public void OnCloseDFAccuracy();
    public void OnClosePlayer();

    public void OnChangeLocalMapPath();

//    public void OnChangeMapPath(String mapPath) throws IOException;
//    public void OnChangeMatrixPath(String matrixPath) throws IOException;
}
