package Kvetka.PlayerWAV;

import Kvetka.Interfaces.IWndEvents;
import Player.PlayerWnd;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class WAVPlayerWnd extends AnchorPane implements Initializable {

    public static Stage WindowPlayer;
    static Scene FrontEndPlayer;

    @FXML
    public PlayerWnd ucPlayer;

    private List<IWndEvents> listenersPlayer = new ArrayList<IWndEvents>();
    public void addListenerPlayer(IWndEvents toAdd) {
        listenersPlayer.add(toAdd);
    }

    public WAVPlayerWnd() {

        InitPlayerWnd();

        WindowPlayer.setTitle("Проигрыватель");
        WindowPlayer.getIcons().add(new Image(getClass().getResourceAsStream("Ico/ShowPlayer.png")));
    }

    private void InitPlayerWnd() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("WAVPlayerWnd.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            FrontEndPlayer = new Scene(fxmlLoader.load(), 332, 277);
            WindowPlayer = new Stage();
            WindowPlayer.initModality(Modality.WINDOW_MODAL);
            WindowPlayer.setResizable(false);
            WindowPlayer.setScene(FrontEndPlayer);
            WindowPlayer.show();

            WindowPlayer.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {

                    // Stage is closing
                    for (IWndEvents PlayerEvent : listenersPlayer)
                        PlayerEvent.OnClosePlayer();
                }
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
